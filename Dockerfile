from ubuntu:20.04

RUN mkdir /allgreek2me
COPY . /allgreek2me
COPY server /server

RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y cmake python3-minimal python3-pip && \
    pip3 --no-cache-dir install pybind11 flask flask_cors waitress && \
    cd /allgreek2me && \
    python3 setup.py install && \
    apt-get remove --purge -y cmake python3-pip gcc g++ && \
    apt-get autoremove -y && \
    apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /allgreek2me && \
    rm -rf /tmp/*

RUN ln -s $(which python3) /bin/python

WORKDIR /server

ENTRYPOINT ["python3", "server.py"]
