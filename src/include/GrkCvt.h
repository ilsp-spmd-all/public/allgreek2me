/*  ---------------------------------------------------------------  */
/*                         All Greek to Me!                          */
/*                                                                   */
/*                          Innoetics Ltd.                           */
/*                     Copyright (c) 2003-2008                       */
/*                       All Rights Reserved.                        */
/*  ---------------------------------------------------------------  */
/*           GrkCvt.h: linux SDK functions' declarations             */
/*  ---------------------------------------------------------------  */

#ifdef __cplusplus
extern "C"
{
#endif

extern void         greeklish_destroy();
extern bool         greeklish_init(const char *key);
extern void         greeklish_fini();
extern const char * greeklish_get_text(void* handle);
extern void         greeklish_release_handle(void* cpsz);
extern void*        greeklish_convert(const char * source);
extern void         greeklish_convert_file(const char *source, const char *dest);
extern void*        greeklish_elot_gr2grl(const char *source);
extern void*        greeklish_custom_gr2grl(const char *source);
extern void         greeklish_convert_gr2grl_file(const char *source, const char *dest);
extern void*        greeklish_analyse(const char * source);

#ifdef __cplusplus
}
#endif
