#include <jni.h>
#include <string.h>
#include <stdlib.h>

#include "AllGreek.h"
#include "GrkCvt.h"

#define ENCODING "ISO-8859-7"

char* GetStringLocalChars(JNIEnv *env, jstring str, const char *encoding){
	jstring enc = env->NewStringUTF(encoding);

	jclass clazz = env->FindClass("java/lang/String");
	jmethodID getBytes = env->GetMethodID(clazz, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray bytes = (jbyteArray)env->CallObjectMethod(str, getBytes, enc);

	jsize len = env->GetArrayLength(bytes);
	char *s = (char*)malloc(len+1);

	jbyte *bs = env->GetByteArrayElements(bytes, NULL);
	memcpy(s, bs, len);

	env->ReleaseByteArrayElements(bytes, bs, 0);

	s[len] = '\0';
	return s;
}

void ReleaseStringLocalChars(JNIEnv *env, char* str){
	free(str);
}

jstring GetStringFromChars(JNIEnv *env, const char* str, const char *encoding){
	jstring enc = env->NewStringUTF(encoding);

  int len = strlen(str);
  jbyteArray bytes = env->NewByteArray(len);
	jbyte *bs = env->GetByteArrayElements(bytes, NULL);
	memcpy(bs, str, len);
	env->ReleaseByteArrayElements(bytes, bs, 0);


	jclass clazz = env->FindClass("java/lang/String");
	jmethodID ctor = env->GetMethodID(clazz, "<init>", "([BLjava/lang/String;)V");
	jstring jstr = (jstring)env->NewObject(clazz, ctor, bytes, enc);

	return jstr;
}

JNIEXPORT jboolean JNICALL Java_AllGreek_Init
  (JNIEnv *env, jobject obj, jstring path)
{
	jboolean ret = true;
	char *pathsz= GetStringLocalChars(env, path, ENCODING);
	if(!greeklish_init(pathsz)) ret = false;
	ReleaseStringLocalChars(env, pathsz);
	return ret;
}

JNIEXPORT void JNICALL Java_AllGreek_Destroy
  (JNIEnv *env, jobject obj)
{
	greeklish_destroy();
}

JNIEXPORT jstring JNICALL Java_AllGreek_Greeklish2Greek
  (JNIEnv *env, jobject obj, jstring text)
{
	jstring ret;
	char *textsz= GetStringLocalChars(env, text, ENCODING);
	void *h = greeklish_convert(textsz);
	ret = GetStringFromChars(env, greeklish_get_text(h), ENCODING);
  greeklish_release_handle(h);
	ReleaseStringLocalChars(env, textsz);
	return ret;
}

JNIEXPORT jstring JNICALL Java_AllGreek_Greek2Greeklish
  (JNIEnv *env, jobject obj, jstring text)
{
	jstring ret;
	char *textsz= GetStringLocalChars(env, text, ENCODING);
	void *h = greeklish_custom_gr2grl(textsz);
	ret = GetStringFromChars(env, greeklish_get_text(h), ENCODING);
  greeklish_release_handle(h);
	ReleaseStringLocalChars(env, textsz);
	return ret;
}

JNIEXPORT jstring JNICALL Java_AllGreek_Greek2GreeklishElot
  (JNIEnv *env, jobject obj, jstring text)
{
	jstring ret;
	char *textsz= GetStringLocalChars(env, text, ENCODING);
	void *h = greeklish_elot_gr2grl(textsz);
	ret = GetStringFromChars(env, greeklish_get_text(h), ENCODING);
  greeklish_release_handle(h);
	ReleaseStringLocalChars(env, textsz);
	return ret;
}

JNIEXPORT jstring JNICALL Java_AllGreek_Analyse
  (JNIEnv *env, jobject obj, jstring text)
{
	jstring ret;
	char *textsz= GetStringLocalChars(env, text, ENCODING);
	void *h = greeklish_analyse(textsz);
	ret = GetStringFromChars(env, greeklish_get_text(h), ENCODING);
  greeklish_release_handle(h);
	ReleaseStringLocalChars(env, textsz);
	return ret;
}
