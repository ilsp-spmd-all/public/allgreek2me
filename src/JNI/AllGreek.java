public class AllGreek {
	String s = "Ayto einai ena test ali8iaw";
	public static void main(String args[]) {
		AllGreek ag = new AllGreek();
		  if(!ag.Init("/usr/share/allgreek/"))
				System.out.println("Failed to initialize greeklish egnine");
			System.out.println("Input:");
			System.out.println(ag.s);
			System.out.println("Greeklish2Greek:");
			System.out.println(ag.Greeklish2Greek(ag.s));
			System.out.println("Greek2Greeklish:");
			System.out.println(ag.Greek2Greeklish(ag.Greeklish2Greek(ag.s)));
			System.out.println("Analyse:");
			System.out.println(ag.Analyse(ag.s));
		  ag.Destroy();
	}
	
	// greeklish methods
	public native boolean Init(String text);
	public native void Destroy();
	public native String Greeklish2Greek(String text);
	public native String Greek2Greeklish(String text);
	public native String Greek2GreeklishElot(String text);
	public native String Analyse(String text);
	
	// load DLL that contains static method
	static {
		System.loadLibrary("allgreekjni");
	}
}
