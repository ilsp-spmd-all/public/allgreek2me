This is an example of how to call Greeklish to Greek library from JAVA using JNI
This shows all the steps from scratch (you can skip directly to step 5)

1. Write a JAVA class that declares external native calls
   AllGreek.java is such an example java class

2. Compile the java calss
   $ javac AllGreek.java

3. Generate a JNI header to implement the native calls
   $ javah -jni AllGreek
   this will generate AllGreek.h

4. Implement the native calls in C/C++
   See AllGreek.cpp for the implementation of the basic functions

5. Create a shared object fro AllGreek.cpp and link to the allgreek shared object
   assuming we are using openjdk, this would be something like
   $ c++ -O3 -DNDEBUG -fPIC -I/path/to/allgreek.sdk/include -I/usr/lib/jvm/java-6-openjdk/include -o AllGreek.cpp.o -c AllGreek.cpp
   $ c++ -fPIC -O3 -DNDEBUG -shared -Wl,-soname,liballgreekjni.so -o liballgreekjni.so AllGreek.cpp.o liballgreek.so /usr/lib/jvm/java-6-openjdk/jre/lib/amd64/libjawt.so /usr/lib/jvm/java-6-openjdk/jre/lib/amd64/server/libjvm.so -Wl,-rpath,/usr/lib/jvm/java-6-openjdk/jre/lib/amd64:/usr/lib/jvm/java-6-openjdk/jre/lib/amd64/server
   I actually used cmake for this. Here is the cmake slipet
   
   find_package(JNI REQUIRED)
   include_directories("allgreek.sdk/include")
   include_directories(${JNI_INCLUDE_DIRS})
   add_library(allgreekjni SHARED AllGreek.cpp AllGreek.h)
   target_link_libraries( allgreekjni allgreek )
   target_link_libraries( allgreekjni ${JNI_LIBRARIES} )

6. Test the JAVA program
   java -Djava.library.path=/path/to/liballgreekjni.so/ AllGreek > test.txt
