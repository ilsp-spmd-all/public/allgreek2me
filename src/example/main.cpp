/*  ---------------------------------------------------------------  */
/*                         All Greek to Me!                          */
/*                                                                   */
/*                          Innoetics Ltd.                           */
/*                     Copyright (c) 2003-2008                       */
/*                       All Rights Reserved.                        */
/*  ---------------------------------------------------------------  */
/*             main.cpp: linux SDK example application               */
/*  ---------------------------------------------------------------  */

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
using namespace std;

#include "GrkCvt.h"

void usage(const char *name){
  cout<<name<<" [-h] [-g] [-i input] [-o output]"<<endl;
  cout<<"\t-h        :\tprint help"<<endl;
  cout<<"\t-g        :\tconvert greek to greeklish (custom)"<<endl;
  cout<<"\t-e        :\tconvert greek to greeklish (elot)"<<endl;
  cout<<"\t-i input  :\tinput file (default stdin)"<<endl;
  cout<<"\t-o output :\tinput file (default stdout)"<<endl;
  cout<<"Windows-1253 encoding is assumed everywhere"<<endl;
}

void greeklish2greek(istream &in, ostream &out)
{
  char linebuf[2048];
  void *cpsz;
  if(in.good() && out.good()){
    while(in.getline(linebuf, sizeof(linebuf))){
      cpsz = greeklish_convert(linebuf);
      out<<greeklish_get_text(cpsz)<<endl;
      greeklish_release_handle(cpsz);
    }
  }	
}

void greek2greeklish(istream &in, ostream &out, bool elot)
{
  char linebuf[2048];
  void *cpsz;
  if(in.good() && out.good()){
   while(in.getline(linebuf, sizeof(linebuf))){
     cpsz = elot?greeklish_custom_gr2grl(linebuf):greeklish_custom_gr2grl(linebuf);
     out<<greeklish_get_text(cpsz)<<endl;
     greeklish_release_handle(cpsz);
   }	
  }
}

int main(int argc, char *argv[]){
  const char* input = NULL;
  const char* output = NULL;
  bool grl2gr = true;
  bool elot = false;
	
  bool parse_ok = true;

  for(int i=1;parse_ok && i<argc;i++){
    if(argv[i][0]=='-'){
      switch(argv[i][1]){
        case 'g':
          grl2gr = false;
          break;
        case 'e':
          grl2gr = false;
          elot = true;
          break;
        case 'i':
          if(input==NULL && ++i<argc) input = argv[i]; else parse_ok = false;
          break;
        case 'o':
          if(output==NULL && ++i<argc) output = argv[i]; else parse_ok = false;
          break;
        default:
          parse_ok = false;
      }
    } else parse_ok = false; 
  }
	
  if(!parse_ok){
    usage(argv[0]);
    return 0;
  }
	
#ifndef WIN32	
  greeklish_init(NULL);
#endif

  if(input && output){
    if(grl2gr){
      greeklish_convert_file(input, output);
    }else{
      greeklish_convert_gr2grl_file(input, output);
    }
  }else if(input) {
    ifstream in(input, ios::in);
    if(grl2gr){
      greeklish2greek(in, cout);
    }else{
      greek2greeklish(in, cout, elot);
    }
  }else if(output) {
    ofstream out(output, ios::out);
    if(grl2gr){
      greeklish2greek(cin, out);
    }else{
      greek2greeklish(cin, out, elot);
    }
  }else {
    if(grl2gr){
      greeklish2greek(cin, cout);
    }else{
      greek2greeklish(cin, cout, elot);
    }
  }

#ifndef WIN32	
  greeklish_fini();
#endif

  return 0;
}

