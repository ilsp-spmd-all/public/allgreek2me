//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("IdxBuilder.res");
USEFORM("idxBldUnit.cpp", Form1);
USEUNIT("MaskFile.cpp");
USEUNIT("node.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  try
  {
     Application->Initialize();
     Application->CreateForm(__classid(TForm1), &Form1);
     Application->Run();
  }
  catch (Exception &exception)
  {
     Application->ShowException(&exception);
  }
  return 0;
}
//---------------------------------------------------------------------------
