#include "MaskFile.h"

#include <string>
using namespace std;
               
//extern TForm1 *Form1;

bool mask_file(const char *in_file_name, const char *out_file_name, int mask_size, unsigned char *mask, TProgressBar *progressBar)
{
  if(mask_size<=0 || progressBar==NULL) return false;

	fstream in_file, out_file, mask_file;

	in_file.open(in_file_name,ios::in|ios::binary);
	out_file.open(out_file_name,ios::out|ios::binary);

	if(!in_file || !out_file) return false;

	char bch[1024];
	long count = 0;

  //{
    in_file.seekg(0,ios::end);
    int fpos = in_file.tellg(), step;
    in_file.seekg(0,ios::beg);
    progressBar->Max = fpos;
    step = fpos/1024;
    progressBar->Step = step;
  //}
  int read_sz;
	while((read_sz=in_file.readsome(bch,1024))>0)
	{
    for(int i=0;i<read_sz;i++)
    {
		  bch[i] ^= mask[count];
		  count = (count+1)%mask_size;
    }
		if(!out_file.write(bch,read_sz)) return false;

    //{
    //  fpos = in_file.tellg();
    //  if(fpos%step == 0)
    //  {
        progressBar->StepIt();
        Application->ProcessMessages();
        if(app_exit) return false;
    //  }
    //}
	}

	in_file.close();
	out_file.close();  

	mask_file.open((string(out_file_name)+"_mask.bin").c_str(),ios::out|ios::binary);
  if(mask_file)
    mask_file.write(mask,mask_size);
	mask_file.close();  

  progressBar->Position = progressBar->Max;

	return true;
}


bool mask_file(const char *in_file_name, int mask_size, unsigned char *mask, TProgressBar *progressBar)
{    
  if(mask_size<=0 || progressBar==NULL) return false;

	fstream in_file, mask_file;

	in_file.open(in_file_name,ios::in|ios::out|ios::binary);

	if(!in_file) return false;

	char bch;
	long count = 0, fpos, step; 

  //{
    in_file.seekg(0,ios::end);
    fpos = in_file.tellg();
    in_file.seekg(0,ios::beg);
    progressBar->Max = fpos;
    step = fpos/1000;
    progressBar->Step = step;
  //}

	while(in_file.read(&bch,1))
	{
		bch ^= mask[count];
		count = (count+1)%mask_size;
    fpos = in_file.tellg();
    in_file.seekp(fpos-1);
		if(!in_file.write(&bch,1)) return false;

    //{
      if(fpos%step == 0)
      {
        progressBar->StepIt();
        Application->ProcessMessages();
        if(app_exit) return false;
      }
    //}
	}

	in_file.close();

	mask_file.open((string(in_file_name)+"_mask.bin").c_str(),ios::out|ios::binary);
  if(mask_file)
    mask_file.write(mask,mask_size);
	mask_file.close();
                        
  progressBar->Position = progressBar->Max;
  
	return true;
}

