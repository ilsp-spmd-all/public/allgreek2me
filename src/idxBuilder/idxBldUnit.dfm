object Form1: TForm1
  Left = 244
  Top = 167
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 399
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxMask: TGroupBox
    Left = 8
    Top = 200
    Width = 481
    Height = 169
    Caption = 'Encrypt a file with xor masking'
    TabOrder = 1
    object Label4: TLabel
      Left = 8
      Top = 16
      Width = 40
      Height = 13
      Caption = 'Input file'
    end
    object Label5: TLabel
      Left = 8
      Top = 56
      Width = 48
      Height = 13
      Caption = 'Output file'
    end
    object Label6: TLabel
      Left = 8
      Top = 96
      Width = 59
      Height = 13
      Caption = 'Mask in Hex'
    end
    object LabelMaskCount: TLabel
      Left = 368
      Top = 118
      Width = 6
      Height = 13
      Caption = '0'
    end
    object EditFile2Mask: TEdit
      Left = 8
      Top = 32
      Width = 433
      Height = 21
      TabOrder = 0
      OnChange = TextBoxesChanged
    end
    object ButtonOpenFile2Mask: TButton
      Left = 448
      Top = 32
      Width = 25
      Height = 21
      Caption = '...'
      TabOrder = 1
      OnClick = ButtonOpenFile2MaskClick
    end
    object EditFileMasked: TEdit
      Left = 8
      Top = 72
      Width = 433
      Height = 21
      TabOrder = 2
      OnChange = TextBoxesChanged
    end
    object ButtonOpenOutMasked: TButton
      Left = 448
      Top = 72
      Width = 25
      Height = 21
      Caption = '...'
      TabOrder = 3
      OnClick = ButtonOpenOutMaskedClick
    end
    object ProgressBarMask: TProgressBar
      Left = 8
      Top = 144
      Width = 465
      Height = 16
      Min = 0
      Max = 100
      Smooth = True
      TabOrder = 4
    end
    object ButtonMask: TButton
      Left = 400
      Top = 112
      Width = 75
      Height = 21
      Caption = 'Mask'
      Enabled = False
      TabOrder = 5
      OnClick = ButtonMaskClick
    end
    object EditMask: TEdit
      Left = 8
      Top = 112
      Width = 353
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 64
      TabOrder = 6
      OnChange = OnMaskChanged
    end
    object CheckBoxMaskFileSame: TCheckBox
      Left = 64
      Top = 56
      Width = 145
      Height = 15
      Caption = 'Overwrite the input file'
      TabOrder = 7
      OnClick = CheckBoxMaskFileSameClick
    end
  end
  object GroupBoxBuild: TGroupBox
    Left = 8
    Top = 8
    Width = 481
    Height = 185
    Caption = 'Index from text file'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 88
      Height = 13
      Caption = 'Input dictionary file'
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 76
      Height = 13
      Caption = 'Output index file'
    end
    object Label3: TLabel
      Left = 8
      Top = 96
      Width = 47
      Height = 13
      Caption = 'Input type'
    end
    object EditDicFile: TEdit
      Left = 8
      Top = 32
      Width = 433
      Height = 21
      TabOrder = 0
      OnChange = TextBoxesChanged
    end
    object ButtonOpenDic: TButton
      Left = 448
      Top = 32
      Width = 25
      Height = 21
      Caption = '...'
      TabOrder = 1
      OnClick = ButtonOpenDicClick
    end
    object EditIdxFile: TEdit
      Left = 8
      Top = 72
      Width = 433
      Height = 21
      TabOrder = 2
      OnChange = TextBoxesChanged
    end
    object ButtonOpenOutIdx: TButton
      Left = 448
      Top = 72
      Width = 25
      Height = 21
      Caption = '...'
      TabOrder = 3
      OnClick = ButtonOpenOutIdxClick
    end
    object Select: TComboBox
      Left = 8
      Top = 112
      Width = 385
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
      Items.Strings = (
        'key(string) data(string) data_probability(double)'
        'key(string) data_probability(double)'
        'key(string) data_index(unsigned long) data_offset(unsigned long)'
        '')
    end
    object ProgressBarBuild: TProgressBar
      Left = 8
      Top = 144
      Width = 465
      Height = 16
      Min = 0
      Max = 100
      Smooth = True
      TabOrder = 5
    end
    object ButtonBuild: TButton
      Left = 400
      Top = 112
      Width = 75
      Height = 21
      Caption = 'Build'
      Enabled = False
      TabOrder = 6
      OnClick = ButtonBuildClick
    end
    object ProgressBarWrite: TProgressBar
      Left = 8
      Top = 163
      Width = 465
      Height = 16
      Min = 0
      Max = 100
      Smooth = True
      TabOrder = 7
    end
  end
  object ButtonExit: TButton
    Left = 8
    Top = 373
    Width = 481
    Height = 25
    Caption = 'Exit'
    TabOrder = 2
    OnClick = ButtonExitClick
  end
  object OpenDialogDicText: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'Text Files|*.txt|All Files|*.*'
    Title = 'Select dictionary text file'
    Left = 336
    Top = 200
  end
  object SaveDialogIdxFile: TSaveDialog
    DefaultExt = 'bin'
    Filter = 'Binary Files|*.bin|All Files|*.*'
    Title = 'Output binary index file'
    Left = 368
    Top = 200
  end
  object OpenDialogFile2Mask: TOpenDialog
    DefaultExt = 'bin'
    Filter = 'Binary Files|*.bin|Text Files|*.txt|All Files|*.*'
    Title = 'Input file to mask'
    Left = 400
    Top = 200
  end
  object SaveDialogMaskedFile: TSaveDialog
    DefaultExt = 'bin'
    Filter = 'Binary Files|*.bin|All Files|*.*'
    Title = 'Output masked file'
    Left = 432
    Top = 200
  end
end
