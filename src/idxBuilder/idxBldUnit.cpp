//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "idxBldUnit.h"
//---------------------------------------------------------------------------
#include "MaskFile.h"
#include "node.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
bool app_exit = false;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
  : TForm(Owner)
{
  Select->ItemIndex = 0;
  EditMask->Text = "";
} 

void __fastcall TForm1::CheckBoxMaskFileSameClick(TObject *Sender)
{
  EditFileMasked->Enabled = !EditFileMasked->Enabled;
  if(!EditFileMasked->Enabled)
    EditFileMasked->Color = clInactiveCaptionText;
  else
    EditFileMasked->Color = clWindow;
    
  TextBoxesChanged(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonExitClick(TObject *Sender)
{
  app_exit = true;  
  Application->Terminate();  
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonMaskClick(TObject *Sender)
{
  unsigned char mask[32], msbyte, lsbyte;
  int mask_size = 0;

#define CHAR2HEX(c) ((c>='0'&&c<='9')?(c-'0'):((c>='a'&&c<='f')?(c-'a'+0xA):((c>='A'&&c<='F')?(c-'A'+0xA):0)))

  //read hex mask
  AnsiString edit_mask = EditMask->Text.c_str();
  for(int i=1;i<=edit_mask.Length()-1;i+=2)
  {
    msbyte = CHAR2HEX(edit_mask[i])<<4;
    lsbyte = CHAR2HEX(edit_mask[i+1]);
    mask[mask_size++] = msbyte | lsbyte;
  }

  if(CheckBoxMaskFileSame->Checked || EditFile2Mask->Text == EditFileMasked->Text)
  {
    mask_file(EditFile2Mask->Text.c_str(), mask_size, mask, ProgressBarMask);
  }
  else
  {
    mask_file(EditFile2Mask->Text.c_str(),EditFileMasked->Text.c_str(), mask_size, mask, ProgressBarMask);
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonOpenDicClick(TObject *Sender)
{
  if(OpenDialogDicText->Execute()){
    EditDicFile->Text = OpenDialogDicText->FileName;
  };
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonOpenOutIdxClick(TObject *Sender)
{
  if(SaveDialogIdxFile->Execute()){
    EditIdxFile->Text = SaveDialogIdxFile->FileName;
  };
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonOpenFile2MaskClick(TObject *Sender)
{
  if(OpenDialogFile2Mask->Execute()){
    EditFile2Mask->Text = OpenDialogFile2Mask->FileName;
  };
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonOpenOutMaskedClick(TObject *Sender)
{
  if(SaveDialogMaskedFile->Execute()){
    EditFileMasked->Text = SaveDialogMaskedFile->FileName;
  };
}
//---------------------------------------------------------------------------


void __fastcall TForm1::OnMaskChanged(TObject *Sender)
{
  AnsiString mask1 = EditMask->Text, mask2 = "";
  int cpos = EditMask->SelStart;
  for(int i=1;i<=mask1.Length();i++)
  {
    if(mask1[i]>='0' && mask1[i]<='9'){mask2 += mask1[i]; continue;}
    if(mask1[i]>='A' && mask1[i]<='F'){mask2 += mask1[i]; continue;}
    if(i==cpos) cpos--;
  }
  EditMask->Text = mask2;
  EditMask->SelStart = cpos;

  LabelMaskCount->Caption = mask2.Length();

  TextBoxesChanged(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::TextBoxesChanged(TObject *Sender)
{
  ButtonBuild->Enabled = EditDicFile->Text.Length()>0 &&
                         EditIdxFile->Text.Length()>0 &&
                         EditDicFile->Text != EditIdxFile->Text;
  ButtonMask->Enabled = EditFile2Mask->Text.Length()>0 &&
                        EditMask->Text.Length()>1 &&
                        EditFileMasked->Text.Length()>0;
}
//---------------------------------------------------------------------------

void lower_string(string *s)
{
	for(int i = 0;i<s->size();i++)
	{
		if(isalnum((*s)[i]))
			(*s)[i] = tolower((*s)[i]);
	}
}

extern int node_counter;

void __fastcall TForm1::ButtonBuildClick(TObject *Sender)
{            
  node_counter = 0;

	fstream in, out;
	node *tree = new node();
	string key, data;
	double prob;
  unsigned long data_index, data_offset;
	long counter = 0, lprob;
	char buf[64];

	in.open(EditDicFile->Text.c_str(),ios::in);

	//
	if(!in)
	{
		return;
	}

  //{
    in.seekg(0,ios::end);
    int fpos = in.tellg(), step, prpos = 0;
    in.seekg(0,ios::beg);
    ProgressBarBuild->Max = fpos;
    step = fpos/1000;
    ProgressBarBuild->Step = step; 
    ProgressBarBuild->Position = ProgressBarBuild->Min;
  //}

  switch(Select->ItemIndex)
  {
  case 0:
  	while(in>>key>>data>>prob)
	  {
      counter++;
      //{
        fpos = in.tellg();
        if((fpos-prpos)>=step)
        {  
          prpos = fpos;
          ProgressBarBuild->StepIt();
          Application->ProcessMessages(); 
          if(app_exit) break;
        }
      //}
		  memcpy(buf,&prob,sizeof(double));
		  memcpy(buf+sizeof(double),(void*)data.c_str(),data.size() + 1);
		  tree->insert(key,data.size() + sizeof(double) + 1,buf);
	  }
    ProgressBarBuild->Position = ProgressBarBuild->Max;
    break;
  case 1:{
	  char *dummy = "0";
	  while(in>>key>>prob)
	  {   
      //{
        fpos = in.tellg();
        if((fpos-prpos)>=step)
        {
          prpos = fpos;
          ProgressBarBuild->StepIt();
          Application->ProcessMessages();
          if(app_exit) break;
        }
      //}
		  lower_string(&key);
		  counter++;
		  memcpy(buf,&prob,sizeof(double));
		  memcpy(buf+sizeof(double),(void*)dummy,2);
		  tree->insert(key,sizeof(double) + 2 + 1,buf);
	  }
    ProgressBarBuild->Position = ProgressBarBuild->Max;
    break;
  }
  case 2:{
	  char *dummy = "0";
	  while(in>>key>>data_index>>data_offset)
	  {   
      //{
        fpos = in.tellg();
        if((fpos-prpos)>=step)
        {
          prpos = fpos;
          ProgressBarBuild->StepIt();
          Application->ProcessMessages();
          if(app_exit) break;
        }
      //}
		  lower_string(&key);
		  counter++;
		  memcpy(buf,&data_index,sizeof(unsigned long));
		  memcpy(buf+sizeof(unsigned long),&data_offset,sizeof(unsigned long));
		  tree->insert(key,sizeof(unsigned long)+sizeof(unsigned long),buf);
	  }
    ProgressBarBuild->Position = ProgressBarBuild->Max;
    break;
  }
  }

	/*while(in>>key>>data>>data)
	{
		cout<<"\r"<<counter++;
		lower_string(&key);
		tree.insert(key,data.size() + 1,(void*)data.c_str());
	}*/

	in.close();

  try
  {
    ProgressBarWrite->Max = 1000;
    ProgressBarWrite->Step = 1;
    ProgressBarWrite->Position = ProgressBarBuild->Min;

	  if(!app_exit)
      tree->write2file(EditIdxFile->Text.c_str());
      
    ProgressBarWrite->Position = ProgressBarBuild->Max;
  }
  catch(string es)
  {
    if(es=="exit") {Application->Terminate(); return;}

    ::MessageBox(0,es.c_str(),"ERROR",MB_OK);
    delete tree;
    return;
  }

  delete tree;

}
//---------------------------------------------------------------------------

//Canvas->StretchDraw(ClientRect, TheGraphic); // Draw the graphic on the Canvas

void __fastcall TForm1::FormShow(TObject *Sender)
{
  TheGraphic = new Graphics::TBitmap(); // Create the bitmap object
  TheGraphic->LoadFromFile("logo2.bmp"); // Load the bitmap from a file

  ButtonExit->Caption = "Exit\0������";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
  //Brush->Style = bsClear;
  //BorderStyle = bsNone; 
}
//---------------------------------------------------------------------------

