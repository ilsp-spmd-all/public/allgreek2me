//---------------------------------------------------------------------------

#ifndef idxBldUnitH
#define idxBldUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
  TGroupBox *GroupBoxBuild;
  TLabel *Label1;
  TEdit *EditDicFile;
  TButton *ButtonOpenDic;
  TLabel *Label2;
  TEdit *EditIdxFile;
  TButton *ButtonOpenOutIdx;
  TComboBox *Select;
  TLabel *Label3;
  TProgressBar *ProgressBarBuild;
  TButton *ButtonBuild;
  TGroupBox *GroupBoxMask;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TEdit *EditFile2Mask;
  TButton *ButtonOpenFile2Mask;
  TEdit *EditFileMasked;
  TButton *ButtonOpenOutMasked;
  TProgressBar *ProgressBarMask;
  TButton *ButtonMask;
  TEdit *EditMask;
  TCheckBox *CheckBoxMaskFileSame;
  TButton *ButtonExit;
  TOpenDialog *OpenDialogDicText;
  TSaveDialog *SaveDialogIdxFile;
  TOpenDialog *OpenDialogFile2Mask;
  TSaveDialog *SaveDialogMaskedFile;
  TLabel *LabelMaskCount;
  TProgressBar *ProgressBarWrite;
  void __fastcall CheckBoxMaskFileSameClick(TObject *Sender);
  void __fastcall ButtonExitClick(TObject *Sender);
  void __fastcall ButtonMaskClick(TObject *Sender);
  void __fastcall ButtonOpenDicClick(TObject *Sender);
  void __fastcall ButtonOpenOutIdxClick(TObject *Sender);
  void __fastcall ButtonOpenFile2MaskClick(TObject *Sender);
  void __fastcall ButtonOpenOutMaskedClick(TObject *Sender);
  void __fastcall OnMaskChanged(TObject *Sender);
  void __fastcall TextBoxesChanged(TObject *Sender);
  void __fastcall ButtonBuildClick(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
  Graphics::TBitmap *TheGraphic;
public:		// User declarations
  __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
