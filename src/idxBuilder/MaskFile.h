#ifndef _MASKFILE_H_
#define _MASKFILE_H_
                      
#include <vcl.h>

#include <fstream>
using namespace std;

bool mask_file(const char *in_file_name, const char *out_file_name, int mask_size, unsigned char *mask, TProgressBar *progressBar);
bool mask_file(const char *in_file_name, int mask_size, unsigned char *mask, TProgressBar *progressBar);

extern bool app_exit;

#endif //_MASKFILE_H_
