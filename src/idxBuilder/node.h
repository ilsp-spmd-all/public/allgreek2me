#ifndef _NODE_H_
#define _NODE_H_

#include <vector>
#include <string>
#include <iostream>

using namespace std;

#define DECRYPTION_MASK 178


extern unsigned char *mask;
extern int mask_length, mask_srand;

class node
{
	string child_keys;
	int data_length;
	vector<node*> children;

	void *data;

	long stream_pos;

	friend ostream& operator<< ( ostream& os, node& dt );

  void inline build_mask();

public:
	node();
	~node();               

  long count_children();

	void insert(string key, int data_length, void *data);

	void write2file(const char *filename);
	void trunc();
};

ostream& operator<< ( ostream& os, node& dt );

#endif //_NODE_H_