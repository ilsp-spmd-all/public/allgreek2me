#include "node.h"
#include <stdlib.h>
#include <fstream>

using namespace std;

//#define DISCARD_DUBLICATES

unsigned char *mask = NULL;
int mask_length, mask_srand;
int node_counter = 0;

node::node()
{
	//child_keys = string("");
  child_keys.reserve(1);
  children.reserve(1);
	data_length = 0;
	data = NULL;
	stream_pos = 0;

  if(mask==NULL)
  {
    mask_length = 27;
    mask_srand = 11;
    mask = new unsigned char[mask_length];
    build_mask();
  }

  node_counter++;
}

node::~node()
{
	for(int i=0;i<child_keys.size();i++)
		delete children[i];

  if(data!=NULL)
    free(data);
  
  if(mask!=NULL)
  {
    delete[] mask;
    mask = NULL;
  }
}

void node::build_mask()
{
  srand(mask_srand);
	for(int i=0;i<mask_length;i++)
    mask[i] = (unsigned char)(rand()%256);

  try{
    fstream mask_out;
    mask_out.open("bin_hex_mask_used.bin",ios::out | ios::binary | ios::trunc);
	  mask_out.write((const char *)mask,mask_length);
  }
  catch(...){}
}

void node::insert(string key, int dl, void *dt)
{
	if(key.size()==0)
	{
		void *tmp = malloc(data_length+dl);

		#ifdef DISCARD_DUBLICATES
		if(data_length>0) return;
		#endif

		if(data_length>0)
			memcpy(tmp,data,data_length);
		memcpy((char*)tmp+data_length,dt,dl);
		data_length += dl;
    if(data!=NULL)
      free(data);
		data = tmp;
		return;
	}

	char chkey = key[0];
	int count = child_keys.size(), pos = 0;
	node *child;

	if(child_keys.size()==0)
	{
		child_keys += chkey;
		child = new node();
		children.push_back(child);
		child->insert(key.substr(1),dl,dt);
		return;
	}

	pos = child_keys.find(chkey);

	if(pos!=string::npos)
	{
		children[pos]->insert(key.substr(1),dl,dt);
		return;
	}

  //!memory economy
  //child_keys.resize(child_keys.size()+1);
  //children.resize(children.size()+1);

	pos = 0;
	vector<node*>::iterator it = children.begin();
	child = new node();

	while(chkey>child_keys[pos])
	{
		pos++;
		it++;
		if(pos == count) break;
	}

	child_keys.insert(pos,1,chkey);
	children.insert(it,child);
	child->insert(key.substr(1),dl,dt);
}

void node::trunc()
{
}

long node::count_children()
{
  long ret;

  ret = child_keys.size();

  if(!ret) return 0;

	for(int i=0;i<child_keys.size();i++)
	{
		ret += children[i]->count_children();
	}

  return ret;
}

void node::write2file(const char *filename)
{
	fstream out;
	out.open(filename, ios::out | ios::binary | ios::trunc);

  if(!out)
  {
    throw string("Could not open file for writing!");
  }
	
	out<<*this;

	out.close();
}

long countout = 0;

bool must_encrypt = true;

inline void os_write(ostream& os, const char *source, int length)
{
	if(length <= 0 || source == NULL)
		//cerr<<"os_write() invalid input!\n";
    throw string("os_write() invalid input!");

	unsigned char *buf = (unsigned char *)malloc(length);
	if(must_encrypt)
	{
    int pos = (int)os.tellp();

		for(int i=0;i<length;i++)
		{
			buf[i] = mask[(pos+i)%mask_length]^(unsigned char)source[i];
		}
	}
	os.write((const char *)buf,length);
	free(buf);
}


#define PROGRESSBAR Form1->ProgressBarWrite
#ifdef PROGRESSBAR
  #include "idxBldUnit.h"
  extern bool app_exit;
  int out_counter=0;
#endif //PROGRESSBAR

ostream& operator<< ( ostream& os, node& dt )
{
	dt.stream_pos = os.tellp();

	if(dt.stream_pos < 0)
		//cerr<<"\nos.tellp() < 0!\n";
    throw string("os.tellp() < 0!");

	if(dt.stream_pos%4 != 0)
		//cerr<<"\nAlignement of 4 bytes lost!\n";
    throw string("Alignement of 4 bytes lost!");

	int count = dt.child_keys.size(), pos = 0;
	char dummy[4] = {0,0,0,0};

	//write the number of childern
	os_write(os, (char*)&count,sizeof(int));

	//write the keys for childern
  if(count>0)
  	os_write(os, (char*)dt.child_keys.c_str(),count);
	//align to 4 bytes
	if(count%4 != 0)
		os_write(os, dummy,4 - count%4);

	long chpos = os.tellp();
	int i;
	//leave place for children's offsets
	for(i=0;i<count;i++)
		os_write(os, (char*)&dt.stream_pos,sizeof(long));

	//write the data length
	os_write(os, (char*)&dt.data_length,sizeof(int));

	//write the data if any
	if(dt.data_length>0)
	{
		os_write(os, (char*)dt.data,dt.data_length);
    //todo if(dt.data_length%4)
		os_write(os, dummy,4 - dt.data_length%4);
	
		//!!cout<<'\r'<<countout++;
	}

	//write the the children
	//node child;
	for(i=0;i<count;i++)
	{
		//child = *(dt.children[i]);
		os<<*(dt.children[i]);//child;
	}

	//write the children's offsets
	os.seekp(chpos);
	for(i=0;i<count;i++)
		os_write(os, (char*)&dt.children[i]->stream_pos,sizeof(long));

	os.seekp(0, ios::end);

  #ifdef PROGRESSBAR                
  out_counter++;
  if(out_counter>=node_counter/1000){
    out_counter = 0;
    PROGRESSBAR->StepIt();
    Application->ProcessMessages();
    if(app_exit) throw string("exit");
  }                 
  #endif //PROGRESSBAR

	return os;
}