#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#ifdef _WIN32
#include <windows.h>

#ifdef __BORLANDC__
#include <vcl.h>
#endif

#pragma hdrstop
#endif

//#include <ctype.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "sstring/sstring.h"
#include <string>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <strstream>
#include <fstream>  

#include "Rules.h"    
#include "CustomDic.h"
#include "Greek2Greeklish.h"
#include "filemapper.h"

using namespace std;

#ifdef _WIN32
extern HINSTANCE hModuleInstance;
#endif

extern int lcount;

namespace ILSP_GREEKLISH{

void lower_string(sstring *s);     
void lower_string_greek(sstring *s);  
void lower_destress_greek(sstring *s);

//destination directory for resource files on non WIN32 platforms
#ifndef _WIN32
	void SetResoucePath(const char *path);
#endif

char * GetResource(unsigned long resID, const char* lpType, bool mask = true);

sstring GetModulePath();

void CallbackFunction(void *buf, long size);

extern bool initialized;

extern FileMapper *fileMapperGr, *fileMapperEn;
extern Rules *rules;
extern CustomDic *customDic;
extern CustomEngDic *customEngDic;
extern CustomDicExceptions *customDicExceptions;
extern Greek2Greeklish *greek2Greeklish;

extern float  THRESHOLD_GR_BACK_FOUND,
              THRESHOLD_GR_DIC_FOUND,
              THRESHOLD_GR_BACK_NOT_FOUND,
              THRESHOLD_TRIPHON,
              THRESHOLD_DIFF,
              THRESHOLD_UNSTRESS;

bool InitRules();

//#define _GR_INDEX_FILENAME_ "index.bin"
//#define _EN_INDEX_FILENAME_ "vocab_english_prob.bin" 
#define _GR_INDEX_FILENAME_ "allgreek1.bin"
#define _EN_INDEX_FILENAME_ "allgreek2.bin"

#ifdef ALLGREEK_CHECKVOLUME
#ifdef _WIN32
  #define MASK_TEXT_WITH_VOLDIFF(X) \
  char ModuleNameBuffer[256], VolumeNameBuffer[256], FileSystemNameBuffer[256];\
  unsigned long VolumeSerialNumber, MaximumComponentLength, FileSystemFlags, RegVolumeSerialNumber;\
  srand(time(NULL));\
  VolumeSerialNumber = 1;\
	DWORD hres = GetModuleFileName(\
		::GetModuleHandle(NULL),\
		ModuleNameBuffer,\
		255\
	);\
  if(hres && GetVolumeInformation(\
    (ExtractFileDrive(ModuleNameBuffer)+'\\').c_str(),\
    VolumeNameBuffer,\
    255,\
    &VolumeSerialNumber,\
    &MaximumComponentLength,\
    &FileSystemFlags,\
    FileSystemNameBuffer,\
    255\
  )){\
    VolumeSerialNumber ^= VolumeSerialNumber>>1;\
  }\
  char szValue[128];\
	memset(szValue, 0, sizeof(szValue));\
	DWORD dwSize = sizeof(szValue);\
	DWORD dwType = REG_SZ;\
	HKEY hKey;\
	::RegOpenKeyEx(\
		HKEY_LOCAL_MACHINE,\
		"SOFTWARE\\ILSP\\AllGreek",\
		0,\
		KEY_ALL_ACCESS,\
		&hKey\
	);\
	::RegQueryValueEx(\
		hKey,\
		"Data",\
		0,\
		&dwType,\
		(LPBYTE)szValue,\
		&dwSize\
	);\
	::RegCloseKey(hKey);\
  if(strlen(szValue)){\
    szValue[11] = '\0';\
    sscanf(szValue+3,"%x",&RegVolumeSerialNumber);\
    VolumeSerialNumber ^= RegVolumeSerialNumber;\
  }\
  if(VolumeSerialNumber) VolumeSerialNumber = rand() ^ rand()<<16;\
  char msgerror[] = {'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,' '-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'!'-100,'\n'-100};\
  sstring msgerrorstr = msgerror;\
  for(unsigned i=0;i<msgerrorstr.size();i++) msgerrorstr[i] = msgerrorstr[i]+100;\
  for(unsigned i=0;i<X.size();i++)\
    X[i] = X[i]^*(((char*)&VolumeSerialNumber)+i%4);\
  if(VolumeSerialNumber)\
    X = msgerrorstr;\

#else //_WIN32   
  #define MASK_TEXT_WITH_VOLDIFF(X) \

#endif //_WIN32

#else //ALLGREEK_CHECKVOLUME
  #define MASK_TEXT_WITH_VOLDIFF(X) \

#endif //ALLGREEK_CHECKVOLUME


#ifdef ALLGREEK_CHECK_LICENSE
#ifdef _WIN32
extern void __destroy();

#define DEFINE_MASK_TEXT_WITH_LIC_DIFF
#ifndef DEFINE_MASK_TEXT_WITH_LIC_DIFF
inline void MASK_TEXT_WITH_LIC_DIFF(sstring &X){
  unsigned i;
  std::string accountID = Lic_RegGetStringValue(
		HKEY_LOCAL_MACHINE,
    "SOFTWARE\\ILSP\\Licenses\\" APPNAME,
		"AccountID");
  std::string regActivationCode = Lic_RegGetStringValue(
		HKEY_LOCAL_MACHINE,
    "SOFTWARE\\ILSP\\Licenses\\" APPNAME,
		"ActivationCode");
  std::string machineID = getMachineID();
  std::string applicationID(APPNAME);
  unsigned char activationCode[2*__N_SIZE__];
  getActivationCode(
    (unsigned char*)accountID.c_str(),
    (unsigned char*)applicationID.c_str(),
    (unsigned char*)machineID.c_str(),
    activationCode);
  sstring::size_type X_size = X.size();
  for(i=0;i<X_size;i++)
    X[i] = X[i]^activationCode[i%(__N_SIZE__/2)];
  char msgerror[] = {
      '�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,
      ' '-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,
      '�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,
      '�'-100,'!'-100,'\n'-100,-100};
  sstring msgerrorstr = msgerror;  
  sstring::size_type msgerrorstr_size = msgerrorstr.size();
  for(unsigned i=0;i<msgerrorstr_size;i++) {
    msgerrorstr[i] = msgerrorstr[i]+100;
    msgerrorstr[i] = msgerrorstr[i]^regActivationCode[i%(__N_SIZE__/2)];
  }
  for(i=0;i<(__N_SIZE__/2);i++) {
    if(regActivationCode[i]!=activationCode[i]){
      X = msgerrorstr;
      X_size = msgerrorstr_size;
      __destroy();
    }
  }
  for(i=0;i<X_size;i++)
    X[i] = X[i]^regActivationCode[i%(__N_SIZE__/2)];
}
#else //DEFINE_MASK_TEXT_WITH_LIC_DIFF

  #define MASK_TEXT_WITH_LIC_DIFF(X)                                            \
  unsigned i;                                                            \
  std::string accountID = Lic_RegGetStringValue(                         \
		HKEY_LOCAL_MACHINE,                                                  \
    "SOFTWARE\\ILSP\\Licenses\\" APPNAME,                                \
		"AccountID");                                                        \
  std::string regActivationCode = Lic_RegGetStringValue(                 \
		HKEY_LOCAL_MACHINE,                                                  \
    "SOFTWARE\\ILSP\\Licenses\\" APPNAME,                                \
		"ActivationCode");                                                   \
  std::string machineID = getMachineID();                                \
  std::string applicationID(APPNAME);                                    \
  unsigned char activationCode[2*__N_SIZE__];                            \
  getActivationCode(                                                     \
    (unsigned char*)accountID.c_str(),                                   \
    (unsigned char*)applicationID.c_str(),                               \
    (unsigned char*)machineID.c_str(),                                   \
    activationCode);                                                     \
  sstring::size_type X_size = X.size();                                        \
  for(i=0;i<X_size;i++)                                                  \
    X[i] = X[i]^activationCode[i%(__N_SIZE__/2)];                        \
  char msgerror[] = {                                                    \
      '�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,   \
      ' '-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,   \
      '�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,'�'-100,   \
      '�'-100,'!'-100,'\n'-100,-100};                                    \
  sstring msgerrorstr = msgerror;                                        \
  sstring::size_type msgerrorstr_size = msgerrorstr.size();                    \
  for(unsigned i=0;i<msgerrorstr_size;i++) {                             \
    msgerrorstr[i] = msgerrorstr[i]+100;                                 \
    msgerrorstr[i] = msgerrorstr[i]^regActivationCode[i%(__N_SIZE__/2)]; \
  }                                                                      \
  for(i=0;i<(__N_SIZE__/2);i++) {                                        \
    if(regActivationCode[i]!=activationCode[i]){                         \
      X = msgerrorstr;                                                   \
      X_size = msgerrorstr_size;                                         \
      __destroy();                                                       \
    }                                                                    \
  }                                                                      \
  for(i=0;i<X_size;i++)                                                  \
    X[i] = X[i]^regActivationCode[i%(__N_SIZE__/2)];                     \

#endif //DEFINE_MASK_TEXT_WITH_LIC_DIFF

#else //not _WIN32
  #define MASK_TEXT_WITH_LIC_DIFF(X) \

#endif _WIN32
#endif

}//ILSP_GREEKLISH

#endif //_GLOBALS_H_

