#ifndef _MAPFILEREADER_H_
#define _MAPFILEREADER_H_

#include "Globals.h"

#define HISTORY_COUNT 16

namespace ILSP_GREEKLISH{

class MapFileReader{
	const unsigned char *source;
  long source_offset;

	int data_length;
  double firstProb;

  int mask_length, mask_srand;
  unsigned char *mask;
  void inline build_mask();

  //misc
  char history[HISTORY_COUNT+1];
  long history_offsets[HISTORY_COUNT];

  bool data_copied;
  void reset_source();
  inline void source_read(char *dest, int length);
  inline void copyData(void *buf);
  void searchKey(const char *key);
public:
  MapFileReader(const unsigned char *fname);
  ~MapFileReader();
                  
	bool findKey(const char *key);
  void getWordProbs(list<dic_word_prob*> &res);
  double getFirstProb();

  int getDataLegnth();
  void getData(char *buffer);
};

} //ILSP_GREEKLISH

#endif //_MAPFILEREADER_H_
 