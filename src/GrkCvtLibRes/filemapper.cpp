#include "filemapper.h"

#ifndef _WIN32
#include <unistd.h>
#endif

#include "sstring/sstring.h"
#include <string>
using namespace std;

namespace ILSP_GREEKLISH{

//------------------------------------------------------------------------------
FileMapper::FileMapper(const char *fullpathfilename)
  :data(NULL) 
#ifdef _WIN32
  ,hMap(NULL),hFile(NULL)
#endif
{
  sstring filename(fullpathfilename), mapname;


#ifdef _WIN32
  sstring::size_type pos = filename.find_last_of('\\');
  if(pos==sstring::npos) return;
#endif //_WIN32

  //!not working with iis (each process must have unique mapname) 
  //mapname = filename.substr(pos+1);
  mapname = filename;
  unsigned mnsz = mapname.size();
  for(unsigned i=0;i<mnsz;i++) if(mapname[i]=='\\') mapname[i]='_';

#ifdef _WIN32
  /*
  SECURITY_ATTRIBUTES sa;
  sa.nLength = sizeof(SECURITY_ATTRIBUTES);
  sa.lpSecurityDescriptor = NULL;
  sa.bInheritHandle = TRUE;
  */
  hMap = ::OpenFileMapping(FILE_MAP_READ, TRUE, mapname.c_str());
  if(hMap == NULL){
    hFile = CreateFile(filename.c_str(),
                       FILE_READ_DATA,//GENERIC_READ,
                       FILE_SHARE_READ,
                       NULL,
                       OPEN_EXISTING,
                       FILE_ATTRIBUTE_READONLY | FILE_FLAG_RANDOM_ACCESS,
                       NULL);
    if(hFile == INVALID_HANDLE_VALUE) {
      hFile = NULL; return;
    }
    hMap = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, mapname.c_str());
  }
  if(hMap == NULL){
    if(hFile) {CloseHandle(hFile); hFile=NULL;} return;
  }
  data = (unsigned char *)MapViewOfFile(hMap, FILE_MAP_READ, 0, 0, 0);
#else
  map_len = 0;
  int fildes = open(filename.c_str(),O_RDONLY);
  if(fildes>=0){
    struct stat fstats;
    if(fstat(fildes,&fstats)==0){
      map_len = fstats.st_size;
      data = (unsigned char*)mmap(0, map_len, PROT_READ, MAP_PRIVATE, fildes, 0);
    }
    close(fildes);
  }else{
    printf("Cannot open file %s\n",filename.c_str()); exit(1);
  }
#endif
}
//------------------------------------------------------------------------------
FileMapper::~FileMapper(){
#ifdef _WIN32
	if (data) {
		::UnmapViewOfFile(data);
		data = NULL;
	}
	if (hMap) {
		::CloseHandle(hMap);
		hMap = NULL;
	}
	if (hFile) {
		::CloseHandle(hFile);
		hFile = NULL;
	}
#else //_WIN32
  if(data!=NULL)
    munmap(data,map_len);
#endif //_WIN32
}
//------------------------------------------------------------------------------


} //ILSP_GREEKLISH

