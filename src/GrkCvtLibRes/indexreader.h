#ifndef _INDEXREADER_H_
#define _INDEXREADER_H_

#include "Globals.h"

#define DECRYPTION_MASK 178

namespace ILSP_GREEKLISH{

class IndexReader
{
protected:
	ifstream source;
  sstring filename;
	int data_length;

	char keys[255];
	int offsets[255];

	bool must_decrypt;

	bool source_ok;
  
  double firstProb;
  bool firstProbRead;

  int mask_length, mask_srand;
  unsigned char *mask;
  void inline build_mask();

  void reset_source();

public:
	IndexReader(const char *fname); 
	~IndexReader();

	double searchKey(const char *key);
	void copyData(void *buf);

	inline void setDecrypt(bool d) {must_decrypt = d;}
	//inline void decrypt(unsigned char *buf, short length, unsigned char mask = DECRYPTION_MASK);
	inline void source_read(char *dest, int length);
};

class WordProbReader : public IndexReader
{
public:
	WordProbReader(const char *fname):IndexReader(fname){}
	inline bool findKey(const char *key) {return searchKey(key)>0.0;}
	void getWordProbs(list<dic_word_prob*> &res);

	double getFirstProb();
};

class StringReader : public IndexReader
{
public:
	StringReader(const char *fname):IndexReader(fname){}
	inline bool findKey(const char *key) {return source_ok && searchKey(key)>0.0;}
	sstring getString();
};


} //ILSP_GREEKLISH

#endif //_INDEXREADER_H_


