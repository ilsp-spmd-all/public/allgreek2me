//---------------------------------------------------------------------------
#ifdef _WIN32
#include <windows.h>
#else
#include <stdlib.h>
#endif

//---------------------------------------------------------------------------
#ifdef _WIN32
extern HINSTANCE hModuleInstance;
#endif

//---------------------------------------------------------------------------
#include "GrkCvt.h"
#include "GrkCvtLib.h"

using namespace ILSP_GREEKLISH;

//---------------------------------------------------------------------------
const char * get_module_path(){
  try{
    return __get_module_path();
  }
  catch(...){return NULL;}
}
//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void) greeklish_destroy()
{
  try{
    __destroy();
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void*)  greeklish_convert(const char * source)
{
  try{
    return (void*)__convert(source);
  }
  catch(...){return NULL;}
}

//---------------------------------------------------------------------------
#ifdef ALLGREEK_LM
extern_C  GrkCvtDeclSpec(void*)  greeklish_convert_lm(const char * source)
{
  try{
    return (void*)__convert_lm(source);
  }
  catch(...){return NULL;}
}
#endif //ALLGREEK_LM

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void)  greeklish_release_handle(void* cpsz)
{
  try{
    return __release_pchar((const char *)cpsz);
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(const char *)  greeklish_get_text(void* handle)
{
  try{
    return (const char *)handle;
  }
  catch(...){}
}
//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void)  greeklish_convert_file(const char *source, const char *dest)
{
  try{
    __convert_file(source, dest);
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void*)  greeklish_elot_gr2grl(const char *source)
{
  try{
    return (void*)__elot_gr2grl(source);
  }
  catch(...){return NULL;}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void*)  greeklish_custom_gr2grl(const char *source)
{
  try{
    return (void*)__custom_gr2grl(source);
  }
  catch(...){return NULL;}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void)  greeklish_convert_gr2grl_file(const char *source, const char *dest)
{
  try{
    __convert_gr2grl_file(source, dest);
  }
  catch(...){}
}


//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void*)  greeklish_analyse(const char * source)
{
  try{
    return (void*)__convert_analyse(source);
  }
  catch(...){return NULL;}
}

//---------------------------------------------------------------------------
extern_C void load_custom_gr2grl(){
  try{
    __load_custom_gr2grl();
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C void save_custom_gr2grl(){
  try{
    __save_custom_gr2grl();
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C void import_custom_gr2grl(const char *rules_str){
  try{
    __import_custom_gr2grl(rules_str);
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C bool is_valid_gr2grl_rule(const char *rule){
	try{
    return __is_valid_gr2grl_rule((rule));
  }
  catch(...){return false;}
}

//---------------------------------------------------------------------------
extern_C const char* export_rules(bool custom){
  try{
    return __export_rules(custom);
  }
  catch(...){return NULL;}
}

//---------------------------------------------------------------------------

//#define HELP_FUNCS

#ifdef HELP_FUNCS
extern_C  GrkCvtDeclSpec(void)  clear_file(const char *source, const char *dest, const char *r)
{
  try{
    __clear_file(source, dest, r);
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void)  engfngr(const char *source, const char *dest, bool prob_exists)
{
  try{
    __engfngr(source, dest, prob_exists);
  }
  catch(...){}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(void) word_phonprob(const char *source, const char *dest)
{
  try{
    __word_phonprob(source, dest);
  }
  catch(...){}
}

//---------------------------------------------------------------------------
#endif //HELP_FUNCS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(bool)  greeklish_init(const char *key)
{
  try{
    return __unlockit(key?key:"INNOETICSG2GENG");
  }
  catch(...){return false;}
}

#ifdef _WIN32
//---------------------------------------------------------------------------
#pragma argsused
BOOL WINAPI DllMain(HINSTANCE hInstDLL, DWORD dwReason, LPVOID lpvReserved) {

  //setlocale(LC_ALL,"el_GR.iso88597");
	switch (dwReason) {
		case DLL_PROCESS_ATTACH:
      hModuleInstance = hInstDLL;
      greeklish_init(NULL);
			break;
		case DLL_PROCESS_DETACH:
      greeklish_destroy();
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
	}

	return 1;
}
//---------------------------------------------------------------------------
#else //_WIN32

//---------------------------------------------------------------------------
#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
__attribute__((constructor))
void on_load(void) {
  char binpath[5000];
  Dl_info dl_info;
  dladdr((const void*)on_load, &dl_info);
  realpath(dl_info.dli_fname, binpath);
  int pos = 0, spos = 0;
	while(binpath[pos++]) if(binpath[pos-1]=='/') spos = pos;
  binpath[spos] = '\0';
  __set_module_path(binpath);
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(const char *) greeklish_getpath(void){
  try{
    return get_module_path();
  }
  catch(...){return NULL;}
}

//---------------------------------------------------------------------------
extern_C  GrkCvtDeclSpec(int) greeklish_fini(void){
  try{
    __destroy();
    return 1;
  }
  catch(...){return 0;}
}

#endif //_WIN32
