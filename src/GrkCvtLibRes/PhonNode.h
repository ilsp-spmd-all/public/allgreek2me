#ifndef _PHONNODE_H_
#define _PHONNODE_H_

#include "Rules.h"
#include "Globals.h"

namespace ILSP_GREEKLISH{

//Kombos ths domhs dentrou gia thn anaptyxh twn pi8anwn metagrafwn greeklish -> phoneme
class PhonNode
{
	vector<PhonNode*> pointers;	//komboi paidia
	sstring *input;						//lexh eisodou (oloklhrh)
	sstring phoneme;					//to phonhma pou antistoixei ston kombo
  sstring greek;           //to antistoixo ellhniko pou antistoixei ston kombo
	sstring phonemes_from_start;					//to mexri stigmhs apotelesma
	sstring letters;					//to tmhma eisodou pou antistoixei ston kombo
	int pos;								//8esh pou prepei na bre8ei to phonhma gia ton kombo
  unsigned int limit;


	void setPhoneme(sstring phon) {phoneme = phon;}
	void setLetters(sstring lett) {letters = lett;}
	inline void calcGreek();

public:
  bool empty;
  
	PhonNode(sstring *in, int p = 0, int limit = 10000);
	~PhonNode();
	void build();
	//void purge();
	void build_pointers();
	//void transcribe(vector<sstring> &res, sstring s = "");
	void getWordProbs(list<word_prob*> &res, sstring ph_till_now = "",
                    sstring gr_till_now = "", double prob = 1.0, int rule_count = 0);
	//void buildrules();

  void setLimit(int lim) {limit = lim;}

	bool addChild(sstring *input, int p, sstring phoneme, sstring letters);

  int getWordProbsRec(list<word_prob> *res,
                      sstring *input,
                      int pos = 0,
                      sstring ph_till_now = "",
                      sstring gr_till_now = "",
                      int rule_count = 0,
                      double rules_prob = 1.0,
                      double triphons_prob = 0.0,
                      int limit = 10000);
};

}//ILSP_GREEKLISH

#endif //_PHONNODE_H_

