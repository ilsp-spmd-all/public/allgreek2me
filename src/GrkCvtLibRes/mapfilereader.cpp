#include "mapfilereader.h"

#include "sstring/sstring.h"
#include <string>

using namespace std;

namespace ILSP_GREEKLISH{

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
MapFileReader::MapFileReader(const unsigned char *src)
{
  source_offset = 0;
  source = src;
  data_copied = true;

  mask_length = 27;
  mask_srand = 11;
  try{
    mask = new unsigned char[mask_length];
  }
  catch(std::bad_alloc){
		source = NULL;
    return;
  }

  build_mask();

  for(int i=0;i<HISTORY_COUNT;i++)
    history[i] = '\0';
}

MapFileReader::~MapFileReader(){
  if(mask) delete[] mask;
}

void MapFileReader::build_mask()
{
#ifdef __BORLANDC__
  srand(mask_srand);
	for(int i=0;i<mask_length;i++)
    mask[i] = (unsigned char)(rand()%256);
#else //__BORLANDC__
  int i = 0;
  mask[i++] = 0x16;
  mask[i++] = 0xCD;
  mask[i++] = 0xDD;
  mask[i++] = 0x42;
  mask[i++] = 0x7F;
  mask[i++] = 0x06;
  mask[i++] = 0x54;
  mask[i++] = 0x7A;
  mask[i++] = 0x05;
  mask[i++] = 0x0A;
  mask[i++] = 0xD5;
  mask[i++] = 0xC1;
  mask[i++] = 0x9B;
  mask[i++] = 0xDA;
  mask[i++] = 0x1D;
  mask[i++] = 0x69;
  mask[i++] = 0xBE;
  mask[i++] = 0x6C;
  mask[i++] = 0xC6;
  mask[i++] = 0xE2;
  mask[i++] = 0xA8;
  mask[i++] = 0xC4;
  mask[i++] = 0x14;
  mask[i++] = 0xEE;
  mask[i++] = 0xB9;
  mask[i++] = 0x53;
  mask[i++] = 0xFE;
#endif //_WIN32
}

void MapFileReader::reset_source(){
  source_offset = 0;
}


void MapFileReader::source_read(char *dest, int length)
{
	if(source){
#ifdef _WIN32
    ::CopyMemory(dest, source + source_offset, length);
#else //_WIN32
    memcpy(dest, source + source_offset, length);
#endif //_WIN32
    for(int i=0;i<length;i++)
      dest[i] = mask[(source_offset+i)%mask_length]^dest[i]; 
    source_offset += length;
  }
} 

void MapFileReader::copyData(void *buf)
{
  if(data_length>0 && !data_copied)
  	source_read((char*)buf,data_length);
  data_copied = true;
}
 
bool MapFileReader::findKey(const char *key)
{
  searchKey(key);
  return data_length>0;
}

void MapFileReader::searchKey(const char *key)
{
  data_length = -1;
	if(!source) return;

  reset_source();

	int key_pos = 0, key_size = strlen(key), count, i;
	char keys[255];
	int offsets[255];

  //search first in history
  while(key_pos<HISTORY_COUNT &&
        key_pos<key_size &&
        history[key_pos] == key[key_pos]) key_pos++;
  if(key_pos>0){        
    source_offset = history_offsets[key_pos-1];
  }

	while(key_pos < key_size)
	{
		source_read((char*)&count,sizeof(int));
		source_read(keys,count + ((count%4)?(4-count%4):0));
		source_read((char*)offsets,count*sizeof(int));

		for(i=0;i<count;i++)
			if(keys[i]==key[key_pos]) break;

		if(i==count) return;

		source_offset = offsets[i]; 

    //keep history
    if(key_pos<HISTORY_COUNT){
      history[key_pos] = key[key_pos];
      history_offsets[key_pos] = source_offset;
      history[key_pos+1] = '\0';
    }
    
		key_pos++;
	}

	source_read((char*)&count,sizeof(int));
	source_offset += count*sizeof(int) + count + ((count%4)?(4-count%4):0);

	source_read((char*)&data_length,sizeof(int));
  data_copied = false;
}

void MapFileReader::getWordProbs(list<dic_word_prob*> &res)
{
	if(!source) return;

	char *buf;
  try{
    buf = new char[data_length];
  }
  catch(std::bad_alloc){
    return;
  }

	int dtpart, length = data_length, pos = 0;

	copyData(buf);
  dic_word_prob *tmp_word_prob;
	do
	{ 
    try{
      tmp_word_prob = new dic_word_prob(sstring(buf+pos+sizeof(double)),*(double*)(buf+pos));
    }
    catch(std::bad_alloc){
      tmp_word_prob = NULL;
    }

		if(tmp_word_prob) res.push_back(tmp_word_prob);
		dtpart = strlen(buf+pos+sizeof(double)) + 1;
		dtpart += sizeof(double);
		if(length<=dtpart) break;
		pos += dtpart;
		length -= dtpart;
	}while(length>4);

  firstProb = (*res.begin())->prob;

	delete[] buf;
}

double MapFileReader::getFirstProb()
{
	if(!source) return 0.0;     
	
	if(!data_copied){
    source_read((char*)&firstProb,sizeof(double));
    data_length -= sizeof(double);
  }

	return firstProb;
}

int MapFileReader::getDataLegnth()
{
  return data_length;
}

void MapFileReader::getData(char *buffer)
{
	if(!source) return;

	if(!data_copied){
	  copyData(buffer);
  }else{
    //error
    //assert(0);
  }
}

} //ILSP_GREEKLISH

