#include "sstring.h"

#ifndef USE_STL_STDSTRING

//#include <ctype.h> //for isalnum()
#include <stdio.h> //for sscaf()
#include <string.h> //for strstr etc.

////////////////////////////////////////////////////////////////
// Constructors
//
sstring::sstring()
	: buf_size(0)
  , buffer(0) {
}

sstring::sstring(const sstring& s)
	: buf_size(0)
  , buffer(0) {

	assign(s.buffer, 0);
}

sstring::sstring(const sstring& s, unsigned orig, unsigned n/*=npos*/)
	: buf_size(0)
  , buffer(0) {
  
  assign(s.buffer, orig, n);
}

sstring::sstring(const char *cp)
	: buf_size(0)
  , buffer(0) {

	assign(cp, 0);
}

sstring::sstring(const char *cp, unsigned orig, unsigned n/*=npos*/)
	: buf_size(0)
  , buffer(0) {

  assign(cp, orig, n);
}

sstring::sstring(char c)
	: buf_size(0)
  , buffer(0) {

  resize(2);
  buffer[0]=c;
}

sstring::sstring(char c, unsigned n)
	: buf_size(0)
  , buffer(0) {

  resize(n+1);
  memset(buffer, c, n);
}

////////////////////////////////////////////////////////////////
// Destructor
//
sstring::~sstring() {
	if (buffer) delete[] buffer;
}

////////////////////////////////////////////////////////////////
// Assignment
//
sstring& sstring::operator = (const sstring& s) {
	return assign(s.buffer, 0);
}

sstring& sstring::assign(const sstring& s, unsigned orig, unsigned n/*=npos*/) {
	return assign(s.buffer, orig, n);
}

sstring& sstring::assign(const char *cp) {
	return assign(cp, 0);
}

sstring& sstring::assign(const char *cp, unsigned orig, unsigned n/*=npos*/) {
	if (!cp) return reset();
	if (cp == buffer) return *this;
	if (n==npos) {
		resize(strlen(cp)-orig+1);
  	strcpy(buffer, &(cp[orig]));
  }
  else {
		resize(n+1);
	  strncpy(buffer, &(cp[orig]), n);
  }
  return *this;
}

////////////////////////////////////////////////////////////////
// Concatenation
//
sstring& sstring::append(const sstring& s) {
	return append(s.buffer, 0);
}       

sstring& sstring::append(const char c) {
  unsigned len = length();
	resize(length()+1);
  buffer[len]=c;
  buffer[len+1]=0;
  return *this;
}

sstring& sstring::append(const char *cp) {
	return append(cp, 0);
}

sstring& sstring::append(const char *cp, unsigned orig, unsigned n/*=npos*/) {
	if (n==npos) {
		//resize(buf_size+strlen(cp)-orig);
    resize(size()+strlen(cp)-orig);
	  strcat(buffer, &(cp[orig]));
  }
  else {
		resize(buf_size+n);
	  strncat(buffer, &(cp[orig]), n);
  }
  return *this;
}

sstring& sstring::append(const sstring& s, unsigned orig, unsigned n/*=npos*/) {
	return append(s.buffer, orig, n);
}      

sstring& sstring::prepend(const sstring& s) {
	return prepend(s.buffer, 0);
}

sstring& sstring::prepend(const sstring& s, unsigned orig, unsigned n/*=npos*/) {
	return prepend(s.buffer, orig, n);
}

sstring& sstring::prepend(const char *cp) {
	return prepend(cp, 0);
}

sstring& sstring::prepend(const char *cp, unsigned orig, unsigned n/*=npos*/) {
	sstring res(cp, orig, n);
  res += *this;
  return *this = res;
}


sstring& sstring::operator += (const sstring& s) {
	return append(s.buffer, 0);
}

sstring& sstring::operator += (const char *cp) {
	return append(cp, 0);
}

sstring& sstring::operator += (const char c) {
	return append(c);
}

sstring sstring::operator + (const sstring& s) const {
	return this->operator+(s.buffer);
}

sstring sstring::operator + (const char *cp) const {
	sstring res = *this;
  res.append(cp, 0);
	return res;
}

/*sstring& operator + (const char *cp, const sstring& s) {
  return sstring(cp)+s;
}*/

////////////////////////////////////////////////////////////////
// Comparison
//
int sstring::compare(const sstring& s) const {
	return compare(s, 0);
}

int sstring::compare(const sstring& s, unsigned orig, unsigned n) const {
	if (n==npos)
		return strcmp(buffer, s.buffer+orig);
  else
		return strncmp(buffer, s.buffer+orig, n);
}

bool sstring::operator == (const char *cp) const {
	return !strcmp(buffer, cp);
}

bool operator > (const sstring& s1, const sstring& s2) {
	return strcmp(s1.buffer, s2.buffer)>0;
}

bool operator < (const sstring& s1, const sstring& s2) {
	return strcmp(s1.buffer, s2.buffer)<0;
}

bool sstring::operator != (const char *cp) const {
	return !this->operator==(cp);
}

bool operator == (const char *cp, const sstring& s) {
	return s == cp;
}

bool operator != (const char *cp, const sstring& s) {
	return s != cp;
}

bool operator == (const sstring& s1, const sstring& s2) {
	return s1==s2.buffer;
}

bool operator != (const sstring& s1, const sstring& s2) {
	return s1!=s2.buffer;
}

////////////////////////////////////////////////////////////////
// Insertion at some position
//
sstring& sstring::insert(unsigned pos, const sstring& s) {
  return insert(pos, s, 0);
}

sstring& sstring::insert(unsigned pos, const sstring& s, unsigned orig, unsigned n/*=npos*/) {
	sstring res(*this, 0, pos);
  res.append(s, orig, n);
  res.append(*this, pos);
  return *this = res;
}

////////////////////////////////////////////////////////////////
// Removal
//
sstring& sstring::erase(unsigned pos) {
  //return *this = sstring(*this, 0, pos);
  buffer[pos] = '\0';
  return *this;
}

sstring& sstring::erase(unsigned pos, unsigned n) {
	sstring res(*this, 0, pos);
  res.append(*this, pos+n);
  return *this = res;
}

////////////////////////////////////////////////////////////////
// Replacement at some position
//
sstring& sstring::replace(unsigned pos, unsigned n, const sstring& s) {
	return replace(pos, n, s, 0);
}

sstring& sstring::replace(unsigned pos, unsigned n1, const sstring& s, unsigned orig, unsigned n2/*=npos*/) {
	sstring res(s, orig, n2);
  res.prepend(*this, 0, pos);
  res.append(*this, pos+n1);
  return *this = res;
}

////////////////////////////////////////////////////////////////
// Subscripting
//
char sstring::get_at(unsigned pos) {
	return buffer[pos];
}

void sstring::put_at(unsigned pos, char c) {
	buffer[pos] = c;
}

char sstring::operator[](unsigned pos) const {
	return buffer[pos];
}

char& sstring::operator[](unsigned pos) {
	return buffer[pos];
}

sstring sstring::operator()(unsigned start, unsigned len/*=npos*/) {
	return substr(start, len); 
}

////////////////////////////////////////////////////////////////
// Substring
//
sstring sstring::substr(unsigned pos, unsigned n/*=npos*/) const {
	return sstring(*this, pos, n); 
}

////////////////////////////////////////////////////////////////
// Miscellaneous
//
sstring& sstring::reset() {
	buffer[0] = '\0';
  buffer = 0;
  buf_size = 0;
  return *this;
}

unsigned sstring::length() const {
	return (buffer)?strlen(buffer):0;
}   

unsigned sstring::size() const {
	return (buffer)?strlen(buffer):0;
}

void sstring::copy(char *cb, unsigned n) {
	copy(cb, n, 0);
}

void sstring::copy(char *cb, unsigned n, unsigned pos) {
	strncpy(cb, buffer+pos, n);
}

const char *sstring::c_str() const {
	return buffer;
}

sstring& sstring::strip(StripType s, char c) {
	if (!buf_size) return *this;
	if (s==Leading || s==Both) {
		while (*buffer==c)
    	memmove(buffer, buffer+1, length());
  }
	if (s==Trailing || s==Both) {
		while (length()>0 && buffer[length()-1]==c)
			buffer[length()-1]=0;
  }
  return *this;
}

sstring& sstring::strip_spaces(StripType s) {
	if (!buf_size) return *this;
	if (s==Leading || s==Both) {
		while (myisspace(*buffer))
    	memmove(buffer, buffer+1, length());
  }
	if (s==Trailing || s==Both) {
		while (length()>0&&myisspace(buffer[length()-1])) {
			buffer[length()-1]=0;
    }
  }
  return *this;
}

unsigned sstring::hash() const {
	unsigned ret = 0;
	for (unsigned i=0; i<min<unsigned>(sizeof(unsigned), buf_size); i++)
  	((char*)&ret)[i] = buffer[i];
	return ret;
}

bool sstring::is_null() const {
	return (!buffer ? true : buffer[0]==0);
}

sstring& sstring::to_lower() {
	strlwr(buffer);
  return *this;
}

sstring& sstring::to_upper() {
	strupr(buffer);
  return *this;
}

////////////////////////////////////////////////////////////////
// Searching
//
unsigned sstring::find(const sstring& s) const {
	return find(s, 0);		
}

unsigned sstring::find(const sstring& s, unsigned pos) const {
	char *ptr = strstr(buffer+pos, s.buffer);
  return ptr ? ptr-buffer : npos;
}

unsigned sstring::rfind(const sstring& s) const {
	strrev(buffer);
  strrev(s.buffer);
  unsigned idx = find(s);
	strrev(buffer);
  strrev(s.buffer);
  return (idx==npos ? npos : length()-idx-s.length());
}

unsigned sstring::rfind(const sstring& s, unsigned pos) const {
	unsigned idx = rfind(s);
  return (idx==npos ? npos : (idx<pos ? npos : idx));
}

unsigned sstring::contains(const char c) const {
	char *ptr = strchr(buffer, c);
	return (ptr==0 ? npos : ptr-buffer);
}

unsigned sstring::contains(const char *pat) const {
  /*
	unsigned n = strcspn(buffer, pat);
  return (n==length() ? npos : n);
  */
  char *ptr = strstr(buffer, pat);
  return (ptr ? ptr-buffer : npos);
}

unsigned sstring::contains(const sstring& s) const {
	return contains(s.buffer);
}

////////////////////////////////////////////////////////////////
// Character set searching
//
unsigned sstring::find_first_of(const sstring& s) const {
	return find_first_of(s, 0);
}

unsigned sstring::find_first_of(const sstring& s, unsigned pos) const {
	char *ptr = strpbrk(buffer+pos, s.buffer);
	return (ptr==0 ? npos : ptr-buffer);
}

unsigned sstring::find_first_not_of(const sstring& s) const {
	return find_first_not_of(s, 0); 
}

unsigned sstring::find_first_not_of(const sstring& s, unsigned pos) const {
  char *ptr = buffer+pos;
	while (*ptr!=0) {
		if (strchr(s.buffer, *ptr)==0)
    	break;
    ptr++;
  }
	if (*ptr) return ptr-buffer;
  return npos;  
}

unsigned sstring::find_last_of(const sstring& s) const {
	strrev(buffer);
  unsigned pos = find_first_of(s);
	strrev(buffer);
  return (pos==npos ? npos : length()-pos-1);
}

unsigned sstring::find_last_of(const sstring& s, unsigned pos) const {
	sstring str(*this, pos);
  unsigned idx = str.find_last_of(s);
  return (idx==npos ? npos : pos+idx);
}

unsigned sstring::find_last_not_of(const sstring& s) const {
	strrev(buffer);
  unsigned pos = find_first_not_of(s);
	strrev(buffer);
  return (pos==npos ? npos : length()-pos-1);
}

unsigned sstring::find_last_not_of(const sstring& s, unsigned pos) const {
	sstring str(*this, pos);
  unsigned idx = str.find_last_not_of(s);
  return (idx==npos ? npos : pos+idx);
}

////////////////////////////////////////////////////////////////
// Stream member functions
//
istream& sstring::read_file(istream& is) {
	//Determine number of chars (bytes) remaining 
	long pos = is.tellg(), num;
  is.seekg(0, istream::end);
  num = is.tellg()-pos;
  is.seekg(pos);

  //Read data into memory
  char *data = new char[num+1];
  memset(data, 0, num+1);
  is.read(data, num);

  //Append to sstring
  reset();
	append(data);

  delete[] data;
	return is;
}

istream& sstring::read_line(istream& is) {
	char tempbuffer[1024];
  memset(tempbuffer, 0, sizeof(tempbuffer));
  reset();

  is.getline(tempbuffer, sizeof(tempbuffer));

  append(tempbuffer);

	return is;
}

istream& sstring::read_to_delim(istream& is, char delim) {
	char tempbuffer[1024];
  memset(tempbuffer, 0, sizeof(tempbuffer));
  reset();

  is.get(tempbuffer, sizeof(tempbuffer), delim);

  append(tempbuffer);

	return is;
}

istream& sstring::read_token(istream& is) {
	char tempbuffer[1024];
  memset(tempbuffer, 0, sizeof(tempbuffer));
  reset();

  char *ptr = tempbuffer;
  /*
  do {
  	is.get(*ptr);
    if (is.eof()) break;
    if (myisspace((int)*ptr)) {
			is.putback(*ptr);
      *ptr = 0;
      break;
    }
    ptr++;
  } while (true);
	*/
  //Read all leading spaces
  while (myisspace(is.peek()) && !is.eof()) is.get();
  //Read all nonspaces
  while (!myisspace(is.peek()) && !is.eof()) is.get(*ptr++);
  append(tempbuffer);

  return is;
}

////////////////////////////////////////////////////////////////
// Data conversion functions
//
sstring::sstring(int value)
	: buf_size(0)
  , buffer(0) {

	resize(16);
	sprintf(buffer, "%i", value);
}

int sstring::ToInt(int def) const {
	int res = def;
	sscanf(buffer, "%i", &res);
  return res;
}

////////////////////////////////////////////////////////////////
// Windows functions
//
#if defined( _Windows )
sstring::sstring(HINSTANCE instance, UINT id, int len)
	: buf_size(0)
  , buffer(0) {

  resize(len);
	::LoadString(instance, id, buffer, len);
}
#endif

////////////////////////////////////////////////////////////////
// Protected
//
void sstring::resize(unsigned sz) {
	unsigned new_size = ((sz/deltaSize)+1)*deltaSize;
	if (new_size==buf_size) return;

	char *new_buffer;
  new_buffer = new char[new_size];
	memset(new_buffer, 0, new_size);
  unsigned umin = (new_size<=buf_size) ? new_size : buf_size; 
  if (buf_size) strncpy(new_buffer, buffer, umin-1);
  if (buffer) delete[] buffer;
  buffer = new_buffer;
  buf_size = new_size;
}

#endif //USE_STL_STDSTRING




