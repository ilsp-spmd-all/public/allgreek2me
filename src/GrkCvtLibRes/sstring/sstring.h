#ifndef __SStringH__ //Sentry
#define __SStringH__

#define USE_STL_STDSTRING

#ifdef USE_STL_STDSTRING
#include <string>
using namespace std;
#define sstring string
#else

#define deltaSize	((unsigned)64)

#include <iostream.h>

class sstring {
	public:
    enum StripType { Leading, Trailing, Both };

  protected:
  	unsigned buf_size;
  	char *buffer;

	public:
    static const unsigned npos = -1;
  	////////////////////////////////////////////////////////////////
    // Constructors
    //
    sstring();

		sstring(const sstring& s);
    sstring(const sstring& s, unsigned orig, unsigned n = npos);

    sstring(const char *cp);
    sstring(const char *cp, unsigned orig, unsigned n = npos);

    sstring(char c);
    sstring(char c, unsigned n);

  	////////////////////////////////////////////////////////////////
    // Destructor
    //
    ~sstring();

  	////////////////////////////////////////////////////////////////
    // Assignment
    //
    sstring& operator = (const sstring& s);
    sstring& assign(const sstring& s, unsigned orig, unsigned n = npos);
    sstring& assign(const char *cp);
    sstring& assign(const char *cp, unsigned orig, unsigned n = npos);

  	////////////////////////////////////////////////////////////////
    // Concatenation
    //
    sstring& operator += (const sstring& s);
    sstring& operator += (const char *cp);
    sstring& operator += (const char c);
    sstring operator + (const sstring& s) const;
    sstring operator + (const char *cp) const;
    //friend sstring& operator + (const char *cp, const sstring& s);

    sstring& append(const sstring& s);
    sstring& append(const sstring& s, unsigned orig, unsigned n = npos);
    sstring& append(const char *cp);
    sstring& append(const char *cp, unsigned orig, unsigned n = npos);
    sstring& append(const char c);

    sstring& prepend(const sstring& s);
    sstring& prepend(const sstring& s, unsigned orig, unsigned n = npos);
    sstring& prepend(const char *cp);
    sstring& prepend(const char *cp, unsigned orig, unsigned n = npos);

  	////////////////////////////////////////////////////////////////
    // Comparison
    //
    int compare(const sstring& s) const;
    int compare(const sstring& s, unsigned orig, unsigned n = npos) const;

    bool operator == (const char *cp) const;
    bool operator != (const char *cp) const;

    friend bool operator == (const sstring& s1, const sstring& s2);
    friend bool operator == (const char *cp, const sstring& s);
    friend bool operator != (const sstring& s1, const sstring& s2);
    friend bool operator != (const char *cp, const sstring& s);

    friend bool operator >  (const sstring& s1, const sstring& s2);
    friend bool operator <  (const sstring& s1, const sstring& s2);

    /*
    friend int operator == (const sstring& s1, const sstring& s2);
    friend int operator != (const sstring& s1, const sstring& s2);
    friend int operator == (const sstring& s, const char *cp);
    friend int operator == (const char *cp, const sstring& s);

    friend int operator != (const sstring& s, const char *cp);
    friend int operator != (const char *cp, const sstring& s);

    friend int operator <  (const sstring& s1, const sstring& s2);
    friend int operator <  (const sstring& s, const char *cp);
    friend int operator <  (const char *cp, const sstring& s);

    friend int operator <= (const sstring& s1, const sstring& s2);
    friend int operator <= (const sstring& s, const char *cp);
    friend int operator <= (const char *cp, const sstring& s);

    friend int operator >  (const sstring& s1, const sstring& s2);
    friend int operator >  (const sstring& s, const char *cp);
    friend int operator >  (const char *cp, const sstring& s);

    friend int operator >= (const sstring& s1, const sstring& s2);
    friend int operator >= (const sstring& s, const char *cp);
    friend int operator >= (const char *cp, const sstring& s);
    */

  	////////////////////////////////////////////////////////////////
    // Insertion at some position
    //
    sstring& insert(unsigned pos, const sstring& s);
    sstring& insert(unsigned pos, const sstring& s, unsigned orig, unsigned n = npos);

  	////////////////////////////////////////////////////////////////
    // Removal
    //
    sstring& erase(unsigned pos = 0);
    sstring& erase(unsigned pos, unsigned n);

  	////////////////////////////////////////////////////////////////
    // Replacement at some position
    //
    sstring& replace(unsigned pos, unsigned n, const sstring& s);
    sstring& replace(unsigned pos, unsigned n1, const sstring& s, unsigned orig, unsigned n2 = npos);

  	////////////////////////////////////////////////////////////////
    // Subscripting
    //
    char get_at(unsigned pos);
    void put_at(unsigned pos, char c);

    char& operator[](unsigned pos);
    char operator[](unsigned pos) const;
    sstring operator()(unsigned start, unsigned len = npos);

		////////////////////////////////////////////////////////////////
    // Searching
    //
    unsigned find(const sstring& s) const;
    unsigned find(const sstring& s, unsigned pos) const;
    unsigned rfind(const sstring& s) const;
    unsigned rfind(const sstring& s, unsigned pos) const;

    unsigned contains(const char c) const;
    unsigned contains(const char *pat) const;
    unsigned contains(const sstring& s) const;

		////////////////////////////////////////////////////////////////
    // Substring
    //
    sstring substr(unsigned pos, unsigned n = npos) const;

		////////////////////////////////////////////////////////////////
    // Character set searching
    //
    unsigned find_first_of(const sstring& s) const;
    unsigned find_first_of(const sstring& s, unsigned pos) const;
    unsigned find_first_not_of(const sstring& s) const;
    unsigned find_first_not_of(const sstring& s, unsigned pos) const;
    unsigned find_last_of(const sstring& s) const;
    unsigned find_last_of(const sstring& s, unsigned pos) const;
    unsigned find_last_not_of(const sstring& s) const;
    unsigned find_last_not_of(const sstring& s, unsigned pos) const;

		////////////////////////////////////////////////////////////////
    // Miscellaneous
    //
    sstring& reset();
    unsigned length() const;
    unsigned size() const;
    void copy(char *cb, unsigned n);
    void copy(char *cb, unsigned n, unsigned pos);
    const char *c_str() const;
		sstring& strip(StripType s = Trailing, char c = ' ');
		sstring& strip_spaces(StripType s = Trailing);
    unsigned hash() const;
    bool is_null() const;
    sstring& to_lower();
    sstring& to_upper();

		////////////////////////////////////////////////////////////////
    // Stream member functions
    //
    istream& read_file(istream& is);
    istream& read_line(istream& is);
    istream& read_to_delim(istream& is, char delim = '\n');
    istream& read_token(istream& is);
    //istream& read_string(istream& is);


		////////////////////////////////////////////////////////////////
    // Data conversion functions
    //
    sstring(int value);
    int ToInt(int def = 0) const;

		////////////////////////////////////////////////////////////////
    // Windows functions
    //
    #if defined( _Windows )
    sstring(HINSTANCE instance, UINT id, int len = 255);
    #endif

  protected:
  	////////////////////////////////////////////////////////////////
    // Protected
    //
    void resize(unsigned sz);
};

#endif //USE_STL_STDSTRING

#endif //__SStringH__ sentry

