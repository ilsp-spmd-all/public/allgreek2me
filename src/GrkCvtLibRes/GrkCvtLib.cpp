//---------------------------------------------------------------------------
#include "GrkCvtLib.h"
#include "Greeklish2Greek.h"
#include "Greeklish2GreekLM.h"
//---------------------------------------------------------------------------
//#include <string.h>
#include "sstring/sstring.h"
#include <fstream>
#include <string>

#include <locale.h>

using namespace std;
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
int lcount = 0;
//todo
#ifdef INNOETICSTTS_LSC
#ifdef WIN32
#include "MachineID.h"
#else
#include "MachineIDLinux.h"
#endif
#include "ActivationCode.h"

std::map<std::string,std::pair<std::string,std::string> > licences;

#ifdef _WIN32
std::string RegGetStringValue(HKEY hMainKey, LPCTSTR lpSubKey, LPCTSTR lpValueName) {
	//Read the registry
	//
	static char szValue[128];
	memset(szValue, 0, sizeof(szValue));

	DWORD dwSize = sizeof(szValue);
	DWORD dwType = REG_SZ;

	HKEY hKey;
	::RegOpenKeyEx(
		hMainKey,
		lpSubKey,
		0,
		KEY_QUERY_VALUE, //KEY_ALL_ACCESS,
		&hKey
	);

	::RegQueryValueEx(
		hKey,
		lpValueName,
		0,
		&dwType,
		(LPBYTE)szValue,
		&dwSize
	);
	::RegCloseKey(hKey);

	return szValue;
}
//---------------------------------------------------------------------
void RegSetStringValue(HKEY hMainKey, LPCTSTR lpSubKey, LPCTSTR lpValueName, const char* lpValue) {
	//Read the registry
	//

	DWORD dwSize = strlen(lpValue);
	DWORD dwType = REG_SZ;

	HKEY hKey;
	::RegOpenKeyEx(
		hMainKey,
		lpSubKey,
		0,
		KEY_SET_VALUE,//KEY_ALL_ACCESS,
		&hKey
	);

	::RegSetValueEx(
		hKey,
		lpValueName,
		0,
		dwType,
		(LPBYTE)lpValue,
		dwSize
	);
	::RegCloseKey(hKey);
}

#define MASK_LEN 4
#define AC_LEN 8
void makeRPCClientActivationCode(
                      unsigned char *pszActivationCode,
                      unsigned char *pszClientActivationCode)
{
  int i;
  unsigned char buffer[MASK_LEN+AC_LEN/2];
  
  // string to bytes
  for(i=0;i<AC_LEN;i+=2){ 
    buffer[MASK_LEN+i/2] = ((pszActivationCode[i]-'0')<<4) + (pszActivationCode[i+1]-'0');
  }

  srand(*((int*)(buffer+MASK_LEN)));

  // rand mask
  for(i=0;i<MASK_LEN;i++) 
    buffer[i] = (unsigned char)rand();

  // mask
  for(i=0;i<AC_LEN;i+=2){ 
    buffer[MASK_LEN+i/2] ^= buffer[(i/2)%MASK_LEN];
  }
  
  // bytes to string
  for(i=0;i<MASK_LEN+AC_LEN/2;i++) {
    pszClientActivationCode[2*i] = HEXHIGHDIGIT(buffer[i]);
    pszClientActivationCode[2*i+1] = HEXLOWDIGIT(buffer[i]);
  }
  pszClientActivationCode[2*i] = 0;
}
// -----------------------------------------------------------------------
std::string getRegAccountID(const char *AppName)
{
  return RegGetStringValue(HKEY_LOCAL_MACHINE,
    (std::string("SOFTWARE\\Innoetics\\Licenses\\") + AppName).c_str(),
		"AccountID");
}

std::string getRegActivationCode(const char *AppName)
{
  return RegGetStringValue(HKEY_LOCAL_MACHINE,
    (std::string("SOFTWARE\\Innoetics\\Licenses\\") + AppName).c_str(),
		"ActivationCode");
}
#else
// -----------------------------------------------------------------------
std::string getRegAccountID(const char *AppName)
{
  std::string ret;
  std::map<std::string,std::pair<std::string,std::string> >::const_iterator it = licences.find(AppName);
  if(it!=licences.end())
    ret = it->second.first;
  return ret;
}

std::string getRegActivationCode(const char *AppName)
{
  std::string ret;
  std::map<std::string,std::pair<std::string,std::string> >::const_iterator it = licences.find(AppName);
  if(it!=licences.end())
    ret = it->second.second;
  return ret;
}
#endif

inline int str_compare(const char *ac, const char *rc)
{
  int d=0; do{ d = *rc - *ac;	} while(*ac++!=0 && *rc++!=0 && !d); lcount = d; return d;
}

inline bool checkLicense(const char *APPNAME, bool old = false)
{
  std::string machineID;
#ifdef _WIN32
  if(old)
    machineID = getMachineID();
  else
#endif
    machineID = getMachineID2();

  std::string applicationID(APPNAME);
  std::string accountID = getRegAccountID(APPNAME);
  std::string regActivationCode = getRegActivationCode(APPNAME);

  unsigned char activationCode[2*__N_SIZE__];
  getActivationCode(
    (unsigned char*)accountID.c_str(),
    (unsigned char*)applicationID.c_str(),
    (unsigned char*)machineID.c_str(),
    activationCode);
  if(str_compare(regActivationCode.c_str(), (const char*)activationCode)==0){
    return true;
  }
#ifdef _WIN32
  unsigned char rpcClientActivationCode[2*__N_SIZE__];
  makeRPCClientActivationCode(activationCode, rpcClientActivationCode);
  return (str_compare(regActivationCode.c_str(), (const char*)rpcClientActivationCode)==0);
#endif
  return false;
}


#else
inline bool checkLicense(const char *APPNAME)
{
  return true;
}

#endif //INNOETICSTTS_LSC

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

namespace ILSP_GREEKLISH {

bool bUnlocked = false;
const char *szKey = "INNOETICSG2GENG";


//---------------------------------------------------------------------------
void __destroy()
{ 
  if (rules){
	  delete rules;
    rules = NULL;
  }
	if (triphons_probs){
		delete triphons_probs;
    triphons_probs = NULL;
  }
	if (customDic){
		delete customDic;
    customDic = NULL;
  }
	if (customEngDic){
		delete customEngDic;
    customEngDic = NULL;
  }
	if (customDicExceptions){
		delete customDicExceptions;
    customDicExceptions = NULL;
  }
	if (greek2Greeklish){
		delete greek2Greeklish;
    greek2Greeklish = NULL;
  }
	if (fileMapperGr){
		delete fileMapperGr;
    fileMapperGr = NULL;
  }
	if (fileMapperEn){
		delete fileMapperEn;
    fileMapperEn = NULL;
  }
}
//---------------------------------------------------------------------------
//This will be used to lock the DLL so that it cannot be used by anybody else
bool __unlockit(const char *key) {
#ifdef _WIN32
	bUnlocked = !stricmp(key,szKey);
#else
  bUnlocked = !strcmp(key,szKey);
#endif
#ifdef INNOETICSTTS_LSC

  #ifndef _WIN32
  std::string licfn = GetModulePath();
  licfn += "licences.txt";
  std::ifstream ifsLic(licfn.c_str());
  if(ifsLic.is_open()){
    std::string app, key, code;
    while(ifsLic>>app>>key>>code) {
      licences[app] = make_pair(key, code);
      //std::cout<<app<<" "<<key<<" "<<code<<std::endl;
    }
  }
  #endif
  
  bUnlocked = checkLicense(key);
#endif
	if (bUnlocked && !rules)
  {
		bUnlocked = InitRules();
#ifdef _WIN32
    hModuleInstance = 0;
#endif
  }

	return bUnlocked;
}

//---------------------------------------------------------------------------
void __release_pchar(const char *cpsz){
  if(cpsz!=NULL)
    delete[] (char*)cpsz;
}

//---------------------------------------------------------------------------
char * __convert(const char *source) {
	sstring result;

	if (!bUnlocked || source==NULL)
		return NULL;

  try{
    Greeklish2Greek GrkObj;

	  result = GrkObj.convertTextCustom(source);

    char *ret = new char[result.size()+16];
    strcpy(ret,result.c_str());
  //::MessageBox(0,ret,"",MB_OK);
    return ret;
  }catch(...){
    return NULL;
  }
}

//---------------------------------------------------------------------------
char * __convert_analyse(const char *source){
	sstring result;

	if (!bUnlocked || source==NULL)
		return NULL;

  Greeklish2Greek GrkObj;

	GrkObj.convertAnalyse(source, result);

  char *ret = new char[result.size()+1];
  strcpy(ret,result.c_str());
  return ret;
}

#ifdef ALLGREEK_LM
//---------------------------------------------------------------------------  
char * __convert_lm(const char *source) {
	sstring result;

	if (!bUnlocked || source==NULL)
		return NULL;

  Greeklish2GreekLM GrkObj;

	GrkObj.convert(source, result);

  char *ret = new char[result.size()+1];
  strcpy(ret,result.c_str());
  return ret;
}
#endif //ALLGREEK_LM

//---------------------------------------------------------------------------  
char * __elot_gr2grl(const char *source) {
	sstring result;

	if (!bUnlocked || source==NULL)
		return NULL;

	result = greek2Greeklish->default_greek2greeklish(source);

  char *ret = new char[result.size()+1];
  strcpy(ret,result.c_str());
	return ret;
}

//---------------------------------------------------------------------------  
char * __custom_gr2grl(const char *source) {
	sstring result;

	if (!bUnlocked || source==NULL)
		return NULL;

  result = greek2Greeklish->custom_greek2greeklish(source);

  char *ret = new char[result.size()+1];
  strcpy(ret,result.c_str());
	return ret;
}

//---------------------------------------------------------------------------
void __convert_file(const char *source, const char *dest) {
	if (!bUnlocked)
	  return;
  //!Greeklish2Greek GrkObj;

  fstream in, out;

  in.open(source,ios::in);
  out.open(dest,ios::out);

  if(!in || !out) return;

  sstring line;
  char ch;

  const char *pcsz;

  while(in.get(ch))
  {
    line+=ch;

    if(ch=='\n')
    {
      pcsz = __convert(line.c_str());
      out<<pcsz;
      __release_pchar(pcsz);
      out.flush();
      line.erase();
    }

    /*char *leak = (char*)malloc(2048);
    leak[0] = 0;
    out<<leak;
    leak = NULL;*/
  }
  pcsz = __convert(line.c_str());
  out<<pcsz;
  __release_pchar(pcsz);

  in.close();
  out.close();
}

//---------------------------------------------------------------------------
void __convert_gr2grl_file(const char *source, const char *dest) {
	if (!bUnlocked)
	  return;

  fstream in, out;

  in.open(source,ios::in);
  out.open(dest,ios::out);

  if(!in || !out) return;

  sstring line;
  char ch;

  const char *pcsz;

  while(in.get(ch))
  {
    line+=ch;

    if(ch=='\n')
    {
      pcsz = __custom_gr2grl(line.c_str());
      out<<pcsz;
      __release_pchar(pcsz);

      out.flush();
      line.erase();
    }
  }
  pcsz = __custom_gr2grl(line.c_str());
  out<<pcsz;
  __release_pchar(pcsz);

  in.close();
  out.close();
}

//---------------------------------------------------------------------------
void __load_custom_gr2grl(){
	if (!bUnlocked)
	  return;

  greek2Greeklish->load_custom_gr2grl();
}

//---------------------------------------------------------------------------
void __save_custom_gr2grl(){
	if (!bUnlocked)
	  return;

  greek2Greeklish->save_custom_gr2grl();
}

//---------------------------------------------------------------------------
void __import_custom_gr2grl(const char *rules_str){
	if (!bUnlocked)
	  return;

  greek2Greeklish->import_custom_gr2grl(rules_str);
}

bool __is_valid_gr2grl_rule(const char *rule){
	if (!bUnlocked)
	  return false;

  return greek2Greeklish->is_valid_rule(rule);
}

//---------------------------------------------------------------------------
const char* __export_rules(bool custom){
  static sstring ret;
  ret = sstring("");
	if (!bUnlocked)
	  return ret.c_str();

  ret += greek2Greeklish->export_rules(custom);

  return ret.c_str();
}
//---------------------------------------------------------------------------

void __load_save_custom_gr_dic(const char *dic){ 
	if (!bUnlocked)
	  return;

  customDic->LoadSaveCustomDic(dic);
}   
//---------------------------------------------------------------------------

void __load_save_custom_en_dic(const char *dic){
	if (!bUnlocked)
	  return;

  customEngDic->LoadSaveCustomDic(dic);
}
//---------------------------------------------------------------------------

void __load_save_custom_ex_dic(const char *dic){
	if (!bUnlocked)
	  return;

  customDicExceptions->LoadSaveCustomDic(dic);
}
//---------------------------------------------------------------------------

const char * __get_module_path(){
  static sstring ret;

  //ret = sstring("");
  //if (bUnlocked)
  if (ret.size()==0)
	  ret += GetModulePath();

  return ret.c_str();
}
//---------------------------------------------------------------------------
#ifndef _WIN32
void __set_module_path(const char *path){
  SetResoucePath(path);
}
#endif
//---------------------------------------------------------------------------

//#define HELP_FUNCS
#ifdef HELP_FUNCS
//---------------------------------------------------------------------------
void __clear_file(const char *source, const char *dest, const char *r) {
	if (!bUnlocked)
	  return;

  Greeklish2Greek GrkObj;

  fstream in, out, rest;

  in.open(source,ios::in);
  out.open(dest,ios::out);
  rest.open(r,ios::out);

  if(!in || !out || !rest) return;

  sstring key, data;
  double prob;
	while(in>>key>>data>>prob)
	{
    if(rules->wordprob_triphons_exist(key))
      out<<key<<'\t'<<data<<'\t'<<prob<<endl;
    else
      rest<<key<<'\t'<<data<<'\t'<<prob<<endl;
	}

  in.close();
  out.close();
  rest.close();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __engfngr(const char *source, const char *dest, bool prob_exists) {
	if (!bUnlocked)
	  return;

  Greeklish2Greek GrkObj;

  fstream in, out;

  in.open(source,ios::in);
  out.open(dest,ios::out);

  if(!in || !out) return;

  sstring key;
  double prob;

  if(prob_exists)
  {
	  while(in>>key>>prob)
	  {                                                                        
      for(int i=0;i<key.size();i++)
        if(!rules->is_letter(key[i])) {key[i] = '\0'; break;}
        
      GrkObj.setWord(key);
      if(GrkObj.findGreek())
      {
        out<<key<<'\t'<<prob<<'\t'
           <<GrkObj.getMostProbGreek()<<'\t'
           <<GrkObj.dic_gr_prob<<endl;
      }
	  }
  }
  else
  {
    while(in>>key)
	  {
      for(int i=0;i<key.size();i++)
        if(!rules->is_letter(key[i])) {key[i] = '\0'; break;}

      GrkObj.setWord(key);
      if(GrkObj.findGreek())
      {
        out<<key<<'\t'
           <<GrkObj.getMostProbGreek()<<'\t'
           <<GrkObj.triph_gr_prob<<endl;
      }
	  }
  }

  in.close();
  out.close();
}
//---------------------------------------------------------------------------
void __word_phonprob(const char *source, const char *dest) {
	if (!bUnlocked)
	  return;

  Greeklish2Greek GrkObj;

  fstream in, out;

  in.open(source,ios::in);
  out.open(dest,ios::out);

  if(!in || !out) return;

  sstring key, mpgr;
  double prob;


  while(in>>key)
	{
    for(int i=0;i<key.size();i++)
      if(!rules->is_letter(key[i])) {key[i] = '\0'; break;}

    GrkObj.setWord(key);
    mpgr = GrkObj.getMostProbGreek();
    if(mpgr.size())
    {
      out<<key<<'\t'
         <<mpgr<<'\t' 
         <<GrkObj.getMostProbPhon()<<'\t'
         <<GrkObj.triph_gr_prob<<'\t'
         <<GrkObj.back_gr_prob<<endl;
    }
 	}

  in.close();
  out.close();
}
//---------------------------------------------------------------------------

#endif //HELP_FUNCS

} //ILSP_GREEKLISH




