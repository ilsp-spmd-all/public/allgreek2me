#include "PhonNode.h"
#include "Globals.h"
#include "Rules.h"
#include <math.h>

namespace ILSP_GREEKLISH{

PhonNode::PhonNode(sstring *in, int p, int lim)
{
	pos = p;
	input = in;
	if(pos==0) lower_string(input);
  limit = lim;
  phonemes_from_start = sstring("");
  greek = sstring("");

  empty = false;
}

PhonNode::~PhonNode()
{
  vector<PhonNode*>::size_type psz = pointers.size();
	for(vector<PhonNode*>::size_type i=0;i<psz;i++)
	{
		delete pointers[i];
	}
}

bool PhonNode::addChild(sstring *input, int pos, sstring phoneme, sstring letters)
{
  if(pointers.size()>=limit) return false;

  //online prunning
  sstring ph_from_start = phonemes_from_start + phoneme;
  int ph_from_start_sz = ph_from_start.size();
  char trg[3] = {'_','_','_'};

  if(ph_from_start_sz>1)
  {
    if(ph_from_start_sz==2)
    {
      trg[1]=ph_from_start[0];
      trg[2]=ph_from_start[1];

      //!
      //double trg_prob = triphon_prob(trg);

      if(!triphons_probs->triphon_exists(trg)) return false;
    }
    else
    {
      if(!triphons_probs->triphon_exists(ph_from_start.substr(ph_from_start_sz-3).c_str())) return false;
      if(phoneme.size()>1)
      {
        if(ph_from_start_sz==3)
        {
          trg[1]=ph_from_start[0];
          trg[2]=ph_from_start[1];
          if(!triphons_probs->triphon_exists(trg)) return false;
        }
        else
        {
          if(!triphons_probs->triphon_exists(ph_from_start.substr(ph_from_start_sz-4,ph_from_start_sz-1).c_str())) return false;
        }
      }
    }
  }  

	PhonNode *child;
  try{
	  child = new PhonNode(input, pos);
  }
  catch(std::bad_alloc){
    return false;
  }

	child->setPhoneme(phoneme);
	child->setLetters(letters);

  child->phonemes_from_start = ph_from_start;

  child->calcGreek();

	//!child->build(); //build_pointers
	pointers.push_back(child);

  return true;
}


void PhonNode::calcGreek()
{
  greek = rules->phon_grk_2_gr(phoneme,letters);
}

void PhonNode::build_pointers()
{
  int ps = pointers.size();

  if(ps==0)
  {
    empty = true;
    return;
  }
  int child_limit = limit/ps;

  //!
  if(child_limit<=0 || limit<=1)
  {
    //child_limit=1;
    vector<PhonNode*>::size_type psz = pointers.size();
	  for(vector<PhonNode*>::size_type i=0;i<psz;i++)
	  {
		  delete pointers[i];
	  }

    pointers.clear();
    empty = true;
    return;
  }

	for(int i=0;i<ps;i++)
	{
		pointers[i]->setLimit(child_limit);
		pointers[i]->build();
	}
  return;
}

void PhonNode::build()
{
	if((*input)[pos]=='\0')
  {
    if(phonemes_from_start.size()>=2)
    {
      char trg[3] = {'_','_','_'};
      trg[0]=phonemes_from_start[phonemes_from_start.size()-2];
      trg[1]=phonemes_from_start[phonemes_from_start.size()-1];
      
      if(!triphons_probs->triphon_exists(trg)) empty = true;
    }
    return;
  }

	sstring s = "";
  int isz;
	stringvec * res;

  for(isz=0;isz<=3;isz++)
  {
  	if((*input)[pos+isz]=='\0')
    {
      build_pointers();
      return;
    }

  	s += (*input)[pos+isz];

	  res = rules->greeklish2phon(s);

    if(res==NULL) continue;

    stringvec::size_type i=0;
    stringvec::size_type ressz = res->size();
	  for(i=0;i<ressz;i++)
	  {
		  if(isz==0 && s=="p" && (*res)[i]=="r" && input->find('r')!=sstring::npos) continue;
		  if(isz==0 && s=="w" && (*res)[i]=="s" && (*input)[pos+1]!='\0') continue;

		  //!sstring dbg = res[i];
      try{
		  addChild(input,pos+isz+1,(*res)[i],s);
      }catch(...){}
	  }
  }

  build_pointers();

  /*
	s += (*input)[pos];
	res = rules->greeklish2phon(s);
  
	int i=0;
	for(i=0;i<res.size();i++)
	{
		if(s=="p" && res[i]=="r" && input->find('r')!=sstring::npos) continue;
		if(s=="w" && res[i]=="s" && (*input)[pos+1]!='\0') continue;

		//!sstring dbg = res[i];

		addChild(input,pos+1,res[i],s);
	}

	if((*input)[pos+1]=='\0')
  {
    build_pointers();
    return;
  }

	s+=(*input)[pos+1];

	res = rules->greeklish2phon(s);
	for(i=0;i<res.size();i++)
	{
    addChild(input,pos+2,res[i],s);
	}

	if((*input)[pos+2]=='\0')
  {
    build_pointers();
    return;
  }

	s+=(*input)[pos+2];

	res = rules->greeklish2phon(s);
	for(i=0;i<res.size();i++)
	{
		addChild(input,pos+3,res[i],s);
	}

	if((*input)[pos+3]=='\0')
  {
    build_pointers();
    return;
  }

	s+=(*input)[pos+3];

	res = rules->greeklish2phon(s);
	for(i=0;i<res.size();i++)
	{
		addChild(input,pos+4,res[i],s);
	}

  build_pointers(); */
}

/*
void PhonNode::transcribe(vector<sstring> &res, sstring s)
{
	if(pointers.size()==0)
	{
		res.push_back(s);
	}
	for(int i=0;i<pointers.size();i++)
	{
		pointers[i]->transcribe(res,s+pointers[i]->phoneme);
	}
}
*/

void PhonNode::getWordProbs(list<word_prob*> &res, sstring ph_till_now,
                            sstring gr_till_now, double prob, int rule_count)
{
	double p;

	if(pos==0)
  {
		build();
    //purge();
  }

	if(pointers.size()==0)
	{
    if(!empty)
    {
		  //p = -rules->wordprob(ph_till_now);
      float p_phon = rules->wordprob_triphons(ph_till_now);
      float p_rules = log10(prob)/rule_count;
      //float p_rules = prob/rule_count;
		  p = -(p_phon + p_rules);

		  if(p<-triphons_probs->ZERO_PROB)
      {
        word_prob *tmp_word_prob;
        try{
          tmp_word_prob = new word_prob(ph_till_now,p,gr_till_now, p_phon, p_rules);
        }
        catch(std::bad_alloc){
          tmp_word_prob = NULL;
        }
			  if(tmp_word_prob) res.push_back(tmp_word_prob);
      }
    }
	} 

  vector<PhonNode*>::size_type ressz = pointers.size();
	for(vector<PhonNode*>::size_type i=0;i<ressz;i++)
	{
		p = prob*rules->rule_probability(pointers[i]->letters+pointers[i]->phoneme);

    sstring tmp_greek_till_now = gr_till_now;
    
    if((pointers[i]->greek[0]=='�' && (*input)[pos]!='f') ||
       (pointers[i]->greek[0]=='�' && (*input)[pos]!='v' && (*input)[pos]!='b'))
    {
      int gsz = tmp_greek_till_now.size();

      if(gsz>0 && (tmp_greek_till_now[gsz-1]=='�' || tmp_greek_till_now[gsz-1]=='�'))
        pointers[i]->greek = '�';    
    }

    if(pointers[i]->greek[0]=='�')
    {
      int gsz = tmp_greek_till_now.size();

      if(gsz>0 && tmp_greek_till_now[gsz-1]=='�') tmp_greek_till_now[gsz-1] = '�';
      else if(gsz>0 && tmp_greek_till_now[gsz-1]=='�') tmp_greek_till_now[gsz-1] = '�';
      else tmp_greek_till_now += pointers[i]->greek;
    }
    else
    {
      tmp_greek_till_now += pointers[i]->greek;
    }

    sstring tmp_ph_till_now = ph_till_now;
    tmp_ph_till_now += pointers[i]->phoneme;
		pointers[i]->getWordProbs(res,tmp_ph_till_now,tmp_greek_till_now,p,rule_count+1);
	}
}

/*
void PhonNode::purge()
{
	if(pointers.size()==0)
	{
    return;
  }
	for(int i=0;i<pointers.size();i++)
	{
		pointers[i]->purge();
	}

  vector<PhonNode*>::iterator it = pointers.begin();
	while(it != pointers.end())
	{
		if((*it)->empty)
      it = pointers.erase(it);
	}
  if(pointers.size()==0) empty = true;
}
*/



int PhonNode::getWordProbsRec(list<word_prob> *res,
                    sstring *input,
                    int pos,
                    sstring ph_till_now,
                    sstring gr_till_now,
                    int rule_count,
                    double rules_prob,
                    double triphons_prob,
                    int limit)
{
  if(limit<=0)
    return 0;

  sstring &word = *input;
   double trg_prob;

  //end recursion
  if(word[pos]=='\0')
  {
    if(ph_till_now.size()>=2)
    {
      char trg[3] = {'_','_','_'};
      trg[0]=ph_till_now[ph_till_now.size()-2];
      trg[1]=ph_till_now[ph_till_now.size()-1];

      if(!triphons_probs->triphon_exists(trg,trg_prob)) return 0;
      triphons_prob += trg_prob;
    }

    double prob;
    prob = -(log10(rules_prob)/rule_count + triphons_prob/word.size());

    if(prob<-triphons_probs->ZERO_PROB)
    {
		  res->push_back(word_prob(ph_till_now,prob,gr_till_now));
      return 1;
    }
    return 0;
  }

  sstring s = "", phoneme;
  int isz;
	stringvec * phrules;

  for(isz=0;isz<=3;isz++)
  {
  	if(word[pos+isz]=='\0') break;

  	s += word[pos+isz];

	  phrules = rules->greeklish2phon(s);

    if(phrules==NULL) continue;

    stringvec::size_type i=0;
    stringvec::size_type phrsz = phrules->size();
	  for(i=0;i<phrsz;i++)
	  {
      phoneme = (*phrules)[i];
		  if(isz==0 && s=="p" && phoneme=="r" && word.find('r')!=sstring::npos) continue;
		  if(isz==0 && s=="w" && phoneme=="s" && word[pos+1]!='\0') continue;

//---------------------------------------------
		  //addChild(input,pos+isz+1,(*res)[i],s);

      //online prunning
      sstring ph_now = ph_till_now + phoneme;
      int ph_now_sz = ph_now.size();
      double cur_triphons_prob = 0.0;
      char trg[3] = {'_','_','_'};

      if(ph_now_sz>1)
      {
        if(ph_now_sz==2)
        {
          trg[1]=ph_now[0];
          trg[2]=ph_now[1];

          if(!triphons_probs->triphon_exists(trg,trg_prob)) continue;
          cur_triphons_prob += trg_prob;
        }
        else
        {
          if(!triphons_probs->triphon_exists(ph_now.substr(ph_now_sz-3).c_str(),trg_prob)) continue;
          cur_triphons_prob += trg_prob;
          if(phoneme.size()>1)
          {
            if(ph_now_sz==3)
            {
              trg[1]=ph_now[0];
              trg[2]=ph_now[1];
              if(!triphons_probs->triphon_exists(trg,trg_prob)) continue;
              cur_triphons_prob += trg_prob;
            }
            else
            {
              if(!triphons_probs->triphon_exists(ph_now.substr(ph_now_sz-4,ph_now_sz-1).c_str(),trg_prob)) continue;
              cur_triphons_prob += trg_prob;
            }
          }
        }
      }
//-------------------------------------------------------
      getWordProbsRec(res,
                    input,
                    pos+isz+1,
                    ph_now,
                    gr_till_now + rules->phon_grk_2_gr(phoneme,s),
                    rule_count+1,
                    rules_prob*rules->rule_probability(s+phoneme),
                    triphons_prob + cur_triphons_prob,
                    limit-1);
	  }
  }

  return 1;
}


} //ILSP_GREEKLISH

