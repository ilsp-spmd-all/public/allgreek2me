#ifndef _TRIPHONSPROBS_
#define _TRIPHONSPROBS_

#include <fstream>
using namespace std;

namespace ILSP_GREEKLISH{

/*
//----------------------------------------------------------------
//#define GREEK_ALPHA

char GRAM_SEQ_FIRST = 'a', GRAM_SEQ_LAST	= 'z', GRAM_EXTRAS_COUNT = 12;

float ZERO_PROB = -18.5;

extern char *gram_extras;
extern char *gram_extras_pos;

char GRAM_COUNT = (GRAM_SEQ_LAST-GRAM_SEQ_FIRST+GRAM_EXTRAS_COUNT+1);

inline char char2index(char c);
//----------------------------------------------------------------
//extern float triphons_probs[GRAM_COUNT][GRAM_COUNT][GRAM_COUNT];
extern float *triphons_probs;
//----------------------------------------------------------------
*/        

class TriphonsProbs
{
  float *triphons_probs;
  char *gram_extras;
  char *gram_extras_pos;
  char GRAM_SEQ_FIRST, GRAM_SEQ_LAST, GRAM_EXTRAS_COUNT, GRAM_COUNT;

  char *data_src;
  bool del_src;

  int read_triphons_header(char *in);

public:
  float ZERO_PROB;

  TriphonsProbs(char *src, bool del_src = false);
  ~TriphonsProbs();

  char char2index(char c);
  
  double triphon_prob(const char triphon[3]);
  double triphon_idx_prob(char i, char j, char k);
  inline bool triphon_exists(const char triphon[3]) {return triphon_prob(triphon)>ZERO_PROB;} 
  inline bool triphon_exists(const char triphon[3], double &res) {res = triphon_prob(triphon); return res>ZERO_PROB;}
};

} //ILSP_GREEKLISH

#endif //_TRIPHONSPROBS_


