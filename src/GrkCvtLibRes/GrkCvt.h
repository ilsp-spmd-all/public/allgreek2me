#ifndef _GRKCVT_H_
#define _GRKCVT_H_

#ifdef _WIN32
  #ifdef GRKCVTDLL
    #ifdef BCB
    #define GrkCvtDeclSpec(_Type) _Type __declspec(dllexport) __stdcall
    #define extern_C extern "C"
    #else
    #define GrkCvtDeclSpec(_Type) __declspec(dllexport) _Type
    #define extern_C extern "C"
    #endif 
  #else //GRKCVTDLL
    #define GrkCvtDeclSpec __stdcall
    #define extern_C extern "C"
  #endif
#else //_WIN32
  //#define GrkCvtDeclSpec(X) __attribute__ ((visibility ("default"))) X
  #define GrkCvtDeclSpec(X) X
  #define extern_C extern "C"
#endif //_WIN32

//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void) greeklish_destroy();
//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(bool)  greeklish_init(const char *key);

//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(const char *) greeklish_get_text(void* handle);
//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void) greeklish_release_handle(void* cpsz);

//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void*) greeklish_convert(const char * source);
//---------------------------------------------------------------------------
#ifdef ALLGREEK_LM
extern_C GrkCvtDeclSpec(void*)  greeklish_convert_lm(const char * source);
#endif
//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void) greeklish_convert_file(const char *source, const char *dest);

//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void*) greeklish_elot_gr2grl(const char *source);
//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void*) greeklish_custom_gr2grl(const char *source);
//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void) greeklish_convert_gr2grl_file(const char *source, const char *dest);

//---------------------------------------------------------------------------
extern_C GrkCvtDeclSpec(void*) greeklish_analyse(const char * source);
//---------------------------------------------------------------------------
#endif //_GRKCVT_H_

