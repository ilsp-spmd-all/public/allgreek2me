// Greek2Greeklish.h: interface for the Greek2Greeklish class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _GREEK2GREEKLISH_H_
#define _GREEK2GREEKLISH_H_

#include <strstream>
#include "sstring/sstring.h"
#include <string>
#include <set> 
using namespace std;

namespace ILSP_GREEKLISH{

class gr2grl_rule
{
public:
  sstring greek, greeklish;
  sstring left_context, right_context;
  bool valid;

  gr2grl_rule(sstring gr = sstring(""), sstring grl = sstring(""),
              sstring lc = sstring(""), sstring rc = sstring(""))
              :greek(gr),greeklish(grl),
              left_context(lc), right_context(rc), valid(true){}

  sstring getGreeklish() const { return greeklish;  }
  sstring getTotGreek() const {
    sstring ret=sstring("");
    if(left_context.size()>0){
      ret+='('; ret+=left_context; ret+=')';
    }
    ret+=greek;
    if(right_context.size()>0){
      ret+='('; ret+=right_context; ret+=')';
    }
    
    return ret;
  }
  sstring getTotRule() const {
    return getTotGreek()+'\t'+getGreeklish();
  }

  bool match(sstring &text, int pos) const;

  bool operator==(const gr2grl_rule& other);
  
  friend bool operator< (const gr2grl_rule& x, const gr2grl_rule& y); 
  friend ostream& operator<< ( ostream& is, gr2grl_rule& rule );
  friend istream& operator>> ( istream& is, gr2grl_rule& rule );
};  

ostream& operator>> ( ostream& is, gr2grl_rule& rule );
//ostream& operator>> ( ostream& is, gr2grl_rule rule ); 
istream& operator>> ( istream& is, gr2grl_rule& rule );

typedef set<gr2grl_rule> gr2grl_ruleset;

class Greek2Greeklish
{
  gr2grl_ruleset customrules, defaultrules;
  int customrules_maxlen, defaultrules_maxlen;
  sstring custom_rules_file;

  void init_rules(istream &rules_stream, gr2grl_ruleset &dest_set, int &customrules_maxlen);

  //sstring gr2grl_key(sstring key, bool first, bool next_voiced, gr2grl_ruleset &gr2grlrules);
                               
  inline sstring greek2greeklish(sstring &greek, gr2grl_ruleset &gr2grlrules, int &gr2grlrules_maxlen);
public:
  Greek2Greeklish(const char *customrules_file);

  sstring default_greek2greeklish(sstring greek);
  sstring custom_greek2greeklish(sstring greek);

  bool is_valid_rule(const char *rule);

  void load_custom_gr2grl(istream &rules_stream);
  void save_custom_gr2grl(ostream &rules_stream);

  void load_custom_gr2grl();
  void save_custom_gr2grl();

  void import_custom_gr2grl(const char *rules_str);
  sstring export_rules(bool custom = true);

  sstring elot742_greek2greeklish(sstring greek);
};

} //ILSP_GREEKLISH

#endif //_GREEK2GREEKLISH_H_

