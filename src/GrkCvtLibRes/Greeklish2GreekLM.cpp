//////////////////////////////////////////////////////////////////////

#include "Greeklish2GreekLM.h"

#ifdef ALLGREEK_LM

#include <math.h>
#include <algorithm>
using namespace std;

namespace ILSP_GREEKLISH{

//!todo
FileMapper *fileMapperLMIndex = NULL, *fileMapperLM = NULL;

Greeklish2GreekLM::Greeklish2GreekLM()
{
	try{
    greeklish_dicindex = new MapFileReader(fileMapperGr->getData());
	  english_dicindex = new MapFileReader(fileMapperEn->getData());

    if(fileMapperLMIndex==NULL){
      fileMapperLMIndex = new FileMapper((GetModulePath()+"lexicon_index.bin").c_str());
    }  
    if(fileMapperLM==NULL){
      fileMapperLM = new FileMapper((GetModulePath()+"lm.bin").c_str());
    }
	  lm_dicindex = new MapFileReader(fileMapperLMIndex->getData());
  }
  catch(std::bad_alloc){
    return;
  }

  //todo
  ifstream ifs_lm_ini("lm.ini");
  if(ifs_lm_ini.is_open()){
    std::string name;
    double value;
    while(ifs_lm_ini>>name>>value){
      if(name=="default_dic_prob") default_dic_prob = value;  
      else if(name=="default_phon_prob") default_phon_prob = value;
      else if(name=="default_rul_prob") default_rul_prob = value;
      else if(name=="default_bck_prob") default_bck_prob = value;
      else if(name=="default_eng_prob") default_eng_prob = value;
      else if(name=="default_join_prob") default_join_prob = value;
      else if(name=="w_dic_prob") w_dic_prob = value;
      else if(name=="w_phon_prob") w_phon_prob = value;
      else if(name=="w_rul_prob") w_rul_prob = value;
      else if(name=="w_bck_prob") w_bck_prob = value;
      else if(name=="w_eng_prob") w_eng_prob = value;
      else if(name=="w_join_prob") w_join_prob = value;
    }
  }
}

Greeklish2GreekLM::~Greeklish2GreekLM()
{
	if(greeklish_dicindex) delete greeklish_dicindex;
	if(english_dicindex) delete english_dicindex;
}

bool Greeklish2GreekLM::next_word2convert(sstring &text, sstring &text_r, int pos_beg, int &pos_word_start, int &pos_word_end)
{        
  //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!! 
  //eat all beginning whites
  while(text[pos_beg]!='\0' && rules->is_white(text[pos_beg])) pos_beg++;
  if(text[pos_beg]=='\0') return false;

  //{
  if(text[pos_beg]=='{' || text[pos_beg]=='}'){
    pos_word_start = pos_beg;
    pos_word_end = pos_beg+1;
    return true;
  }

  //find next white
  int pos_white = pos_beg+1;
  while(text[pos_white]!='\0' && !rules->is_white(text[pos_white])) pos_white++;
  if(customDicExceptions->find(text.substr(pos_beg,pos_white - pos_beg))!=NULL){
    pos_word_start = pos_beg;
    pos_word_end = pos_white;
    return true;
  }

  #ifdef _ENABLE_CHEATS_
  sstring tmpstr = text.substr(pos_beg,pos_white - pos_beg);
  if(matches_cheat(tmpstr)){
    pos_word_start = pos_beg;
    pos_word_end = pos_white;
    return true;
  }
  #endif //_ENABLE_CHEATS_

  //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!!

  //eat all beginning not alpha letters
  while(text[pos_beg]!='\0' && !rules->is_letter(text[pos_beg]))
  {
    //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!!
    if(rules->is_white(text[pos_beg])){
      while(text[pos_beg]!='\0' && rules->is_white(text[pos_beg])) pos_beg++;
      if(text[pos_beg]=='\0') return false;
      pos_white = pos_beg+1;
      while(text[pos_white]!='\0' && !rules->is_white(text[pos_white])) pos_white++;
      if(customDicExceptions->find(text.substr(pos_beg,pos_white - pos_beg))!=NULL){
        pos_word_start = pos_beg;
        pos_word_end = pos_white;
        return true;
      }
      continue;
    }
    //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!!
    pos_beg++;
  }
  if(text[pos_beg]=='\0') return false;

  //if last eaten is digit eat also all digits
  if(pos_beg>0 && rules->is_digit(text[pos_beg-1]))
  {
    while(text[pos_beg]!='\0' && !rules->is_alpha_letter(text[pos_beg])) pos_beg++;
    if(text[pos_beg]=='\0') return false;
  } 

  //find next white
  pos_white = pos_beg+1;
  while(text[pos_white]!='\0' && !rules->is_white(text[pos_white])) pos_white++;

  //eat all trailing not alpha letters
  int pos_end = pos_white-1;
  while(pos_end>pos_beg && !rules->is_letter(text[pos_end])) pos_end--;

  //if last eaten is digit eat also all digits
  if(rules->is_digit(text[pos_end+1]))
  {
    while(pos_end>pos_beg && !rules->is_alpha_letter(text[pos_end])) pos_end--;
  }

  int pos, pos_last = pos_end;

  //"somenone's" case keep original
  //if(text[pos_end]=='s' && (text[pos_end-1]=='\'' && text[pos_end-1]=='\�'))
  if(text[pos_end]=='s' && (text[pos_end-1]=='\'' && (unsigned char)text[pos_end-1]==146))
    return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);

  sstring text_frag = text.substr(pos_beg,pos_last-pos_beg+1);
  //url kai parakatw
  if(text_frag.find("://")!=sstring::npos ||
     text_frag.find("www.")!=sstring::npos)
    return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);
  //email address kai parakatw   
  if((pos=text_frag.find('@'))!=(int)sstring::npos &&
     text_frag.find('.',pos)!=sstring::npos)
    return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);

  //check for kati-kati
  for(pos=pos_end-1;pos>pos_beg;pos--)
  {
    if(rules->is_letter(text[pos])) continue;
    pos_last = pos-1;  

    if(!rules->is_payla(text[pos]) && !rules->is_stress(text[pos]))
    {
      //!
      /*
      if(!(text[pos]=='.' && (pos_end-pos)>3)
        && rules->is_letter(text[pos+1]) && rules->is_letter(text[pos-1]))
      {
        return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);
      }
      */
      bool b1 = text[pos]=='.',
           b2 = rules->is_extension_domain(text.substr(pos+1,pos_end - pos)),
           b3 = rules->is_letter(text[pos+1]),
           b4 = rules->is_letter(text[pos-1]),
           b5 = text[pos+1]==text_r[pos+1]; //kefalaio meta apo teleia
      if(b1 && b2 && b3 && b4 && b5)
      {
        return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);
      }
    }
  }

  pos_word_start = pos_beg;
  pos_word_end = pos_last + 1;
  return true;
}

void Greeklish2GreekLM::capitalize_word(sstring &word, sstring &word_r)
{ 
  //capitalise
  bool allup = true;
  int ws = word_r.size();
  if(!ws || !word.size()) return;
  for(int i=0;i<ws;i++)
    //!numbers!if(word_r[i]<'A' || word_r[i]>'Z') {allup = false;break;}
    if(word_r[i]>='a' && word_r[i]<='z') {allup = false;break;}

  if(allup)
  {
    for(sstring::size_type i=0;i<word.size();i++)
    word[i] = rules->upper_greek(word[i]);
  }
  else
  if(word_r[0]>='A' && word_r[0]<='Z')
  {
    word[0] = rules->upper_greek(word[0]);
  }
}

void Greeklish2GreekLM::tokenize(sstring &text_r, vector_token &tokens)
{
  try
  {
    sstring text = text_r;

#ifdef TEXT_LIMIT
    //text limit TEXT_LIMIT = 255 characters
    char buff255[TEXT_LIMIT+1];
    int cut_pos = TEXT_LIMIT;
    while(cut_pos>0 && rules->is_letter(text_r[cut_pos]))
      cut_pos--;
    strcpy(buff255, text_r.substr(0,cut_pos).c_str());
    text = buff255;
#endif //TEXT_LIMIT

	  lower_string(&text);

	  int pos_start = 0, pos_end = 0, pos_last = 0;
	  while(next_word2convert(text,text_r,pos_end,pos_start,pos_end))
	  {
      if(pos_start>pos_last)
      {
        tokens.push_back(token());
        token &tok = tokens.back();
        tok.text_r = text_r.substr(pos_last,pos_start - pos_last);
        tok.text = text.substr(pos_last,pos_start - pos_last);
        tok.isdelim = true;
      }
      pos_last = pos_end;

      tokens.push_back(token());
      token &tok = tokens.back();
      tok.text_r = text_r.substr(pos_start,pos_end - pos_start);
      tok.text = text.substr(pos_start,pos_end - pos_start);
    }
    sstring trail = text_r.substr(pos_last);
    if(trail.size()){
      tokens.push_back(token());
      token &tok = tokens.back();
      tok.text_r = trail;
      tok.text = text.substr(pos_last);   
      tok.isdelim = true;
    }
  }
  catch(...)
  {
    //!!
  }
}

void Greeklish2GreekLM::analyze_token(const token &_tok, vector_token &conversions)
{
  if(_tok.isdelim){
    conversions.push_back(_tok);
    return;
  }

	sstring wordkey = "";
	sstring word = _tok.text;

  if(english_dicindex->findKey(word.c_str())){
    conversions.push_back(token());
    token &tok = conversions.back();
    tok.text_r = _tok.text_r;
    tok.text = _tok.text_r;
    tok.eng_prob = english_dicindex->getFirstProb();
  }

  list<word_prob*> prob_words;
  list<word_prob*>::iterator prob_words_it, prob_words_best_phon_it;
  list<dic_word_prob*> prob_words_dic;
  list<dic_word_prob*>::iterator prob_words_dic_it;

  PhonNode *nod;
  try{
	  nod = new PhonNode(&word);
  }
  catch(std::bad_alloc){
    //!
  }
  nod->getWordProbs(prob_words);
  delete nod;

  prob_words_best_phon_it = prob_words.begin();
  for(prob_words_it=prob_words.begin();prob_words_it!=prob_words.end();prob_words_it++)
  {
	  wordkey = (*prob_words_it)->word;
    float p_phon = (*prob_words_it)->p1;
    float p_rules = (*prob_words_it)->p2;

    if((*prob_words_best_phon_it)->p1 > p_phon)
      prob_words_best_phon_it = prob_words_it;

    // search custom dictionary
    const sstring *customfind = customDic->find(wordkey, word);
    if(customfind!=NULL)
    {
      conversions.push_back(token());
      token &tok = conversions.back();
      tok.text_r = _tok.text_r;
      tok.phon = wordkey;
      tok.text = *customfind;
      tok.dic_prob = 0.; //!
    }

    if(greeklish_dicindex->findKey(wordkey.c_str()))
    {
      prob_words_dic.clear();
      greeklish_dicindex->getWordProbs(prob_words_dic);
	    prob_words_dic_it = prob_words_dic.begin();
	    for(;prob_words_dic_it!=prob_words_dic.end();prob_words_dic_it++)
	    { 
        conversions.push_back(token());
        token &tok = conversions.back();
        tok.text_r = _tok.text_r;           
        tok.phon = wordkey;
        tok.text = (*prob_words_dic_it)->word;
        tok.dic_prob = (*prob_words_dic_it)->prob;
        tok.phon_prob = p_phon;
        tok.rul_prob = p_rules;

        int num_of_back_rules;
			  double p_backward = rules->match_greek_greeklish(tok.text, word, num_of_back_rules);
        if(p_backward > 0)
				  p_backward = log10(p_backward)/num_of_back_rules;
			  else
				  p_backward = -20;
        tok.bck_prob = p_backward;
        capitalize_word(tok.text, tok.text_r);
      }
    }
  }

  if(conversions.size()<=0)
  {
    conversions.push_back(token());
    token &tok = conversions.back();
    if(prob_words_best_phon_it != prob_words.end()){
      tok.text_r = _tok.text_r;
      tok.phon = (*prob_words_best_phon_it)->word;
      tok.text = sstring("\"");
      tok.text += tok.phon;
      tok.text += "\"";
    }else{   
      tok.text_r = _tok.text_r;
      tok.text = tok.text_r;
    }
  }
}

//todo
void xml_out(fstream &out, sstring &s){
  for(int i=0;i<s.size();i++){
    switch(s[i]){
    case '&':
      out<<"&amp;";
      break;
    case '>':
      out<<"&gt;";
      break;
    case '"':
      out<<"&quot;";
      break;
    case '<':
      out<<"&lt;";
      break;
    case '\'':
      out<<"&apos;";
      break;
    default:
      out<<s[i];
    }
  }
}

void Greeklish2GreekLM::analyze_text(sstring &text, vector_token &final_tokens)
{
  if(!text.size()) return;
  
  vector_token tokens;
  tokenize(text, tokens);

  vector<vector_token> graph;
  graph.reserve(tokens.size());

  int prev_layer_non_delim_i = -1;
  int discarded = 0;

  for(sstring::size_type token_i=0;token_i<tokens.size();token_i++){
    graph.push_back(vector_token());
    vector_token &layer_tokens = graph.back();
    analyze_token(tokens[token_i], layer_tokens);
    if(!layer_tokens.size()){
      graph.pop_back();
      discarded++;
      continue;
    }
    final_tokens.push_back(token());
    if(!layer_tokens.begin()->isdelim){
      for(sstring::size_type layer_token_i=0;layer_token_i<layer_tokens.size();layer_token_i++){
        token &tok = layer_tokens[layer_token_i];
        double prob =
          w_dic_prob * tok.dic_prob +
          w_phon_prob * tok.phon_prob +
          w_rul_prob * tok.rul_prob +
          w_bck_prob * tok.bck_prob +
          w_eng_prob * tok.eng_prob;
        if(prev_layer_non_delim_i>=0){
          vector_token &prev_layer_tokens = graph[prev_layer_non_delim_i];
          for(sstring::size_type prev_layer_token_i=0;
              prev_layer_token_i<prev_layer_tokens.size();prev_layer_token_i++){
            const token &prev_tok = prev_layer_tokens[prev_layer_token_i];
            double join_prob = calc_join_cost(prev_tok.text, tok.text);
            double join_cost = w_join_prob * join_prob;
            if(prev_layer_token_i==0
              || (tok.prob < (prob+join_cost+prev_tok.prob))){
              tok.prob = prob+join_cost+prev_tok.prob;
              //tok.join_prob = join_prob;
              tok.best_join_prob = join_prob;
              tok.index = prev_layer_token_i;
              tok.layer_index = prev_layer_non_delim_i;
            } 
            tok.join_probs.push_back(join_prob);
          }
        }else{
          tok.prob = prob;
        }
      }
      prev_layer_non_delim_i = token_i - discarded;
    }else{ //isdelim
      token &tok = layer_tokens[0];
      //todo
      if(tok.text_r.find_first_of(".!?;")!=sstring::npos){
        prev_layer_non_delim_i = -1;
      }
    }
  }

  int best_i = -1;
  if(prev_layer_non_delim_i>=0){
    vector_token &last_layer = graph[prev_layer_non_delim_i];
    best_i=0;
    for(sstring::size_type layer_token_i=1;layer_token_i<last_layer.size();layer_token_i++){
      if(last_layer[layer_token_i].prob > last_layer[best_i].prob)
        best_i=layer_token_i;
    }
  }

  for(int layer_i=graph.size()-1;layer_i>=0;layer_i--){
    vector_token &layer = graph[layer_i];
    if(!layer.begin()->isdelim){
      if(best_i == -1){
        best_i=0;
        for(sstring::size_type layer_token_i=1;layer_token_i<layer.size();layer_token_i++){
          if(layer[layer_token_i].prob > layer[best_i].prob)
            best_i=layer_token_i;
        }
      }
      final_tokens[layer_i] = layer[best_i];
      best_i = final_tokens[layer_i].index;
    }else{
      final_tokens[layer_i] = graph[layer_i][0];
    }
    /*if(layer_i==prev_layer_non_delim_i){
      final_tokens[layer_i] = graph[layer_i][best_i];
      best_i = final_tokens[layer_i].index;
      prev_layer_non_delim_i = final_tokens[layer_i].layer_index;
    }else{
      final_tokens[layer_i] = graph[layer_i][0];
    } */
  }

  //debug
  fstream fsg("graphLM.xml", ios::out);
  fsg<<"<?xml version=\"1.0\" encoding=\"WINDOWS-1253\"?>"<<endl;
  fsg<<"<?xml-stylesheet type=\"text/xsl\" href=\"graphLM.xsl\"?>"<<endl;
  fsg<<"<graph>"<<endl;
  sstring tmp;
  for(int l=0;l<graph.size();l++){
    fsg<<"<layer ";
    if(graph[l][0].text_r.size())
    {
      fsg<<"original_text = \"";
      xml_out(fsg, graph[l][0].text_r);
      fsg<<"\" ";
    }
    if(final_tokens[l].text.size())
    {
      fsg<<"final_text = \"";
      xml_out(fsg, final_tokens[l].text);
      fsg<<"\" ";
    }
    //fsg<<"layer_index = \""<<l<<"\"";
    fsg<<" >"<<endl;
    /*for(int n=0;n<graph[l].size();n++){
      token &node = graph[l][n];
      if(node.isdelim) continue;
      fsg<<"<node";
      fsg<<" text = \""<<node.text<<"\"";
      fsg<<" phon = \""<<node.phon<<"\"";
      fsg<<" dic_prob = \""<< node.dic_prob<<"\"";
      fsg<<" phon_prob = \""<< node.phon_prob<<"\"";
      fsg<<" rul_prob = \""<< node.rul_prob<<"\"";
      fsg<<" bck_prob = \""<< node.bck_prob<<"\"";
      fsg<<" eng_prob = \""<< node.eng_prob<<"\"";
      fsg<<" join_prob = \""<< node.join_prob<<"\"";
      fsg<<" prob = \""<< node.prob<<"\"";
      fsg<<" prev_best = \""<< node.index<<"\"";
      fsg<<" layer_index = \""<< node.layer_index<<"\"";
      fsg<<" />"<<endl;
    }*/
    for(int n=0;n<graph[l].size();n++){
      token &node = graph[l][n];
      if(node.isdelim) continue;
      fsg<<"<node";

      fsg<<" text = \"";
      xml_out(fsg, node.text);
      fsg<<"\"";

      fsg<<" phon = \"";
      xml_out(fsg, node.phon);
      fsg<<"\"";

      fsg<<" acc_prob = \""<< node.prob<<"\"";
      if(node.layer_index!=-1 && node.index!=-1){
        fsg<<" prev_best = \"";
        xml_out(fsg, graph[node.layer_index][node.index].text);
        fsg<<"\"";
      }
      //fsg<<" prev_best_layer_index = \""<< node.layer_index<<"\"";
      fsg<<" >"<<endl;

      fsg<<"<dic_prob>"<<node.dic_prob<<"</dic_prob>"<<endl;
      fsg<<"<phon_prob>"<<node.phon_prob<<"</phon_prob>"<<endl;
      fsg<<"<rul_prob>"<<node.rul_prob<<"</rul_prob>"<<endl;
      fsg<<"<back_prob>"<<node.bck_prob<<"</back_prob>"<<endl;
      fsg<<"<eng_prob>"<<node.eng_prob<<"</eng_prob>"<<endl;
      fsg<<"<bjn_prob>"<<node.best_join_prob<<"</bjn_prob>"<<endl;
      fsg<<"<join_probs>"<<endl;
      for(int ji=0;ji<node.join_probs.size();ji++)
        fsg<<node.join_probs[ji]<<endl;
      fsg<<"</join_probs>"<<endl;
      fsg<<"</node>"<<endl;
    }
    fsg<<"</layer>";
  }     
  fsg<<"</graph>"<<endl;
}

void Greeklish2GreekLM::convert(sstring &text, sstring &converted_text)
{ 
  vector_token tokens;
  analyze_text(text, tokens);
  converted_text.clear();
  converted_text.reserve((sstring::size_type)(text.size()*1.5));
  for(vector_token_cit tok_cit=tokens.begin();tok_cit!=tokens.end();tok_cit++)
    converted_text+=tok_cit->text;
}

//todo
struct ulong_float{
  unsigned long index;
  float prob;
};
inline bool ulong_float_less(const ulong_float &uf1, const ulong_float &uf2){
  return uf1.index<uf2.index;
}

double Greeklish2GreekLM::calc_join_cost(sstring &word1, sstring &word2)
{
  double ret = default_join_prob;

  if(!lm_dicindex->findKey(word1.c_str()))
    return ret;
  int data_length = lm_dicindex->getDataLegnth();
  unsigned long data1[2];
  data1[0] = -1;
  data1[1] = -1;
  if(data_length==2*sizeof(unsigned long))
    lm_dicindex->getData((char*)data1);
  if(data1[1]==-1)
    return ret;

  if(!lm_dicindex->findKey(word2.c_str()))
    return ret;
  data_length = lm_dicindex->getDataLegnth();
  unsigned long data2[2];
  data2[0] = -1;
  data2[1] = -1;
  if(data_length==2*sizeof(unsigned long))
    lm_dicindex->getData((char*)data2);

  const unsigned char *lm = fileMapperLM->getData();
  lm += data1[1];
  if((*(unsigned long*)lm)!=data1[0])
    return ret; //todo error
  lm+=4;
  long num_of_links = *(long*)lm;
  lm+=4;

  ulong_float *start = (ulong_float *)lm;
  ulong_float *end = start + num_of_links;
  ulong_float val;
  val.index = data2[0];

  ulong_float *res = lower_bound(start, end, val, ulong_float_less);

  if(res->index==val.index){
    ret = res->prob;
  }

  return ret;
}

} //ILSP_GREEKLISH

#endif //ALLGREEK_LM


