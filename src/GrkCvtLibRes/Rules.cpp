#include "Rules.h"
#include <float.h>
#include <math.h>

namespace ILSP_GREEKLISH{

//#define CONSOLE_MODE

Rules::Rules(istream& greeklish2phon_rules,
             istream& greek_from_greeklish_rules,
             istream& extensions_domains_file)
{
  greek_letters = sstring("������������������������������������");
  greek_vowels = sstring("������������������");
  nonalpha_letters = sstring("");
  letters = sstring("");
  stress_chars = sstring("';");
	//symfwna = sstring("bcDdfgGhjJklLmMnNpsTtvxXz");
  white_chars = sstring(" \t\r\n");
  payla_chars = sstring("_-+=/\\,!?()");

	read_greeklish2phon_rules(greeklish2phon_rules);
	//read_diphone_probabilities(diphone_probabilities);
	read_greek_from_greeklish_rules(greek_from_greeklish_rules);
	read_extensions_domains(extensions_domains_file);

  letters_payla = sstring((letters + payla_chars).c_str());
}

Rules::~Rules()
{
  return;
}


stringvec * Rules::greeklish2phon(sstring key)
{
  map_string_stringvec::iterator greeklish2phon_rules_it;
	greeklish2phon_rules_it = greeklish2phon_rules.find(key);
	if(greeklish2phon_rules_it == greeklish2phon_rules.end())
	{
		return NULL;
	}
	return &(greeklish2phon_rules_it->second);
}


bool Rules::is_extension_domain(sstring key)
{ 
  lower_string(&key);

  return extensions_domains.find(key)!=extensions_domains.end();
}

/*double Rules::diphone_probability(sstring key)
{
	return (diphones_probabilities.find(key))->second;
}*/

double Rules::rule_probability(sstring key)
{
	return (rules_probabilities.find(key))->second;
}

/*double Rules::wordprob(sstring word)
{
	int wsize = word.size();
	if(wsize<2) return 0.0;

	double ret = 0.0;
	sstring key = "__";
	map_string_double::iterator it;

	for(int i=1;i<wsize;i++)
	{
		key = word.substr(i-1,2);
		it = diphones_probabilities.find(key);
		if(it != diphones_probabilities.end())
			ret += it->second;
    else
      ret -= 10.0;
	}
	return ret/word.size();
}*/

double Rules::wordprob_triphons(sstring word)
{
	int wsize = word.size();
	if(wsize<1) return 0.0;

	double ret = 0.0;
	char *key_idx;
  try{
    key_idx = new char[word.size()+3];
  }
  catch(std::bad_alloc){
    return 0.0;
  }

  int i=0;
  key_idx[i++] = triphons_probs->char2index('_');
  for(i=0;i<wsize;i++)
    if((key_idx[i+1] = triphons_probs->char2index(word[i]))==-1) key_idx[i+1] = key_idx[0];
  key_idx[i+1] = triphons_probs->char2index('_');

  float trp;

  for(i=0;i<wsize;i++)
  {
    trp =  triphons_probs->triphon_idx_prob(key_idx[i],key_idx[i+1],key_idx[i+2]);
    ret += trp;

    /*//!
    if(trp==0)
    {
      const char dbg[3] = {word[i-1],word[i],word[i+1]};
      int ii=0;
    }
    */
#ifdef _WIN32
    if(_isnan(ret))
    {
      _fpreset();
      return triphons_probs->ZERO_PROB;
    }
#else //_WIN32
    if(isnan(ret))
    {
      //!!!
      printf("isnan\n");
      return triphons_probs->ZERO_PROB;
    }
#endif //_WIN32
  }
  delete[] key_idx;
	return ret/wsize;
}

bool Rules::wordprob_triphons_exist(sstring word)
{
	int wsize = word.size();
	if(wsize<1) return false;

	char *key_idx;
  try{
    key_idx = new char[word.size()+3];
  }
  catch(std::bad_alloc){
    return false;
  }
  int i=0;
  key_idx[i++] = triphons_probs->char2index('_');
  for(i=0;i<wsize;i++)
    if((key_idx[i+1] = triphons_probs->char2index(word[i]))==-1) key_idx[i+1] = key_idx[0];
  key_idx[i+1] = triphons_probs->char2index('_');

  double trp;
  for(i=0;i<wsize;i++)
  {
    trp =  triphons_probs->triphon_idx_prob(key_idx[i],key_idx[i+1],key_idx[i+2]);
    if(trp<=triphons_probs->ZERO_PROB)
      return false;
  }

	return true;
}
/*
bool Rules::isSymphwno(char c)
{
	return symfwna.find_first_of(c)!=sstring::npos;
}
*/

void Rules::read_greeklish2phon_rules(sstring ifs_name)
{
	fstream ifs;

	ifs.open(ifs_name.c_str(),ios::in);

	if(!ifs)
#if defined CONSOLE_MODE
		cout<<ifs_name<<" not openned"<<endl;
#else
		throw sstring("Greeklish phoneme rules file not found!");
#endif

	read_greeklish2phon_rules(ifs);
}

void Rules::read_greeklish2phon_rules(istream& ifs)
{
	stringvec vec_rules;

  std::string std_token = "";
	sstring key;
	float prob;
	bool new_rule = true;
	while(!ifs.eof())
	{
		ifs>>std_token;

		if(std_token=="#")
		{
			greeklish2phon_rules.insert(map_string_stringvec::value_type(key,vec_rules));
			new_rule = true;
			continue;
		}

		if(new_rule)
		{
			new_rule = false;
			key = std_token.c_str();
			vec_rules.clear();

      sstring::size_type keysz = key.size();
			for(sstring::size_type p=0;p<keysz;p++)
			{
				if(letters.find_first_of(key[p])==sstring::npos)
				{
					letters += key[p];
				}

				if(!isalpha(key[p]))
				{
					if(nonalpha_letters.find_first_of(key[p])==sstring::npos)
					{
					  nonalpha_letters += key[p];
					}
				}
			}
		}
		else
		{
			ifs>>prob;
			vec_rules.push_back(sstring(std_token.c_str()));
			rules_probabilities.insert(map_string_double::value_type(key+std_token.c_str(),prob));
		}
	}
}

/*
void Rules::read_diphone_probabilities(sstring ifs_name)
{
	fstream ifs;

	ifs.open(ifs_name.c_str(),ios::in);

	if(!ifs)
#if defined CONSOLE_MODE
		cout<<ifs_name<<" not openned"<<endl;
#else
		throw sstring("Greeklish diphone probabilities file not fount!");
#endif

	read_diphone_probabilities(ifs);
}
void Rules::read_diphone_probabilities(istream& ifs)
{
	sstring key = "";
	double prob = 0.0;
	while(!ifs.eof())
	{
		ifs>>key>>prob;

		if(ifs.eof()) break;
		diphones_probabilities.insert(map_string_double::value_type(key,prob));
	}
}*/


void Rules::read_greek_from_greeklish_rules(sstring ifs_name)
{
	fstream ifs;

	ifs.open(ifs_name.c_str(),ios::in);

	if(!ifs)
#if defined CONSOLE_MODE
		cout<<ifs_name<<" not openned"<<endl;
#else
		throw sstring("Greeklish diphone probabilities file not fount!");
#endif

	read_greek_from_greeklish_rules(ifs);
}
void Rules::read_greek_from_greeklish_rules(istream& ifs)
{
	std::string key = "";
	double prob = 0.0;
	while(!ifs.eof())
	{
		ifs>>key>>prob;

		if(ifs.eof()) break;
		greek_from_greeklish_rules.insert(map_string_double::value_type(sstring(key.c_str()),prob));
	}
}           


void Rules::read_extensions_domains(sstring ifs_name)
{
	fstream ifs;

	ifs.open(ifs_name.c_str(),ios::in);

	if(!ifs)
#if defined CONSOLE_MODE
		cout<<ifs_name<<" not openned"<<endl;
#else
		throw sstring("Extensions&domains file not fount!");
#endif

	read_extensions_domains(ifs);
}
void Rules::read_extensions_domains(istream& ifs)
{
	std::string std_key = "";
	sstring key = "";
	//double prob = 0.0;
  extensions_domains.clear();
	while(!ifs.eof())
	{
		ifs>>std_key;
		if(ifs.eof()) break;
    key = std_key.c_str();

    lower_string(&key);
    extensions_domains.insert(key);
		//if(!extensions_domains.insert(key).second) ::MessageBox(0,key.c_str(),"",MB_OK);
	}
}

double Rules::match_greek_greeklish(sstring greek, sstring greeklish, int &num_of_rules)
{
  lower_destress_greek(&greek);
  lower_string(&greeklish);
             
  int ret_num_of_rules=1;
  double ret = match_greek_greeklish_rec(greek,greeklish,ret_num_of_rules);
  num_of_rules = ret_num_of_rules;
  return ret;
}

double Rules::match_greek_greeklish_rec(sstring greek, sstring greeklish, int &nfr)
{
	double ret = 1, dtmp = 0.0;
  int ret_num_of_rules = 0, num_of_rules;

	if(greek.size() == 0)
  {
    nfr = 0;
    return greeklish.size()?0.0:ret;
  }
	if(greeklish.size() == 0)
  {         
    nfr = 0;
    return 0.0;
  }

	sstring skey = "";
	map_string_double::iterator it;

	//1 1
	skey += greek[0];
	skey += greeklish[0];
	it = greek_from_greeklish_rules.find(skey);
	if(it!=greek_from_greeklish_rules.end())
	{
		dtmp = it->second;
		dtmp *= match_greek_greeklish_rec(greek.substr(1),greeklish.substr(1),num_of_rules);
	}
  else
  {
    dtmp = 0.00001;
		dtmp *= match_greek_greeklish_rec(greek.substr(1),greeklish.substr(1),num_of_rules);
  }
	ret = dtmp;
  ret_num_of_rules = num_of_rules;

	//1 2
	if(greeklish.size()>=2)
	{
		skey = "";
		skey += greek[0];
		skey += greeklish.substr(0,2);
		it = greek_from_greeklish_rules.find(skey);
		if(it!=greek_from_greeklish_rules.end())
		{
			dtmp = it->second;
			dtmp *= match_greek_greeklish_rec(greek.substr(1),greeklish.substr(2),num_of_rules);
		}
	}

	if(dtmp>ret)
  {
    ret = dtmp;
    ret_num_of_rules = num_of_rules;
  }

	//2 1
	if(greek.size()>=2)
	{
		skey = "";
		skey += greek.substr(0,2);
		skey += greeklish[0];
		it = greek_from_greeklish_rules.find(skey);
		if(it!=greek_from_greeklish_rules.end())
		{
			dtmp = it->second;
			dtmp *= match_greek_greeklish_rec(greek.substr(2),greeklish.substr(1),num_of_rules);
		}
	}

	if(dtmp>ret)
  {
    ret = dtmp;
    ret_num_of_rules = num_of_rules;
  }

	//2 2
	if(greek.size()>=2 && greeklish.size()>=2)
	{
		skey = "";
		skey += greek.substr(0,2);
		skey += greeklish.substr(0,2);
		it = greek_from_greeklish_rules.find(skey);
		if(it!=greek_from_greeklish_rules.end())
		{
			dtmp = it->second;
			dtmp *= match_greek_greeklish_rec(greek.substr(2),greeklish.substr(2),num_of_rules);
		}
	}

	if(dtmp>ret)
  {
    ret = dtmp;
    ret_num_of_rules = num_of_rules;
  }

  nfr = ret_num_of_rules+1;

	return ret;
}

sstring Rules::phoneme2greek(sstring phon_word, sstring greeklish)
{
	sstring ret = "";

  sstring::size_type phon_wordsz = phon_word.size();
	for(sstring::size_type i=0;i<phon_wordsz;i++)
	{
		switch(phon_word[i])
		{
			case 'a': ret += "�"; break;
			case 'b': ret += "��"; break;
			case 'c': ret += "�"; break;
			case 'D':
			{
				if(phon_word[i+1]=='j') {ret+="��"; i++; break;}
				ret += "�"; break;
			}
			case 'd': ret += "��"; break;
			case 'e': ret += "�"; break;
			case 'f': ret += "�"; break;
			case 'g': ret += "��"; break;
			case 'G': ret += "��"; break;	//!
			case 'h': ret += "�"; break;	//!
			case 'I': ret += "�"; break;	//!
			case 'i': ret += "�"; break;
			case 'j': ret += "�"; break;
			case 'J': ret += "�"; break;
			case 'k':
			{
				if(phon_word[i+1]=='s') {ret+="�"; i++; break;}
				ret += "�"; break;
			}
			case 'l': ret += "�"; break;
			case 'L': ret += "��"; break;
			case 'm': ret += "�"; break;
			case 'M': ret += "�"; break;
			case 'n': ret += "�"; break;
			case 'N': ret += "��"; break;
			case 'o': ret += "�"; break;
			case 'p':
			{
				if(phon_word[i+1]=='s') {ret+="�"; i++; break;}
				ret += "�"; break;
			}
			case 'r': ret += "�"; break;
			case 's': ret += "�"; break;
			case 'T': ret += "�"; break;
			case 't': ret += "�"; break;
			case 'u': ret += "��"; break;
			case 'v': ret += "�"; break;
			case 'x': ret += "�"; break;
			case 'X': ret += "�"; break;
			case 'z': ret += "�"; break;
		}
	}

 #ifdef KEEP_LOG
  fstream flog;
  flog.open("phon2gr_log.txt",ios::out|ios::app);

  if(flog)
  {
    flog<<greeklish + '\t' + phon_word + '\t' + ret + '\n';
    flog.close();
  }
 #endif //KEEP_LOG

	return ret;
}

sstring Rules::phon_grk_2_gr(sstring phon, sstring grk)
{
	sstring ret = "";

  //todo
  if(phon=="tri" && grk=="3"){
    ret = "���";
    return ret;
  }

  sstring::size_type phonsz = phon.size();
 	for(sstring::size_type i=0,j=0;i<phonsz;i++)
	{
    if(grk[j]==0) break;
		switch(phon[i])
		{
			case 'i':
        if(grk[j]=='i') {ret += "�"; break;}
        if(grk[j]=='h' || grk[j]=='n') {ret += "�"; break;}
        if(grk[j]=='y' || grk[j]=='u') {ret += "�"; break;}
        if(grk[j]=='e' && grk[j+1]=='i') {ret += "��"; j++; break;}
        if(grk[j]=='o' && grk[j+1]=='i') {ret += "��"; j++; break;}

        break;
			case 'a':
        ret += "�";
        if(phon[i+1]=='f' || phon[i+1]=='v')
        {
          ret+="�"; i++; j++;
        }
        break;
			case 'b':
        ret += "��";
        break;
			case 'c':
        ret += "�";
        if(grk[j+1]=='k') {ret += "�";j++;}
        if(phon[i+1]!='i')
        {
          if(grk[j+1]=='i') {ret += "�";j++;}
          if(grk[j+1]=='h' || grk[j+1]=='n') {ret += "�"; j++;}
          if(grk[j+1]=='y' || grk[j+1]=='u') {ret += "�"; j++;}
          if(grk[j+1]=='e' && grk[j+2]=='i') {ret += "��"; j+=2;}
          if(grk[j+1]=='o' && grk[j+2]=='i') {ret += "��"; j+=2;}
        }
        break;
			case 'D':
        ret += "�";
        if(phon[i+1]=='j')
        {
          ret += "�"; i++;
          j++;
          if(grk[j+1]=='o' || grk[j+1]=='e') j++;
        }
        break;
			case 'd':
        ret += "��";
        if(grk[j]=='n' || grk[j]=='v') j++;
        break;
			case 'e':
        if(grk[j]=='e')
        {
          ret += "�";
          if(phon[i+1]=='f' || phon[i+1]=='v')
          {
            ret+="�"; i++; j++;
          }
        }
        else
        {
          ret += "��"; j++;
        }
        break;
			case 'f':
        if(grk[j]=='p') {ret += "�"; j++;}
        else if(grk[j]=='f' || grk[j]=='q') {ret += "�";}
        else
        {
          ret += "�";
        }
        break;
			case 'g':
        if(grk[j]=='n') {ret += "��"; j++;}
        else if(grk[j]=='g')
        {
          if(grk[j+1]=='g') {ret += "��"; j++;}
          else if(grk[j+1]=='k') {ret += "��"; j++;}
          else  ret += "��";
        }
        else
        {
          if(grk[j+1]=='j') {ret += "��"; j++;}
          else if(grk[j+1]=='k') {ret += "��"; j++;}
          else  ret += "��";
        }
        break;
			case 'G':
        if(grk[j]=='n') {ret += "��"; j++;}
        else if(grk[j]=='g')
        {
          if(grk[j+1]=='g') {ret += "��"; j++;}
          else if(grk[j+1]=='k') {ret += "��"; j++;}
          else  ret += "��";
        }
        else
        {
          if(grk[j+1]=='j') {ret += "��"; j++;}
          else if(grk[j+1]=='k') {ret += "��"; j++;}
          else  ret += "��";
        }
        if(phon[i+1]!='i')
        {
          if(grk[j+1]=='i') {ret += "�";j++;}
          if(grk[j+1]=='h' || grk[j+1]=='n') {ret += "�"; j++;}
          if(grk[j+1]=='y' || grk[j+1]=='u') {ret += "�"; j++;}
          if(grk[j+1]=='e' && grk[j+2]=='i') {ret += "��"; j+=2;}
          if(grk[j+1]=='o' && grk[j+2]=='i') {ret += "��"; j+=2;}
        }
        break;
			case 'h':
        if(grk[j]=='n') {ret += "�";}
        else
        {
          ret += "�";
        }
        break;
			case 'I':
        if(grk[j]=='i') ret += "�";
        break;
			case 'j':
        ret += "�";
        if(phon[i+1]!='i')
        {
          if(grk[j+1]=='i') {ret += "�";j++;}
          if(grk[j+1]=='h' || grk[j+1]=='n') {ret += "�"; j++;}
          if(grk[j+1]=='y' || grk[j+1]=='u') {ret += "�"; j++;}
          if(grk[j+1]=='e' && grk[j+2]=='i') {ret += "��"; j+=2;}
          if(grk[j+1]=='o' && grk[j+2]=='i') {ret += "��"; j+=2;}
        }
        break;
			case 'J':
        ret += "�";
        break;
			case 'k':
        if(phon[i+1]=='s')
        {
          ret+="�"; i++;
          if(grk[j]=='k') j++;
          break;
        }
				ret += "�";
        if(grk[j+1]=='k') {ret += "�";j++;}
        break;
			case 'l':
        ret += "�";
        if(grk[j+1]=='l') {ret += "�";j++;}
        break;
			case 'L':
        ret += "�";
        if(grk[j+1]=='l') {ret += "�";j++;}
        if(phon[i+1]!='i')
        {
          if(grk[j+1]=='i') {ret += "�";j++;}
          if(grk[j+1]=='h' || grk[j+1]=='n') {ret += "�"; j++;}
          if(grk[j+1]=='y' || grk[j+1]=='u') {ret += "�"; j++;}
          if(grk[j+1]=='e' && grk[j+2]=='i') {ret += "��"; j+=2;}
          if(grk[j+1]=='o' && grk[j+2]=='i') {ret += "��"; j+=2;}
        }
        break;
			case 'm':
        ret += "�";
        if(grk[j+1]=='m') {ret += "�";j++;}
        break;
			case 'M':
        ret += "�";
        break;
			case 'n':
        ret += "�";
        if(grk[j+1]=='n' ||
            (grk[j+1]=='v' && phon[i+1]!='o')) {ret += "�";j++;}
        break;
			case 'N':
        ret += "�";
        if(grk[j+1]=='n' || grk[j+1]=='v') {ret += "�";j++;}
        if(phon[i+1]!='i')
        {
          if(grk[j+1]=='i') {ret += "�";j++;}
          if(grk[j+1]=='h' || grk[j+1]=='n') {ret += "�"; j++;}
          if(grk[j+1]=='y' || grk[j+1]=='u') {ret += "�"; j++;}
          if(grk[j+1]=='e' && grk[j+2]=='i') {ret += "��"; j+=2;}
          if(grk[j+1]=='o' && grk[j+2]=='i') {ret += "��"; j+=2;}
        }
        break;
			case 'o':
        if(grk[j]=='o') {ret += "�";}
        else
        {
          ret += "�";
        }
        break;
			case 'p':
        if(phon[i+1]=='s')
        {
          ret+="�"; i++;
          if(grk[j]=='p') j++;
          break;
        }
				ret += "�";
        if(grk[j+1]=='p') {ret += "�";j++;}
        break;
			case 'r':
        ret += "�";
        if(grk[j+1]=='r') {ret += "�";j++;}
        break;
			case 's':
				ret += "�";
        if(grk[j+1]=='s' ||
          (grk[j+1]=='6' && (phon[i+1]!='D'||phon[i+1]!='T'))) {ret += "�";j++;}
        break;
			case 'T':
        ret += "�";
        if(grk[j+1]=='t') j++;
        break;
			case 't':
        if(phon[i+1]=='s' && grk[j]!='t')
        {
          ret+="��"; i++;
          break;
        }
				ret += "�";
        break;
			case 'u':
        ret += "��";
        if(grk[j]=='o') j++;
        break;
			case 'v':
        if(grk[j]=='b' || grk[j]=='v')
        {
          ret += "�";
          if(grk[j+1]=='b' || grk[j+1]=='v') {ret += "�";j++;}
        }
        else
        {
          ret += "�";
        }
        break;
			case 'x':
        ret += "�";
        if(grk[j]=='c') j++;
        break;
			case 'X':
        if(grk[j]=='i') {ret += "�"; j++; break;}
        if(grk[j]=='o' && grk[j+1]=='i') {ret += "��"; j+=2; break;}
        ret += "�";
        if(grk[j]=='c') j++;
        break;
			case 'z':
        if(grk[j]=='s') ret += "�";
        else ret += "�";
        break;
		}
    j++;
	}

	return ret;
}
static const sstring grup  = "������������������������ٸ�������ۺ�";
static const sstring grupb = "������������������������������������";
static const sstring grlow = "������������������������������������";

char Rules::upper_greek(char grch)
{
  sstring::size_type pos = grlow.find(grch);
  return (pos!=sstring::npos)?grup[pos]:grch;
}
char Rules::upper_greek_bare(char grch)
{
  sstring::size_type pos = grlow.find(grch);
  return (pos!=sstring::npos)?grupb[pos]:grch;
}

char Rules::stress_greek_vowel(char c)
{
  switch(c)
	{
	  case '�': return '�';
    case '�': return '�';
		case '�': return '�';
		case '�': return '�';
		case '�': return '�';
		case '�': return '�';
		case '�': return '�';
	}
  return c;
}

static const sstring sentence_endings   = ",.?!;;";
bool Rules::is_sentence_end(sstring &word)
{
  sstring::size_type i=0;
  sstring::size_type wsz = word.size();
  if(!wsz) return false;
  while(myisspace(word[i])) i++;
  if(i<wsz && sentence_endings.find(word[i])!=sstring::npos) return true;
  i = word.size()-1;
  while(myisspace(word[i]) && i>0) i--;                                       
  if(i>0 && sentence_endings.find(word[i])!=sstring::npos) return true;
  return false;
}
  

static const sstring greek_stressed   = "���������";
bool Rules::is_greek_word_stressed(sstring &word)
{
  sstring::size_type wsz = word.size();
  for(sstring::size_type i=0;i<wsz;i++)
    if(greek_stressed.find(word[i])!=sstring::npos) return true;
  return false;
}

static const sstring greek_stressed_dial   = "�����������";
static const sstring greek_unstressed = "�����������";
//supposed lower greek input
sstring Rules::unstress_greek(sstring in)
{
  sstring::size_type insz = in.size(), pos;
  for(sstring::size_type i=0;i<insz;i++)
  {
    if((pos = greek_stressed_dial.find(in[i]))!=sstring::npos)
      in[i] = greek_unstressed[pos];
  }
  return in;
}


} //ILSP_GREEKLISH

