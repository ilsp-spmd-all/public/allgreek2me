// Greeklish2Greek.h: interface for the Greeklish2Greek class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _GREEKLISH2GREEK_H_
#define _GREEKLISH2GREEK_H_

#include "sstring/sstring.h"
#include <string>
#include <vector>
using namespace std;

#include "Globals.h"
#include "Rules.h"
#include "PhonNode.h"
#include "indexreader.h"
#include "mapfilereader.h"

namespace ILSP_GREEKLISH{

class out_fragment
{
public:
  sstring original_text, converted_text;
  bool original_found, converted_found, done;
  float original_text_prob, converted_text_prob,
        converted_dic_prob, converted_back_prob, converted_triph_prob;
  list<dic_word_prob> converted_candidates;

  out_fragment(sstring in, sstring out = sstring(""),
               bool inf = false, bool outf = false,
               float inp = 0, float outp = 0,
               float outdicp = 0, float outbp = 0, float outtrp = 0, bool dn = false);

  sstring &getDefault();

  sstring getProccessed();
};

class Greeklish2Greek  
{
private:
	MapFileReader *greeklish_dicindex, *english_dicindex;
  sstring path;


	bool needs_proccessing, foundGreek, english_found;
	sstring word_r, word, mostProbPhon, mostProbGreek;

  bool analyse; 
  list<dic_word_prob> word_candidates;

private:

  bool next_word2convert(sstring &text, sstring &text_r, int pos_beg, int &pos_word_start, int &pos_word_end);
	void proccess();

  //void stress_word(sstring &word);
  void capitalise_word(sstring &word, sstring &word_r);

public:
	double max_gr_prob, dic_gr_prob, dic_eng_prob, back_gr_prob, triph_gr_prob;
  //!
  //word_prob dbg_wp;
	bool findGreek();
	sstring getGreek();
	sstring getMostProbPhon();
	sstring getMostProbGreek();

	Greeklish2Greek();
	virtual ~Greeklish2Greek();

	void setWord(sstring word, sstring word_r);
  
	void convertText2Vec(sstring text, vector<out_fragment> &res);
	sstring convertTextCustom(sstring text);                      

	void convertAnalyse(sstring word, sstring &analysis);

};


} //ILSP_GREEKLISH

#endif // _GREEKLISH2GREEK_H_


