#include "Greek2Greeklish.h"
#include "Globals.h"
#include "GrkRes.rh"  

#ifdef ALLGREEK_EXE
  #ifdef _WIN32
  #include "ProgressUnit.h"
  #endif //_WIN32
#endif //ALLGREEK_EXE

#include <fstream>
using namespace std;

namespace ILSP_GREEKLISH{

static const sstring greek_non_voiced = sstring("�������������������");
static const sstring allowed_chars = sstring("������������������������������������_CV()");
   
//------------------------------------------------------------------------------
bool match_context(const sstring &sgVC, sstring &sg)
{
  sstring::size_type sgVCsz = sgVC.size();
  if(sgVCsz!=sg.size()) return false;
  for(sstring::size_type i=0;i<sgVCsz;i++)
  {
    if(sgVC[i]==sg[i]) continue;
    if(sgVC[i]=='V')
      if(greek_non_voiced.find(sg[i])!=sstring::npos) return false; else continue;
    if(sgVC[i]=='C')
      if(greek_non_voiced.find(sg[i])==sstring::npos) return false; else continue;
    if(sgVC[i]=='_')
      if(rules->is_greek_letter(sg[i])) return false; else continue;
    return false;
  }
  return true;
} 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
bool gr2grl_rule::match(sstring &text, int pos) const {
  sstring lc, rc;
  int text_sz = text.size(), left_context_sz, right_context_sz;

  if(greek!=text.substr(pos,greek.size())) return false;

  left_context_sz = left_context.size();
  if(left_context_sz){ 
    if(!pos && left_context[0]=='_') left_context_sz--;
    if(pos<left_context_sz) return false;
    lc = text.substr(pos-left_context_sz,left_context_sz);
    if(!match_context(left_context, lc)) return false;
  }

  right_context_sz = right_context.size();
  pos+=greek.size();
  if(pos<=text_sz && right_context_sz){
    rc = text.substr(pos,right_context_sz);
    if((text_sz-pos)<right_context_sz && right_context[right_context_sz-1]=='_')
    {
      rc+='_';
      right_context_sz--;
    }
    if((text_sz-pos)<right_context_sz) return false;
    if(!match_context(right_context, rc)) return false;
  }

  return true;
}
   
//------------------------------------------------------------------------------
bool gr2grl_rule::operator==(const gr2grl_rule& other){
  return  other.greek == greek &&
          other.left_context == left_context &&
          other.right_context == right_context;
}
//------------------------------------------------------------------------------
bool operator< (const gr2grl_rule& x, const gr2grl_rule& y) {
  bool ret;
  ret = (
          (x.greek != y.greek)? (x.greek > y.greek) :
                               (x.left_context != y.left_context)? (x.left_context > y.left_context) :
                                                                   (x.right_context > y.right_context)
         );
  return ret;
}
  
//------------------------------------------------------------------------------
ostream& operator<< ( ostream& os, gr2grl_rule& rule ) {
  return os<<rule.getTotRule().c_str();
}     

//------------------------------------------------------------------------------
ostream& operator<< ( ostream& os, gr2grl_rule rule ) {
  return os<<rule.getTotRule().c_str();
}

//------------------------------------------------------------------------------
istream& operator>> ( istream& is, gr2grl_rule& rule ) {
  string greek, greeklish; 
  if(!(is>>greek>>greeklish) && !greek.size() && !greeklish.size())
  {
    rule.valid = false;
    return is;
  }

 if(greek.find_first_not_of(allowed_chars.c_str())!=sstring::npos)
    ;//::MessageBox(0,(sstring("Invalid rule!\r\n")+greek+'\t'+greeklish + "\tdiscarded!").c_str(),"WARNING",MB_OK);

  // [(lc)]gr[(rc)]
  sstring lc = sstring(""), rc = sstring(""), gr = sstring("");
  int left_pars = 0, right_pars = 0;
  bool gr_ok = false;
  bool error = false;
  sstring::size_type greeksz = greek.size();
  for(sstring::size_type i=0;i<greeksz && !error;i++){
    switch(greek[i]){
      case '(':
        left_pars++;
        break;
      case ')':
        right_pars++;
        break;
      case '_':
        if(left_pars==1 && i==1)  lc+= '_';
        else if(gr_ok && i==greek.size()-2 && greek[i+1]==')')  rc+= '_';
        else error=true;
        break;
      case 'V':
      case 'C':
        //if(left_pars==0 && right_pars == 0) {error = true;}
        //else if(left_pars==1 && right_pars == 1) {error = true;}
        if(left_pars==right_pars) {error = true;}
        else if(left_pars==1 && right_pars == 0){
          if(gr_ok) rc+=greek[i]; else lc+=greek[i];
        }
        else if(left_pars==2 && right_pars == 1) rc+=greek[i];
        else error = true;
        break;
      default:
        if((left_pars==0 && right_pars == 0) ||
           (left_pars==1 && right_pars == 1)) {gr_ok = true; gr+=greek[i];}
        else if(left_pars==1 && right_pars == 0){
          if(gr_ok) rc+=greek[i]; else lc+=greek[i];
        }
        else if(left_pars==2 && right_pars == 1) rc+=greek[i];
        else error = true;
    }
  }
  if(error || left_pars!=right_pars || left_pars>2 || !gr.size())
  {
    rule.valid = false;
    ;//::MessageBox(0,(sstring("Invalid rule!\r\n")+greek+'\t'+greeklish + "\tdiscarded!").c_str(),"WARNING",MB_OK);
  }
  else
  {
    rule.valid = true;
    rule.greek = gr;
    rule.greeklish = greeklish.c_str();
    rule.left_context = lc;    
    rule.right_context = rc;
  }

  return is;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
Greek2Greeklish::Greek2Greeklish(const char *customrules_file)
{
  customrules.clear();

  fstream rules_file, discared_rules;
  customrules_maxlen = 1;

  custom_rules_file = sstring("") + customrules_file;
                                
  rules_file.open(customrules_file,ios::in);
  //discared_rules.open((sstring("discared_")+customrules_file).c_str(),ios::out);

  if(rules_file.is_open()/* && discared_rules.is_open()*/)
  {
    init_rules(rules_file, customrules, customrules_maxlen);
  }

  char *buf;
	buf = GetResource(IDR_COMMONGR2GRL, "TEXT");
  if(buf){
    istream *is = new istrstream(buf,strlen(buf));
    init_rules(*is, defaultrules, defaultrules_maxlen);
	  delete[] buf;
    delete is;
  }

  /*fstream dbg;
  dbg.open("customrules.txt",ios::out);
  save_custom_gr2grl(dbg);
  dbg.close();*/
}    

bool Greek2Greeklish::is_valid_rule(const char *rule){ 
  istrstream rule_stream(rule);

  gr2grl_rule new_rule;
  rule_stream>>new_rule;
  
  return new_rule.valid;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Greek2Greeklish::load_custom_gr2grl(istream &rules_stream) {
  if(rules_stream)
    init_rules(rules_stream, customrules, customrules_maxlen);
}

//------------------------------------------------------------------------------
void Greek2Greeklish::save_custom_gr2grl(ostream &rules_stream) {
  gr2grl_ruleset::reverse_iterator customrules_it;

  for(customrules_it=customrules.rbegin();customrules_it!=customrules.rend();customrules_it++)
    rules_stream<<(gr2grl_rule)*customrules_it<<endl;
}   
 
//------------------------------------------------------------------------------
void Greek2Greeklish::load_custom_gr2grl() {
  fstream rules_file;
  rules_file.open(custom_rules_file.c_str(),ios::in);

  load_custom_gr2grl(rules_file);
}

//------------------------------------------------------------------------------
void Greek2Greeklish::save_custom_gr2grl() {
  fstream rules_file;
  rules_file.open(custom_rules_file.c_str(),ios::out);

  save_custom_gr2grl(rules_file);
}
                                    
//------------------------------------------------------------------------------
void Greek2Greeklish::import_custom_gr2grl(const char *rules_str)
{
  istrstream rule_stream(rules_str);
  load_custom_gr2grl(rule_stream);
}

//------------------------------------------------------------------------------
sstring Greek2Greeklish::export_rules(bool custom)
{
  sstring ret = sstring("");
  gr2grl_ruleset::reverse_iterator customrules_it, bg, nd;

  if(custom) {bg = customrules.rbegin(); nd = customrules.rend();}
  else {bg = defaultrules.rbegin(); nd = defaultrules.rend();}

  for(customrules_it=bg;customrules_it!=nd;customrules_it++)
    ret += customrules_it->getTotRule() + '\n';

  return ret;
}
  
//------------------------------------------------------------------------------
void Greek2Greeklish::init_rules(istream &rules_stream, gr2grl_ruleset &dest_set, int &customrules_maxlen)
{
  try
  {
    dest_set.clear();
    customrules_maxlen = 1;
    gr2grl_rule new_rule;
    while(rules_stream>>new_rule)
    {
      if(!new_rule.valid) continue;
                                          
      if(new_rule.greek.size()>(sstring::size_type)customrules_maxlen) customrules_maxlen = new_rule.greek.size();

      pair<gr2grl_ruleset::iterator, bool> ins_ret;
      ins_ret = dest_set.insert(new_rule);

      //if(!ins_ret.second)
      //  ::MessageBox(0,(sstring("Rule conflict!\r\n")+new_rule.getTotRule() + "\tdiscarded!").c_str(),"WARNING",MB_OK);
    } 
  }
  catch(...)
  {
    dest_set.clear();
  }
}

//------------------------------------------------------------------------------
sstring Greek2Greeklish::greek2greeklish(sstring &greek_r, gr2grl_ruleset &gr2grlrules, int &gr2grlrules_maxlen)
{
#ifdef ALLGREEK_EXE
  #ifdef _WIN32
    ProgressForm->bCanShow = ProgressForm->bCanShow && greek_r.size()>2000;
    if(ProgressForm->bCanShow){
      ProgressForm->Position = poOwnerFormCenter;
      ProgressForm->Show();
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE

  sstring ret = sstring("");
  sstring greek = greek_r;

  lower_string_greek(&greek);

  gr2grl_ruleset::iterator gr2grlrules_it;
  int pos = 0, greek_sz = greek.size();

#ifdef ALLGREEK_EXE
  #ifdef _WIN32
    int prg_range = greek_sz, prg_pos = 0;
    ProgressForm->setRange(0,prg_range);
  #endif //_WIN32
#endif //ALLGREEK_EXE

  while(pos<greek_sz){
#ifdef ALLGREEK_EXE
  #ifdef _WIN32
    if(ProgressForm->bCanShow){
      if(((float)prg_pos)/prg_range > 0.01){
        ProgressForm->incrPos(pos - prg_pos);
      }
      prg_pos = pos;
      Application->ProcessMessages();
      if(ProgressForm->mustStop()) break;
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE
    gr2grlrules_it = gr2grlrules.begin();
    char gr_at_pos = greek[pos];
    if(rules->is_greek_letter(gr_at_pos)){
      //check for matching only rules that have .greek begining with greek[pos]
      //move iterator to the first candidate
      while(gr2grlrules_it!=gr2grlrules.end() &&
        gr2grlrules_it->greek[0]!=gr_at_pos) gr2grlrules_it++;
      while(gr2grlrules_it!=gr2grlrules.end() &&
        gr2grlrules_it->greek[0]==gr_at_pos &&
        !gr2grlrules_it->match(greek,pos)) {
        gr2grlrules_it++;
      }
      if(gr2grlrules_it!=gr2grlrules.end() && gr2grlrules_it->greek[0]!=gr_at_pos)
        gr2grlrules_it = gr2grlrules.end();
    }
    else
      gr2grlrules_it = gr2grlrules.end();
    if(gr2grlrules_it!=gr2grlrules.end()){//matched
      sstring grl = gr2grlrules_it->getGreeklish();
      int frag_sz = gr2grlrules_it->greek.size();

      bool all_gr_up = true;
      for(int i=pos;i<pos+frag_sz;i++)
      {
        if(greek[i]==greek_r[i]) {all_gr_up = false; break;};
      }

      if(all_gr_up)
      {
        for(sstring::size_type i=0;i<grl.size();i++)
          grl[i] = toupper(grl[i]);
      }
      else if(greek[pos]!=greek_r[pos])
      {
        grl[0] = toupper(grl[0]);
      }

      ret+=grl;
      pos+=frag_sz;
    }
    else{
      ret+=greek_r[pos];
      pos++;
    }
  }

#ifdef ALLGREEK_EXE
  #ifdef _WIN32
    ProgressForm->Hide();
  #endif //_WIN32
#endif //ALLGREEK_EXE

  return ret;
}

//------------------------------------------------------------------------------
sstring Greek2Greeklish::custom_greek2greeklish(sstring greek_r)
{
  sstring ret = default_greek2greeklish(greek2greeklish(greek_r, customrules, customrules_maxlen));
                         
#ifdef ALLGREEK_CHECK_LICENSE
  MASK_TEXT_WITH_LIC_DIFF(ret);
#else
  MASK_TEXT_WITH_VOLDIFF(ret);
#endif    
  return ret;
}   

//------------------------------------------------------------------------------
sstring Greek2Greeklish::default_greek2greeklish(sstring greek)
{
  sstring ret = greek2greeklish(greek, defaultrules, defaultrules_maxlen); 

#ifdef ALLGREEK_CHECK_LICENSE
  MASK_TEXT_WITH_LIC_DIFF(ret);
#else
  MASK_TEXT_WITH_VOLDIFF(ret);
#endif
  return ret;
}

#define check_next_yfv(src,idx,res,voiced) \
      voiced = (src[idx+1]=='\0' || src[idx+2]=='\0')?false:(greek_non_voiced.find(src[idx+2])==sstring::npos);  \
      if(src[idx+1]=='�')         \
      {                           \
        ret += voiced?'v':'f';    \
        idx++;                    \
      }                           \
      if(src[idx+1]=='�')         \
      {                           \
        ret += voiced?'V':'F';    \
        idx++;                    \
      }

//------------------------------------------------------------------------------
sstring Greek2Greeklish::elot742_greek2greeklish(sstring greek)
{
  //!

  sstring ret = sstring("");

  try
  {
    bool voiced = true;
    int i, szgr = greek.size();
    for(i=0;i<szgr;i++)
    {
      switch(greek[i])
      {
      case '�':
        ret += 'a';
        check_next_yfv(greek,i,ret,voiced);
        break;
      case '�':
        ret += 'a';
        break;
      case '�':
          ret += 'A';
        check_next_yfv(greek,i,ret,voiced);
        break;
      case '�':
        ret += 'A';
        break;
      case '�':
        ret += 'v';
        break;
      case '�':
        ret += 'V';
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�': ret+="ng"; i++; break;
        case '�': ret+="gk"; i++; break;
        case '�': ret+="nx"; i++; break;
        case '�': ret+="nch"; i++; break;
        case '�': ret+="nG"; i++; break;
        case '�': ret+="gG"; i++; break;
        case '�': ret+="nX"; i++; break;
        case '�': ret+="nCH"; i++; break;
        default: ret+='g';
        }
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�': ret+="Ng"; i++; break;
        case '�': ret+="Gk"; i++; break;
        case '�': ret+="Nx"; i++; break;
        case '�': ret+="Nch"; i++; break;
        case '�': ret+="NG"; i++; break;
        case '�': ret+="GG"; i++; break;
        case '�': ret+="NX"; i++; break;
        case '�': ret+="NCH"; i++; break;
        default: ret+='G';
        }
        break;
      case '�':
        ret += 'd';
        break;
      case '�':
        ret += 'D';
        break;
      case '�':
        ret += 'e';
        check_next_yfv(greek,i,ret,voiced);
        break;
     case '�':
        ret += 'e';
        break;
     case '�':
        ret += 'E';
        check_next_yfv(greek,i,ret,voiced);
        break;
      case '�':
        ret += 'E';
        break;
      case '�':
        ret += 'z';
        break;
      case '�':
        ret += 'Z';
        break;
      case '�':
        ret += 'i';
        check_next_yfv(greek,i,ret,voiced);
        break;
      case '�':
        ret += 'i';
        break;
      case '�':
        ret += 'I';
        check_next_yfv(greek,i,ret,voiced);
        break;
      case '�':
        ret += 'I';
        break;
      case '�':
        ret += "th";
        break;
      case '�':
        ret += "TH";
        break;
      case '�':
      case '�':
      case '�':
      case '�':
        ret += 'i';
        break;
      case '�':
      case '�':
      case '�':
        ret += 'I';
        break;
      case '�':
        ret += 'k';
        break;
      case '�':
        ret += 'K';
        break;
      case '�':
        ret += 'l';
        break;
      case '�':
        ret += 'L';
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�': ret+="mv"; i++; break;
        case '�': if(i==0) ret+='b'; else ret+="mp"; i++; break;
        case '�': ret+="mV"; i++; break;
        case '�': if(i==0) ret+='b'; else ret+="mP"; i++; break;
        default: ret+='m';
        }
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�': ret+="Mv"; i++; break;
        case '�': if(i==0) ret+='B'; else ret+="Mp"; i++; break;
        case '�': ret+="MV"; i++; break;
        case '�': if(i==0) ret+='B'; else ret+="MP"; i++; break;
        default: ret+='M';
        }
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�': ret+="nd"; i++; break;
        case '�': ret+="nt"; i++; break;
        case '�': ret+="nD"; i++; break;
        case '�': ret+="nT"; i++; break;
        default: ret+='n';
        }
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�': ret+="Nd"; i++; break;
        case '�': ret+="Nt"; i++; break;
        case '�': ret+="ND"; i++; break;
        case '�': ret+="NT"; i++; break;
        default: ret+='N';
        }
        break;
      case '�':
        ret += 'x';
        break;
      case '�':
        ret += 'X';
        break;
      case '�':
        switch(greek[i+1])
        {        
        case '�':
        case '�': ret+="ou"; i++; break;
        case '�':
        case '�': ret+="oU"; i++; break;
        default: ret+='o';
        }
        break;
      case '�':
        ret += 'o';
        break;
      case '�':
        switch(greek[i+1])
        {
        case '�':
        case '�': ret+="Ou"; i++; break;
        case '�':
        case '�': ret+="OU"; i++; break;
        default: ret+='O';
        }
        break;
      case '�':
        ret += 'O';
        break;
      case '�':
        ret += 'p';
        break;
      case '�':
        ret += 'P';
        break;
      case '�':
        ret += 'r';
        break;
      case '�':
        ret += 'R';
        break;
      case '�':
      case '�':
        ret += 's';
        break;
      case '�':
        ret += 'S';
        break;
      case '�':
        ret += 't';
        break;
      case '�':
        ret += 'T';
        break;
      case '�':
      case '�':
      case '�':
      case '�':
        ret += 'y';
        break;
      case '�':
      case '�':
      case '�':
        ret += 'Y';
        break;
      case '�':
        ret += 'f';
        break;
      case '�':
        ret += 'F';
        break;
      case '�':
        ret += "ch";
        break;
      case '�':
        ret += "CH";
        break;
     case '�':
        ret += "ps";
        break;
      case '�':
        ret += "PS";
        break;
      case '�':
      case '�':
        ret += 'o';
        break;
      case '�':
      case '�':
        ret += 'O';
        break;
      default: ret+=greek[i];
      }
    }
  }
  catch(...)
  {
    ret += "\n|!";
  }
  return ret; 
} 
//------------------------------------------------------------------------------
 

} //ILSP_GREEKLISH


