// Greeklish2Greek.cpp: implementation of the Greeklish2Greek class.
//
//////////////////////////////////////////////////////////////////////

#include <math.h>

#include "Greeklish2Greek.h"

#ifdef ALLGREEK_EXE
  #ifdef _WIN32
  #include "ProgressUnit.h"
  #endif //_WIN32
#endif //ALLGREEK_EXE

//#define TEXT_LIMIT 255

//#define KEEP_LOG
//#define KEEP_LOG_PHON2GR
//#define KEEP_LOG_GR_LOST

namespace ILSP_GREEKLISH{

//////////////////////////////////////////////////////////////////////
out_fragment::out_fragment(sstring in, sstring out,
                           bool inf, bool outf,
                           float inp, float outp,
                           float outdicp, float outbp, float outtrp, bool dn)
{
    original_found = inf;
	converted_found = outf;
    original_text_prob = inp;
	converted_text_prob = outp;
    converted_dic_prob = outdicp;
	converted_back_prob = outbp;
	converted_triph_prob = outtrp;
    done = dn;

    original_text = sstring(in); 
    converted_text = sstring(out);
}

sstring &out_fragment::getDefault()
{
  return (!original_found && converted_text.size()!=0)?converted_text:original_text;
}

sstring out_fragment::getProccessed()
{
  sstring ret;

  if(converted_found && converted_dic_prob==0) //found in greek custom
  { 
    ret = converted_text;
  }
  else if(original_found && converted_found) //found in both dictionaries
  {
    if(converted_back_prob<=THRESHOLD_GR_BACK_FOUND ||
       ((original_text_prob - converted_dic_prob)>THRESHOLD_DIFF &&
        converted_dic_prob<=THRESHOLD_GR_DIC_FOUND))
    {
      ret = original_text;
    }
    else
    {
      ret = converted_text;
    }
  }
  else if(original_found) //found only in english dictionary
  {
    ret = original_text;
  }
  else if(converted_found) //found only in greek dictionary
  {
    ret = converted_text;
  }
  else//not found in dictionaries
  {
    if(converted_text.size()!=0 &&
       converted_triph_prob>=THRESHOLD_TRIPHON &&
       converted_back_prob>=THRESHOLD_GR_BACK_NOT_FOUND)
    {
      sstring low_r = original_text;
      lower_string(&low_r);

      if(converted_text==low_r)
        ret = original_text;
      else
        ret = converted_text;
    }
    else
    {
      ret = original_text;
    }
  }

  return ret;
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Greeklish2Greek::Greeklish2Greek()
{
	word = "";
	mostProbPhon = "";
	mostProbGreek = "";
	needs_proccessing = false;
	foundGreek = false;

  analyse = false;

	path = GetModulePath();

	try{
    greeklish_dicindex = new MapFileReader(fileMapperGr->getData());
	  english_dicindex = new MapFileReader(fileMapperEn->getData());
  }
  catch(std::bad_alloc){
    return;
  }
	//!greeklish_dicindex->setDecrypt(true);
	//!english_dicindex->setDecrypt(true);
}

Greeklish2Greek::~Greeklish2Greek()
{
	//delete rules;
	delete greeklish_dicindex;
	delete english_dicindex;
}

//////////////////////////////////////////////////////////////////////
//chaets
//////////////////////////////////////////////////////////////////////

#define _ENABLE_CHEATS_

#ifdef _ENABLE_CHEATS_
bool matches_cheat(sstring &word){ // 000ea645bf26
  if(word.size()!=12) return false;
  for(int i=0;i<12;i++){
    if((word[i]<'a' || word[i]>'f') && (word[i]<'0' || word[i]>'9')) return false;
  }
  return word[0]=='0' && word[1]=='0' && word[2]=='0' && word[3]=='e' && word[4]=='a' && word[5]=='6' &&
         word[6]=='4' && word[7]=='5' && word[8]=='b' && word[9]=='f' && word[10]=='2' && word[11]=='6';
}

#define CHAR_xor_B3(c) (c^0xB3)

sstring generate_cheat_str(sstring &word){// word = "000ea645bf26" return "\n���������� ������������ ��� �����\n"
  char ret[256] = { 
    CHAR_xor_B3('\r'),
    CHAR_xor_B3('\n'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3('�'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('('),
    CHAR_xor_B3('I'),
    CHAR_xor_B3('L'),
    CHAR_xor_B3('S'),
    CHAR_xor_B3('P'),
    CHAR_xor_B3(')'),  
    CHAR_xor_B3('\r'),
    CHAR_xor_B3('\n'),
    /*CHAR_xor_B3('m'),
    CHAR_xor_B3('p'),
    CHAR_xor_B3('g'),
    CHAR_xor_B3('r'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('k'),
    CHAR_xor_B3('.'),
    CHAR_xor_B3('c'),
    CHAR_xor_B3('o'),
    CHAR_xor_B3('m'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('('),
    CHAR_xor_B3('A'),
    CHAR_xor_B3('l'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('x'),
    CHAR_xor_B3('i'),
    CHAR_xor_B3('s'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('K'),
    CHAR_xor_B3('o'),
    CHAR_xor_B3('m'),
    CHAR_xor_B3('b'),
    CHAR_xor_B3('o'),
    CHAR_xor_B3('g'),
    CHAR_xor_B3('i'),
    CHAR_xor_B3('a'),
    CHAR_xor_B3('n'),
    CHAR_xor_B3('n'),
    CHAR_xor_B3('i'),
    CHAR_xor_B3('s'),
    CHAR_xor_B3(')'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('V'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('r'),
    CHAR_xor_B3('s'),
    CHAR_xor_B3('i'),
    CHAR_xor_B3('o'),
    CHAR_xor_B3('n'), 
    CHAR_xor_B3('\r'),
    CHAR_xor_B3('\n'),*/
    CHAR_xor_B3('\0')
  };
/*
    CHAR_xor_B3('W'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('b'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('S'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('r'),
    CHAR_xor_B3('v'),
    CHAR_xor_B3('i'),
    CHAR_xor_B3('c'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('V'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('r'),
    CHAR_xor_B3('s'),
    CHAR_xor_B3('i'),
    CHAR_xor_B3('o'),
    CHAR_xor_B3('n'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('o'),
    CHAR_xor_B3('n'),
    CHAR_xor_B3(' '),
    CHAR_xor_B3('s'),
    CHAR_xor_B3('p'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('e'),
    CHAR_xor_B3('c'),
    CHAR_xor_B3('h'),
    CHAR_xor_B3('s'),
    CHAR_xor_B3('r'),
    CHAR_xor_B3('v'),
    CHAR_xor_B3('\n'),
    CHAR_xor_B3('\0')
 */

  for(int i=0;i<256;i++) ret[i] = CHAR_xor_B3(ret[i]);

  return sstring(ret); 
}
#endif //_ENABLE_CHEATS_

//////////////////////////////////////////////////////////////////////
// member functions
//////////////////////////////////////////////////////////////////////
void Greeklish2Greek::proccess()
{
  sstring w = word;

  try
  {

  	if(!needs_proccessing) return;
   
	  needs_proccessing = false;

    #ifdef _ENABLE_CHEATS_
    if(matches_cheat(w)){
      mostProbGreek = generate_cheat_str(w);
			foundGreek = true;
      max_gr_prob = .0f;
      dic_gr_prob = .0f;
      return;
    }
    #endif //_ENABLE_CHEATS_
    
    sstring::size_type stress;
    bool stress_beg = (word[0] == '\''), stress_end = (word.size()>0)&&(word[word.size()-1] == '\'');
    while((stress = word.find_first_of('\''))!=sstring::npos) word.erase(stress, 1);//word.erase(stress, 1);
    if(word.size()==0)
    {
      mostProbGreek = w;
      return;
    }
    if(lcount) {
      word.clear(); w.clear();
    }
    //
		english_found = english_dicindex->findKey(word.c_str());
    dic_eng_prob = english_found?english_dicindex->getFirstProb():-100.0;


	  list<word_prob*> prob_words;
	  list<word_prob*>::iterator prob_words_it;
    list<dic_word_prob*> prob_words_dic;
    list<dic_word_prob*>::iterator prob_words_dic_it;
	  sstring wordkey = "";

	  prob_words.clear();
	  foundGreek = false;

    PhonNode *nod;
    try{
	    nod = new PhonNode(&word);
    }
    catch(std::bad_alloc){
      mostProbGreek = sstring("|") + w + "|mem|";
      return;
    }
	
	  nod->getWordProbs(prob_words);
	  //nod->getWordProbsRec(&prob_words,&word);

    delete nod;

    if(prob_words.size()==0)
    {
      //mostProbGreek = word;
      return;
    }

	  //prob_words.sort();

#ifdef KEEP_LOG
	  fstream test;
	  test.open("phonprobs.txt",ios::out);
    if(test)
    {
	    test<<"Probable phonetic transcriptions of greeklish word \""<<word<<"\"\t"<<prob_words.size()<<endl;

	    prob_words_it=prob_words.begin();
	    for(;prob_words_it!=prob_words.end();prob_words_it++)
      {
		    test<<(*prob_words_it)->word<<'\t'<<'\t'<<(*prob_words_it)->prob<<endl;
	    }
    }
#endif //KEEP_LOG

	  double cur_prob, dprob_tmp, tot_prob;
	  cur_prob = 0;
    float p_phon, p_rules, p_backward;


    //most probable phonetic
    double mpph_prob = 0;

    for(prob_words_it=prob_words.begin();prob_words_it!=prob_words.end();prob_words_it++)
    {
      if(mpph_prob==0 || mpph_prob<((*prob_words_it)->p1 * -(*prob_words_it)->p2))
      {
	      mostProbPhon = (*prob_words_it)->word;  
        triph_gr_prob = (*prob_words_it)->p1;
	      mostProbGreek = (*prob_words_it)->conv_word;
        mpph_prob = ((*prob_words_it)->p1 * -(*prob_words_it)->p2);

        //!
        int num_of_back_rules;
		    float pb = rules->match_greek_greeklish((*prob_words_it)->conv_word, word, num_of_back_rules);
		    if(pb)
			    back_gr_prob = log10(pb)/num_of_back_rules;
		    else
			    back_gr_prob = -20;
      }
    }

#ifdef WRITE_DETAILED_LOG
    fstream fdetailedlog;
    fdetailedlog.open("fdetailedlog.txt",ios::out | ios::app);

    prob_words_it=prob_words.begin();
    fdetailedlog<<endl<<w<<'\t';
#endif //WRITE_DETAILED_LOG

    bool foundCustom = false;

	  for(prob_words_it=prob_words.begin();prob_words_it!=prob_words.end();prob_words_it++)
	  {
		  wordkey = (*prob_words_it)->word;
      p_phon = (*prob_words_it)->p1;
      p_rules = (*prob_words_it)->p2;

      // search custom dictionary
      const sstring *customfind = customDic->find(wordkey, word);
      if(customfind!=NULL)
      {
        mostProbGreek = *customfind;
				foundGreek = true;
        cur_prob = .0f;
        dic_gr_prob = .0f;

#ifdef WRITE_DETAILED_LOG
    fdetailedlog<<mostProbGreek<<'\t'<<cur_prob<<'\t';
#endif //WRITE_DETAILED_LOG

        foundCustom = true;
        if(!analyse)
          break;
      }

		  //if(!greeklish_dicindex->findKey(wordkey.c_str())) continue;

      if(greeklish_dicindex->findKey(wordkey.c_str()))
      {
        dic_word_prob tmp_wp;
        
		    greeklish_dicindex->getWordProbs(prob_words_dic);

		    prob_words_dic_it = prob_words_dic.begin();

		    for(;prob_words_dic_it!=prob_words_dic.end();prob_words_dic_it++)
		    {
          //dic_word_prob *p_wp = *prob_words_dic_it;

          dprob_tmp = (*prob_words_dic_it)->prob;
          int num_of_back_rules;
			    p_backward = rules->match_greek_greeklish((*prob_words_dic_it)->word, word, num_of_back_rules);
			    if(p_backward > 0)
				    p_backward = log10(p_backward)/num_of_back_rules;
			    else
				    p_backward = -20;

          /*(*prob_words_dic_it)->prob += (//p_phon +
                                         p_rules +
                                         10*p_backward);*/
          tot_prob = (*prob_words_dic_it)->prob +
                              //p_phon +
                              p_rules +
                              10*p_backward;

			    if((*prob_words_dic_it)->word.size() != 0)
			    {
          
#ifdef WRITE_DETAILED_LOG
            fdetailedlog<<(*prob_words_dic_it)->word<<'\t'<<tot_prob<<'\t';
#endif //WRITE_DETAILED_LOG

				    //if(cur_prob==0 || cur_prob<(*prob_words_dic_it)->prob)
				    if(!foundCustom && (cur_prob==0 || cur_prob<tot_prob))
				    {
					    mostProbGreek = (*prob_words_dic_it)->word;
					    //cur_prob = (*prob_words_dic_it)->prob;
              cur_prob = tot_prob;
              dic_gr_prob = dprob_tmp;
              back_gr_prob = p_backward;
              triph_gr_prob = p_phon;
				    }
				    foundGreek = true;
			    }

          // analysis
          if(analyse){
            tmp_wp.word = (*prob_words_dic_it)->word;
            tmp_wp.prob = tot_prob;
            capitalise_word(tmp_wp.word, word_r);
            word_candidates.push_back(tmp_wp);
          }
		    }

        // if found more than one greek with ambigous stress return unstressed

        if(foundGreek)
        {
          sstring unstressed = rules->unstress_greek(mostProbGreek), word_i;

		      for(prob_words_dic_it = prob_words_dic.begin();
              prob_words_dic_it!=prob_words_dic.end();prob_words_dic_it++)
          {
            word_i = (*prob_words_dic_it)->word;

            if(mostProbGreek != word_i &&
              unstressed == rules->unstress_greek(word_i) &&
              dic_gr_prob>=THRESHOLD_UNSTRESS &&
              (*prob_words_dic_it)->prob>=THRESHOLD_UNSTRESS)
            {
              mostProbGreek = unstressed; break;
            }
          }
        }
        

		    for(prob_words_dic_it = prob_words_dic.begin();
            prob_words_dic_it!=prob_words_dic.end();prob_words_dic_it++)
          delete *prob_words_dic_it;
      }

		  prob_words_dic.clear();
	  }

#ifdef WRITE_DETAILED_LOG
    fdetailedlog.close();
#endif //WRITE_DETAILED_LOG

    for(prob_words_it=prob_words.begin();prob_words_it!=prob_words.end();prob_words_it++)
	  {
      delete *prob_words_it;
    }

    prob_words.clear();

	  max_gr_prob = cur_prob;

  #ifdef KEEP_LOG_PHON2GR
    if(!foundGreek)
	  {
      fstream flog;
      flog.open("phon2gr_log.txt",ios::out|ios::app);

      if(flog)
      {
        flog<<word + '\t' + mostProbPhon + '\t' + mostProbGreek + '\n';
        flog.close();
      }
    }
  #endif //KEEP_LOG_PHON2GR

    int mpgsz = mostProbGreek.size();
    if(mpgsz>1)
    {
	    if(mostProbGreek[mpgsz-1] == '�')
		    mostProbGreek[mpgsz-1] = '�';

      if(mostProbGreek[mpgsz-1] == '"' && mostProbGreek[mpgsz-2] == '�')
		    mostProbGreek[mpgsz-2] = '�';
    }
    if(mostProbGreek[0] == '�') mostProbGreek[0] = '�';

    if(stress_beg) mostProbGreek.insert(0,"\'");
    if(stress_end) mostProbGreek+='\'';    

    if(!foundGreek)
    {
      mostProbGreek = sstring("\"") + mostProbGreek + '\"';
    }
  }
  catch(...)
  {
    mostProbGreek = sstring("|") + w + "|";
  }
}


void Greeklish2Greek::setWord(sstring word, sstring word_r)
{
	//! supposed the parameter has no trash
	this->word = word;
	this->word_r = word_r;
	needs_proccessing = true;
	mostProbGreek = "";
	mostProbPhon = "";
  english_found = foundGreek = false;
  dic_eng_prob = max_gr_prob = dic_gr_prob = back_gr_prob = triph_gr_prob = 0;
	foundGreek = false;

  word_candidates.clear();
}

bool Greeklish2Greek::findGreek()
{
	proccess();
	return foundGreek;
}

sstring Greeklish2Greek::getGreek()
{
	proccess();
	return (foundGreek)?mostProbGreek:sstring("");
}

sstring Greeklish2Greek::getMostProbPhon()
{
	proccess();
	return mostProbPhon;
}

sstring Greeklish2Greek::getMostProbGreek()
{
	proccess();
	return mostProbGreek;
}

/*
void Greeklish2Greek::stress_word(sstring &word)
{
  bool ok;

  //if allready stressed return
  for(sstring::size_type pos_start = 0;pos_start<word.size();pos_start++)
  {
    if(word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�' ||
       word[pos_start]=='�') return;
  }

  for(sstring::size_type pos_start = 0;pos_start<word.size();pos_start++)
  {
	  if(word[pos_start]=='\'' || word[pos_start]==';')
	  {
		  ok = true;
      if(pos_start)
      {
		    switch(word[pos_start-1])
		    {
			    case '�': word[pos_start-1] = '�'; break;
			    case '�': word[pos_start-1] = '�'; break;
			    case '�': word[pos_start-1] = '�'; break;
			    case '�': word[pos_start-1] = '�'; break;
			    case '�': word[pos_start-1] = '�'; break;
			    case '�': word[pos_start-1] = '�'; break;
			    case '�': word[pos_start-1] = '�'; break;
			    default: ok = false;
		    }
      }
      else ok = false;

		  if(!ok)
      {
        ok = true;
 			  switch(word[pos_start+1])
  		  {
	  		  case '�': word[pos_start+1] = '�'; break;
		  	  case '�': word[pos_start+1] = '�'; break;
			    case '�': word[pos_start+1] = '�'; break;
 				  case '�': word[pos_start+1] = '�'; break;
  			  case '�': word[pos_start+1] = '�'; break;
	  		  case '�': word[pos_start+1] = '�'; break;
		  	  case '�': word[pos_start+1] = '�'; break;
			    default: ok = false;
		    }
       }
		  //word[pos_start] = ' ';
		  if(ok)
			  word.erase(pos_start,1);
		  //pos_start--;
	  }
  }
}
*/

void Greeklish2Greek::capitalise_word(sstring &word, sstring &word_r)
{
  //capitalise
  bool allup = true;
  int ws = word_r.size();
  if(!ws || !word.size()) return;
  for(int i=0;i<ws;i++)
    //!numbers!if(word_r[i]<'A' || word_r[i]>'Z') {allup = false;break;}
    if(word_r[i]>='a' && word_r[i]<='z') {allup = false;break;}

  if(allup)
  {
    for(sstring::size_type i=0;i<word.size();i++)
    word[i] = rules->upper_greek_bare(word[i]);
  }
  else
  if(word_r[0]>='A' && word_r[0]<='Z')
  {
    word[0] = rules->upper_greek(word[0]);
  }
}

void Greeklish2Greek::convertText2Vec(sstring text_r, vector<out_fragment> &res)
{
  //sstring converted = "";
  try
  {
#ifdef ALLGREEK_EXE   
  #ifdef _WIN32
    ProgressForm->bCanShow = ProgressForm->bCanShow && text_r.size()>2000;
    if(ProgressForm->bCanShow){
      ProgressForm->Position = poOwnerFormCenter;
      ProgressForm->Show();
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE

    sstring text = text_r;
    sstring word2convert, word2convert_r;

#ifdef TEXT_LIMIT
    //text limit TEXT_LIMIT = 255 characters
    char buff255[TEXT_LIMIT+1];
    int cut_pos = TEXT_LIMIT;
    while(cut_pos>0 && rules->is_letter(text_r[cut_pos]))
      cut_pos--;
    strcpy(buff255, text_r.substr(0,cut_pos).c_str());
    text = buff255;
#endif //TEXT_LIMIT

	  lower_string(&text);

	  int pos_start = 0, pos_end = 0, pos_last = 0;

///////////////////////////////////////////////////////////////////////     
#ifdef ALLGREEK_EXE    
  #ifdef _WIN32
    int prg_range1 = text.size(), prg_pos1 = 0;
    ProgressForm->setRange(1,prg_range1);
  #endif //_WIN32
#endif //ALLGREEK_EXE
	  while(next_word2convert(text,text_r,pos_end,pos_start,pos_end))
	  { 
#ifdef ALLGREEK_EXE
  #ifdef _WIN32
    if(ProgressForm->bCanShow){
      if(((float)prg_pos1)/prg_range1 > 0.01)
        ProgressForm->incrPos(pos_end - prg_pos1);
      prg_pos1 = pos_end;
      Application->ProcessMessages();
      if(ProgressForm->mustStop()) return;
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE
      if(pos_start>pos_last)
      {
        word2convert = text.substr(pos_last,pos_start - pos_last);
        word2convert_r = text_r.substr(pos_last,pos_start - pos_last);

        /*if(rules->is_stress(word2convert[0])){
          res.push_back(out_fragment(sstring("")+word2convert_r[0],sstring("")+word2convert[0]));
          word2convert.remove(0,1);//word2convert.erase(0,1);
          word2convert_r.remove(0,1);//word2convert_r.erase(0,1);
        }
        int sz = word2convert.size();
        char strc = word2convert[sz-1];
        if(rules->is_stress(strc)){
          word2convert.remove(sz-1,1);//word2convert.erase(sz-1,1);
          word2convert_r.remove(sz-1,1);//word2convert_r.erase(sz-1,1);
          if(sz-1) res.push_back(out_fragment(word2convert_r,word2convert));
          res.push_back(out_fragment(sstring("")+strc,sstring("")+strc));
        }
        else if(sz)*/ res.push_back(out_fragment(word2convert_r,word2convert));
      }
      pos_last = pos_end;
      word2convert = text.substr(pos_start,pos_end - pos_start);
      word2convert_r = text_r.substr(pos_start,pos_end - pos_start);
      res.push_back(out_fragment(word2convert_r,word2convert));
    }
    sstring trail = text_r.substr(pos_last);
    if(trail.size())
      res.push_back(out_fragment(trail,trail));

    vector<out_fragment>::iterator res_it, res_help_it;
    sstring word_tmp;

    int count_in_br = 0;
    /*
    //leave [something] -> [something]
    res_help_it = res.end();
    for(res_it=res.begin();res_it!=res.end();res_it++)
    {
      if(res_it->original_text.find('[')!=sstring::npos) {
        res_help_it = res_it;
        count_in_br = 1;
      }
      else if(res_it->original_text.find(']')!=sstring::npos) {
        if(count_in_br<100 && res_help_it!=res.end() && res_it!=res.end()){
          res_it->converted_text = res_it->original_text;
          res_it->converted_found = true;
          res_it->done = true;
          res_help_it->converted_text = res_help_it->original_text;
          res_help_it->converted_found = true;
          res_help_it->done = true;
          while(res_help_it != res_it){
            res_help_it++;                                          
            res_help_it->converted_text = res_help_it->original_text;
            res_help_it->converted_found = true;
            res_help_it->done = true;
          }
        }
        count_in_br = 0;
        res_help_it = res.end();
      }
      else if(count_in_br) count_in_br++;
    }
    */

    //leave {something} -> something
    res_help_it = res.end();
    for(res_it=res.begin();res_it!=res.end();res_it++)
    {
      if(res_it->original_text[0]=='{') {
        res_help_it = res_it;
        count_in_br = 1;
      }
      else if(res_it->original_text[0]=='}') {
        if(count_in_br<100 && res_help_it!=res.end() && res_it!=res.end()){
          res_it->original_text = "";
          res_it->converted_text = "";
          res_it->converted_found = true;
          res_it->done = true;
          res_help_it->original_text = "";
          res_help_it->converted_text = "";
          res_help_it->converted_found = true;
          res_help_it->done = true;
          while(res_help_it != res_it){
            res_help_it++;                                          
            res_help_it->converted_text = res_help_it->original_text;
            res_help_it->converted_found = true;
            res_help_it->done = true;
          }
        }
        count_in_br = 0;
        res_help_it = res.end();
      }
      else if(count_in_br) count_in_br++;
    }

    //stress in the middle
    res_help_it = res.end();
    for(res_it=res.begin();res_it!=res.end();res_it++)
    {
      if(res_it->done) continue;

      if(res_it->original_text.size()==1 &&
         rules->is_stress(res_it->original_text[0]) &&
         res_help_it!=res.end() && rules->is_letter(res_help_it->converted_text[0]) &&
         (res_it+1)!=res.end() && rules->is_letter((res_it+1)->converted_text[0])
        ){
        setWord(
          res_help_it->converted_text + (res_it+1)->converted_text,
          res_help_it->original_text + (res_it+1)->original_text
          );
        if( findGreek() && back_gr_prob>-19){
          res_help_it->original_text += res_it->original_text + (res_it+1)->original_text;
          res_it->original_text = sstring("");
          (res_it+1)->original_text = sstring("");

          res_help_it->converted_text += (res_it+1)->converted_text;
          res_it->converted_text = sstring("");
          (res_it+1)->converted_text = sstring("");

          if(analyse){
            res_help_it->converted_candidates.splice(
              res_help_it->converted_candidates.end(),
              word_candidates);
          }
        }
      }

      res_help_it = res_it;
    }

    //main loop for conversion 
#ifdef ALLGREEK_EXE  
  #ifdef _WIN32
    int prg_range2 = res.size(), prg_pos2 = 0;
    ProgressForm->setRange(2,prg_range2);
  #endif //_WIN32
#endif //ALLGREEK_EXE
    for(res_it=res.begin();res_it!=res.end();res_it++)
    {
#ifdef ALLGREEK_EXE
  #ifdef _WIN32
    if(ProgressForm->bCanShow){
      prg_pos2++;
      if(((float)prg_pos2)/prg_range2 > 0.01)
        ProgressForm->incrPos(1);
      Application->ProcessMessages();
      if(ProgressForm->mustStop()) return;
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE
      if(res_it->done) continue;

      word_tmp = res_it->converted_text;

      //!!!!!!!!!!!!!next_word2convert!!!!!!!!!!!!!!!!!!!!!!
      const sstring *exc = customDicExceptions->find(word_tmp);
      if(exc!=NULL){
        res_it->converted_text = *exc;
        res_it->original_found = false;
        res_it->converted_found = true;

#ifdef WRITE_DETAILED_LOG
        fstream fdetailedlog;
        fdetailedlog.open("fdetailedlog.txt",ios::out | ios::app);
        fdetailedlog<<endl<<res_it->original_text<<'\t'<<res_it->converted_text<<'\t';
        fdetailedlog.close();
#endif //WRITE_DETAILED_LOG

        if(!analyse) continue;
        res_it->converted_candidates.push_back(
          dic_word_prob(res_it->converted_text, 0.f));
      }   
      //!!!!!!!!!!!!!next_word2convert!!!!!!!!!!!!!!!!!!!!!!

      if(word_tmp.size()==0 || !rules->is_letter(word_tmp[0])) {

#ifdef WRITE_DETAILED_LOG
        fstream fdetailedlog;
        fdetailedlog.open("fdetailedlog.txt",ios::out | ios::app);
        fdetailedlog<<endl<<res_it->original_text<<'\t';
        fdetailedlog.close();
#endif //WRITE_DETAILED_LOG

        continue;
      }
      if(word_tmp.find_first_not_of(rules->get_nonalpha_letters())==sstring::npos)
		  {
        res_it->converted_text = sstring("");    

#ifdef WRITE_DETAILED_LOG
        fstream fdetailedlog;
        fdetailedlog.open("fdetailedlog.txt",ios::out | ios::app);
        fdetailedlog<<endl<<res_it->original_text<<'\t';
        fdetailedlog.close();
#endif //WRITE_DETAILED_LOG

			  continue;
		  }

      sstring::size_type stress_pos = word_tmp.find_first_of(rules->get_stress_chars());
      if(stress_pos!=sstring::npos) word_tmp = word_tmp.substr(0,stress_pos);

      bool customEng = customEngDic->find(word_tmp);
		  if(customEng) {
        if(!analyse) continue;
        res_it->converted_candidates.push_back(
          dic_word_prob(res_it->original_text, 0.f));
      }

      setWord(word_tmp, res_it->original_text);
      proccess();

      if(analyse){
        res_it->converted_candidates.splice(
          res_it->converted_candidates.end(),
          word_candidates);
      }
      if(customEng) continue;

		  sstring converted_word = getMostProbGreek();
      capitalise_word(converted_word, res_it->original_text);
      res_it->converted_text = converted_word;

      res_it->original_found = english_found;
      res_it->converted_found = foundGreek;
      res_it->original_text_prob = dic_eng_prob;
      res_it->converted_text_prob = max_gr_prob;
      res_it->converted_dic_prob = dic_gr_prob;
      res_it->converted_back_prob = back_gr_prob;
      res_it->converted_triph_prob = triph_gr_prob;
    }

    //stress after or before
    res_help_it = res.end();
    for(res_it=res.begin();res_it!=res.end();res_it++)
    {
      if(res_it->done) continue;
      int sz;
      if(res_it->original_text.size()>=1
         && rules->is_stress(res_it->original_text[0])
         && res_help_it!=res.end()
         //&& !rules->is_greek_word_stressed(res_help_it->converted_text)
        ){
        sz = res_help_it->converted_text.size();
        if( sz && rules->is_greek_vowel(res_help_it->converted_text[sz-1])){
          if(!rules->is_greek_word_stressed(res_help_it->converted_text))
            res_help_it->converted_text[sz-1] = rules->stress_greek_vowel(res_help_it->converted_text[sz-1]);
          //res_it->converted_text = sstring("");
          res_it->converted_text = res_it->converted_text.substr(1);

          res_help_it->original_text += res_it->original_text[0];
          //res_it->original_text = sstring("");
          res_it->original_text = res_it->original_text.substr(1);
        }
      }
      sz = res_it->original_text.size();
      if(sz>=1
         && rules->is_stress(res_it->original_text[sz-1])
         && (res_it+1)!=res.end()
         //&& !rules->is_greek_word_stressed((res_it+1)->converted_text)
        ){
        if(rules->is_greek_vowel((res_it+1)->converted_text[0])){
          if(!rules->is_greek_word_stressed((res_it+1)->converted_text))
            (res_it+1)->converted_text[0] = rules->stress_greek_vowel((res_it+1)->converted_text[0]);
          (res_it+1)->original_text = sstring("") + res_it->original_text[sz-1] + (res_it+1)->original_text;

          //res_it->converted_text[sz-1] = '\0';
          //res_it->original_text[sz-1] = '\0';
          res_it->converted_text.erase(sz-1);
          res_it->original_text.erase(res_it->original_text.size()-1);
          res_it++;
          res_help_it = res_it;
          continue;
        }
      }

      res_help_it = res_it;
    }
  }
  catch(...)
  {
    res.push_back(out_fragment(sstring("\n|!")));
  }
}

////////////////////////////////////////////////////////////////////////////////
bool Greeklish2Greek::next_word2convert(sstring &text, sstring &text_r, int pos_beg, int &pos_word_start, int &pos_word_end)
{
  if(pos_beg>text.size()) return false;
  //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!! 
  //eat all beginning whites
  while(text[pos_beg]!='\0' && rules->is_white(text[pos_beg])) pos_beg++;
  if(text[pos_beg]=='\0') return false;

  //{
  if(text[pos_beg]=='{' || text[pos_beg]=='}'){
    pos_word_start = pos_beg;
    pos_word_end = pos_beg+1;
    return true;
  }

  //find next white
  int pos_white = pos_beg+1;
  while(text[pos_white]!='\0' && !rules->is_white(text[pos_white])) pos_white++;
  if(customDicExceptions->find(text.substr(pos_beg,pos_white - pos_beg))!=NULL){
    pos_word_start = pos_beg;
    pos_word_end = pos_white;
    return true;
  }

  #ifdef _ENABLE_CHEATS_
  sstring tmpstr = text.substr(pos_beg,pos_white - pos_beg);
  if(matches_cheat(tmpstr)){
    pos_word_start = pos_beg;
    pos_word_end = pos_white;
    return true;
  }
  #endif //_ENABLE_CHEATS_

  //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!!

  //eat all beginning not alpha letters
  while(text[pos_beg]!='\0' && !rules->is_letter(text[pos_beg]))
  {
    //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!!
    if(rules->is_white(text[pos_beg])){
      while(text[pos_beg]!='\0' && rules->is_white(text[pos_beg])) pos_beg++;
      if(text[pos_beg]=='\0') return false;
      pos_white = pos_beg+1;
      while(text[pos_white]!='\0' && !rules->is_white(text[pos_white])) pos_white++;
      if(customDicExceptions->find(text.substr(pos_beg,pos_white - pos_beg))!=NULL){
        pos_word_start = pos_beg;
        pos_word_end = pos_white;
        return true;
      }
      continue;
    }
    //!!!!!!!!!!!!!exceptions!!!!!!!!!!!!!!!!!!!!!!
    pos_beg++;
  }
  if(text[pos_beg]=='\0') return false;

  //if last eaten is digit eat also all digits
  if(pos_beg>0 && rules->is_digit(text[pos_beg-1]))
  {
    while(text[pos_beg]!='\0' && !rules->is_alpha_letter(text[pos_beg])) pos_beg++;
    if(text[pos_beg]=='\0') return false;
  } 

  //find next white
  pos_white = pos_beg+1;
  while(text[pos_white]!='\0' && !rules->is_white(text[pos_white])) pos_white++;

  //eat all trailing not alpha letters
  int pos_end = pos_white-1;
  while(pos_end>pos_beg && !rules->is_letter(text[pos_end])) pos_end--;

  //if last eaten is digit eat also all digits
  if(rules->is_digit(text[pos_end+1]))
  {
    while(pos_end>pos_beg && !rules->is_alpha_letter(text[pos_end])) pos_end--;
  }

  int pos, pos_last = pos_end;

  //"somenone's" case keep original
  //if(text[pos_end]=='s' && (text[pos_end-1]=='\'' && text[pos_end-1]=='\�'))
  if(text[pos_end]=='s' && (pos_end>0 && text[pos_end-1]=='\'' && (unsigned char)text[pos_end-1]==146))
    return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);

  sstring text_frag = text.substr(pos_beg,pos_last-pos_beg+1);
  //url kai parakatw
  if(text_frag.find("://")!=sstring::npos ||
     text_frag.find("www.")!=sstring::npos)
    return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);
  //email address kai parakatw   
  if((pos=text_frag.find('@'))!=(int)sstring::npos &&
     text_frag.find('.',pos)!=sstring::npos)
    return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);

  //check for kati-kati
  for(pos=pos_end-1;pos>pos_beg;pos--)
  {
    if(rules->is_letter(text[pos])) continue;
    pos_last = pos-1;  

    if(!rules->is_payla(text[pos]) && !rules->is_stress(text[pos]))
    {
      //!
      /*
      if(!(text[pos]=='.' && (pos_end-pos)>3)
        && rules->is_letter(text[pos+1]) && rules->is_letter(text[pos-1]))
      {
        return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);
      }
      */
      bool b1 = text[pos]=='.',
           b2 = rules->is_extension_domain(text.substr(pos+1,pos_end - pos)),
           b3 = rules->is_letter(text[pos+1]),
           b4 = pos>0&&rules->is_letter(text[pos-1]),
           b5 = text[pos+1]==text_r[pos+1]; //kefalaio meta apo teleia
      if(b1 && b2 && b3 && b4 && b5)
      {
        return next_word2convert(text,text_r,pos_white,pos_word_start,pos_word_end);
      }
    }
  }

  pos_word_start = pos_beg;
  pos_word_end = pos_last + 1;
  return true;
}

#define GR_TO_TOT_LOW 0.35
#define TOT_MACRO_RULE_MIN 5
sstring Greeklish2Greek::convertTextCustom(sstring text_r)
{
  vector<out_fragment> res;
  vector<out_fragment>::iterator res_it, res_help_it;
  res.clear();
  convertText2Vec(text_r, res);
  sstring ret = "", word = "";

  //sentence macro rule
  res_help_it = res.begin();
  float en_word_count = 0, gr_word_count = 0, tot_word_count;
  for(res_it=res.begin();res_it!=res.end();res_it++)
  {                  
#ifdef ALLGREEK_EXE  
  #ifdef _WIN32
    if(ProgressForm->bCanShow){
      Application->ProcessMessages();
      if(ProgressForm->mustStop()) break;
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE
    //if is sentence break

    if(rules->is_sentence_end(res_it->original_text))
    {
      tot_word_count = en_word_count+gr_word_count;
      if(tot_word_count>TOT_MACRO_RULE_MIN && gr_word_count/tot_word_count<GR_TO_TOT_LOW){
        while(res_help_it!=res_it)
        {
          res_help_it->original_found = true;
          res_help_it->converted_found = false;
          res_help_it++;
        }
      }

      gr_word_count = en_word_count = 0;
      res_help_it = res_it;
    }

    if(res_it->original_found && res_it->converted_found)
    {
      word = res_it->getProccessed();
      sstring lw = sstring("") + word[0];
      lower_string(&lw);
      lower_destress_greek(&lw);
      if(rules->is_alpha_letter(lw[0])) en_word_count++;
      else if(rules->is_greek_letter(lw[0])) gr_word_count++;
    }
    else if(res_it->original_found) en_word_count++;
    else if(res_it->converted_found) gr_word_count++;
    else {    
      word = res_it->getProccessed(); 
      sstring lw = sstring("") + word[0];
      lower_string(&lw);
      lower_destress_greek(&lw);
      if(word.size()>1 && lw[0] == '"' && rules->is_greek_letter(lw[0]))
         gr_word_count++;
    }
  }

  tot_word_count = en_word_count+gr_word_count;
  if(tot_word_count>TOT_MACRO_RULE_MIN && gr_word_count/tot_word_count<GR_TO_TOT_LOW){
    while(res_help_it!=res.end())
    {
      res_help_it->original_found = true;
      res_help_it->converted_found = false;
      res_help_it++;
    }
  } 

#ifdef ALLGREEK_EXE    
  #ifdef _WIN32
    int prg_range3 = res.size(), prg_pos3 = 0;
    ProgressForm->setRange(3,prg_range3);
  #endif //_WIN32
#endif //ALLGREEK_EXE

  for(res_it=res.begin();res_it!=res.end();res_it++)
  {         
#ifdef ALLGREEK_EXE   
  #ifdef _WIN32
    if(ProgressForm->bCanShow){
      prg_pos3++;
      if(((float)prg_pos3)/prg_range3 > 0.01)
        ProgressForm->incrPos(1);
      Application->ProcessMessages();
      if(ProgressForm->mustStop()) break;
    }
  #endif //_WIN32
#endif //ALLGREEK_EXE
    word = res_it->getProccessed();
    if(ret.size()>0 && ret[ret.size()-1] == '\"' && word[0] == '\"')
      ret += word.substr(1);
    else
      ret += word;
  }
   
#ifdef ALLGREEK_EXE  
  #ifdef _WIN32
  ProgressForm->Hide();
  #endif //_WIN32
#endif //ALLGREEK_EXE

#ifdef ALLGREEK_CHECK_LICENSE
  MASK_TEXT_WITH_LIC_DIFF(ret);
#else
  MASK_TEXT_WITH_VOLDIFF(ret);
#endif

  return ret;
}

////////////////////////////////////////////////////////////////////////////////
void Greeklish2Greek::convertAnalyse(sstring text_r, sstring &analysis)
{
  vector<out_fragment> res;
  vector<out_fragment>::iterator res_it, res_help_it;
  sstring word = "";

  analysis.clear();
  res.clear();

  analyse = true;
  convertText2Vec(text_r, res);

  //sentence macro rule
  res_help_it = res.begin();
  float en_word_count = 0, gr_word_count = 0, tot_word_count;
  for(res_it=res.begin();res_it!=res.end();res_it++)
  {
    //if is sentence break
    if(rules->is_sentence_end(res_it->original_text))
    {
      tot_word_count = en_word_count+gr_word_count;
      if(tot_word_count>TOT_MACRO_RULE_MIN && gr_word_count/tot_word_count<GR_TO_TOT_LOW){
        while(res_help_it!=res_it)
        {
          res_help_it->original_found = true;
          res_help_it->converted_found = false;
          res_help_it++;
        }
      }

      gr_word_count = en_word_count = 0;
      res_help_it = res_it;
    }

    if(res_it->original_found && res_it->converted_found)
    {
      word = res_it->getProccessed();
      sstring lw = sstring("") + word[0];
      lower_string(&lw);
      lower_destress_greek(&lw);
      if(rules->is_alpha_letter(lw[0])) en_word_count++;
      else if(rules->is_greek_letter(lw[0])) gr_word_count++;
    }
    else if(res_it->original_found) en_word_count++;
    else if(res_it->converted_found) gr_word_count++;
    else {
      word = res_it->getProccessed();
      sstring lw = sstring("") + word[0];
      lower_string(&lw);
      lower_destress_greek(&lw);
      if(word.size()>1 && lw[0] == '"' && rules->is_greek_letter(lw[0]))
         gr_word_count++;
    }
  }

  tot_word_count = en_word_count+gr_word_count;
  if(tot_word_count>TOT_MACRO_RULE_MIN && gr_word_count/tot_word_count<GR_TO_TOT_LOW){
    while(res_help_it!=res.end())
    {
      res_help_it->original_found = true;
      res_help_it->converted_found = false;
      res_help_it++;
    }
  }
        
  int analysis_length = 100; //header   
  for(res_it=res.begin();res_it!=res.end();res_it++){
    analysis_length += 50; //frg, r, c
    analysis_length += 2*max(res_it->original_text.size(),res_it->converted_text.size());
    if(res_it->converted_candidates.size()>0){
      analysis_length +=13; //alts
      for(list<dic_word_prob>::iterator it=res_it->converted_candidates.begin();
        it!=res_it->converted_candidates.end();it++)
      {
        analysis_length +=35; //alt, w, p, buf
        analysis_length += it->word.size();
      }
    }
  }
  analysis.reserve(analysis_length);

  analysis += "<?xml version=\"1.0\" encoding=\"WINDOWS-1253\" ?>";
  analysis += "<g2g>";
  for(res_it=res.begin();res_it!=res.end();res_it++)
  {
    analysis += "<frg>";

    analysis += "<r><![CDATA[";
    analysis += res_it->original_text;
    analysis += "]]></r>";

    analysis += "<c><![CDATA[";
    analysis += res_it->getProccessed();
    analysis += "]]></c>";

    if(res_it->converted_candidates.size()>0){
      res_it->converted_candidates.sort();
      analysis += "<alts>";
      for(list<dic_word_prob>::reverse_iterator it=res_it->converted_candidates.rbegin();
        it!=res_it->converted_candidates.rend();it++)
      {
        analysis += "<alt>";
        analysis += "<w>";
        analysis += it->word;
        analysis += "</w>";

        char buf[10];
        sprintf(buf, "%.3f", it->prob);
        analysis += "<p>";
        analysis += buf;
        analysis += "</p>";
        analysis += "</alt>";
      }
      analysis += "</alts>";
    }

    analysis += "</frg>";
  }
  analysis += "</g2g>";

#ifdef ALLGREEK_CHECK_LICENSE
  MASK_TEXT_WITH_LIC_DIFF(analysis);
#else
  MASK_TEXT_WITH_VOLDIFF(analysis);
#endif

  //return analysis;
}

} //ILSP_GREEKLISH
