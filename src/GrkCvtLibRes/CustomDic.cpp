#include "CustomDic.h"
#include "Globals.h"

namespace ILSP_GREEKLISH{

// obj ctor
CLetter2Sound::CLetter2Sound() {
}

// obj dtor
CLetter2Sound::~CLetter2Sound() {
	/*input.~string();
	buffer.~string();
	pcoutput.~string(); 
	ipunct.~vector<int>();
	IWs.~vector<int>();
	IWe.~vector<int>();*/
}

/////////////////////////////////////////////////////////////////////////
// MEMBER FUNCTIONS IMPLEMENTATION:
/////////////////////////////////////////////////////////////////////////

// ������� �� pcinput ��� �� ���������� ���� �������� ������ �� �� ���������� �����.
void  CLetter2Sound::ConvCap(std::string &pcinput) {

	///////////////////////////////////////////////////
	// First Parsing to convert capitalized letters  //
	///////////////////////////////////////////////////

	// � ��������� replace ����� generic algorithm ��� �� Standard Template Library
	// (STL) ��� ��� ����������� ���� ��� STL string.
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');
	replace(pcinput.begin(), pcinput.end(), '�', '�');

	return;
}


// ������ ���� ������ ��� �� ��������.
void CLetter2Sound::ConvPhon1(string pcinput, string &pcoutput, 
							  list_int &ipunct) {

	// ������ �������� �� ������� ��� pcoutput ��� ���� ��� ������
	// �� ����� �������� resize ��� ������� ��� pcinput. ���� ��� 
	// ������ ������ pcoutput.size() <= 2*pcinput.size()
	//
	// �� ��� ����� � ��������� pcoutput resize ��� ����������� ��
	// �� ������� pcoutput[4]='f' �� ��������� �� crasha��� ����� 
	// �� �������� �� ����� ��� ��� ���� ����� allocated => crash!
	pcoutput.resize(2*pcinput.size());
	// �� ���� ������ ��� ��� ��� container vector<int>:
	ipunct.resize(2*pcinput.size());

	int i, j, iNumOfChars;

	// ���� �������������� string ������� ��� member function size()
	// ���� �� ������� loop ���� �� ������������ �� �������.
	iNumOfChars=pcinput.size();
	
	///////////////////////////////////////////////////
	// Second Parsing to find dipthings and stresses //
	///////////////////////////////////////////////////
	i=0; j=-1;
	while (i<iNumOfChars) {		

		if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {			// check for ��
			j++; i++;
			pcoutput[j]='e';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ei
			j++; i++;
			pcoutput[j]='e';
			ipunct[j]=1;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='i';
			ipunct[j]=1;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='i';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='i';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='i';
			ipunct[j]=1;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='u';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='u';
			ipunct[j]=1;
		}
		else if (((pcinput[i]=='�') && (pcinput[i+1]=='�')) || 
			     ((pcinput[i]=='�') && (pcinput[i+1]=='�'))) {	// check for �� ��
			j++; i++;
			pcoutput[j]='g';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;											// ����������� �����
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for �� 
			j++; i++;											// ����������� �����
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++;
			i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='d';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ��
			j++; i++;
			pcoutput[j]='�';
			ipunct[j]=0;
		}
/*		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for ts
			j++; i++;
			pcoutput[j]='S';
			ipunct[j]=0;

		}
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�')) {	// check for tz
			j++; i++;
			pcoutput[j]='Z';
			ipunct[j]=0;
		}	*/
		// A������ �� ������� ��� pcinput[i+1]!='�' ??
		else if ((pcinput[i]=='�') && (pcinput[i+1]=='�') 
								   && (pcinput[i+2]!='�')) {	// check for �� 
			j++; i++;											// (��� bt)
			pcoutput[j]='b';
			ipunct[j]=0;
		}
		else if (pcinput[i]=='�') {								// check for �
			j++; i++;
			pcoutput[j]='k';
			ipunct[j]=0;
			j++; 
			pcoutput[j]='s';
			ipunct[j]=0;
			continue;	// ��� ������!! 14/3/2002
		}
		else if (pcinput[i]=='�') {								// check for �
			j++; i++;
			pcoutput[j]='p';
			ipunct[j]=0;
			j++; 
			pcoutput[j]='s';
			ipunct[j]=0;
			continue;	// ��� ������!! 14/3/2002
		}
		else if (pcinput[i]=='�') {								// check for �
			j++; i++;
			pcoutput[j]='a';
			ipunct[j]=1;
			continue;
		}
		else if (pcinput[i]=='�') {								// check for �
			j++; i++;
			pcoutput[j]='e';
			ipunct[j]=1;
			continue;
		}
		else if ((pcinput[i]=='�') || (pcinput[i]=='�')) {		// check for �, �
			j++; i++;
			pcoutput[j]='o';
			ipunct[j]=1;
			continue;
		}
		else if ((pcinput[i]=='�')|| (pcinput[i]=='�')) {		// check for �,�,�
			j++; i++;
			pcoutput[j]='i';
			ipunct[j]=1;
			continue;
		}
		else if ((pcinput[i]=='�') || (pcinput[i]=='�')) {		// check for �,�
			j++; i++;
			pcoutput[j]='i';
			ipunct[j]=0;
			continue;
		}
		else {
			j++;
			pcoutput[j]=pcinput[i];
			ipunct[j]=0;
		}
		i++;
	}

	return;
}

// ���������� �� ��������� ����������: �������� �������� ���������.
void CLetter2Sound::Conv2Eng(string &pcinput) {

	replace((pcinput).begin(), (pcinput).end(), '�', 'a');
	replace((pcinput).begin(), (pcinput).end(), '�', 'v');
	replace((pcinput).begin(), (pcinput).end(), '�', 'J');
	replace((pcinput).begin(), (pcinput).end(), '�', 'D');
	replace((pcinput).begin(), (pcinput).end(), '�', 'e');
	replace((pcinput).begin(), (pcinput).end(), '�', 'z');
	replace((pcinput).begin(), (pcinput).end(), '�', 'T');
	replace((pcinput).begin(), (pcinput).end(), '�', 'i');
	replace((pcinput).begin(), (pcinput).end(), '�', 'l');
	replace((pcinput).begin(), (pcinput).end(), '�', 'k');
	replace((pcinput).begin(), (pcinput).end(), '�', 'm');
	replace((pcinput).begin(), (pcinput).end(), '�', 'n');
	replace((pcinput).begin(), (pcinput).end(), '�', 'o');
	replace((pcinput).begin(), (pcinput).end(), '�', 'p');
	replace((pcinput).begin(), (pcinput).end(), '�', 'r');
	replace((pcinput).begin(), (pcinput).end(), '�', 's');
	replace((pcinput).begin(), (pcinput).end(), '�', 's');
	replace((pcinput).begin(), (pcinput).end(), '�', 't');
	replace((pcinput).begin(), (pcinput).end(), '�', 'f');
	replace((pcinput).begin(), (pcinput).end(), '�', 'x');
	replace((pcinput).begin(), (pcinput).end(), '�', 'o');
	return;
}

// ��������� �� ������ ���������� ���������. 
void CLetter2Sound::ConvPhon2(string pcinput, string &pcoutput,
							  list_int &ipunct) {

    list_int itemppunct;
	pcoutput.resize(pcinput.size());
	// �� ���� ������ ��� ��� ��� container vector<int>:
	ipunct.resize(pcinput.size());
	itemppunct.resize(pcinput.size());

	int i, j, iNumOfChars;

	// ���� �������������� string ������� ��� member function size()
	// ���� �� ������� loop ���� �� ������������ �� �������.
	iNumOfChars=pcinput.size();

	i=0; j=0;
	while (i<iNumOfChars) {	
		if ((pcinput[i]=='J') && (pcinput[i+1]=='i') && (ipunct[i+1]==0) &&
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || (pcinput[i+2]=='u') ||
			(pcinput[i+2]=='o'))) {							// check for ��
			pcoutput[j]='j';
			itemppunct[j]=ipunct[i];
			j++; i++; i++;
			continue;
		}
		if ((pcinput[i]=='J') && (pcinput[i+1]=='i') && (ipunct[i+1]==1) &&
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || (pcinput[i+2]=='u') ||
			(pcinput[i+2]=='o'))) {							// check for ��
			pcoutput[j]='j';
			itemppunct[j]=ipunct[i];
			j++; i++; 
			continue;
		}
		if ((pcinput[i]=='J') && (pcinput[i+1]=='i')) {		// check for ��
			pcoutput[j]='j';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='J') && (pcinput[i+1]=='e')) {		// check for �e
			pcoutput[j]='j';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='J') && (pcinput[i+1]=='x')) {		// check for a�xos
			pcoutput[j]='h';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		// Den xerw an xreiazomaste afto to phoneme!!!!!!!!!!!!!
		if ((pcinput[i]=='n') && (pcinput[i+1]=='g')) {		// check for ng
			pcoutput[j]='h';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='m') && ((pcinput[i+1]=='v') || 
			(pcinput[i+1]=='f'))) {							// check for ng
			pcoutput[j]='M';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='D') && (pcinput[i+1]=='i') && 
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || 
			(pcinput[i+2]=='o')) && (i>0&&ipunct[i-1]==1) && (i>1&&pcinput[i-2]!='i')) {							// check for Di
			pcoutput[j]='D';
			itemppunct[j]=ipunct[i];
			j++; i++;
			pcoutput[j]='j';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}		
		
		if ((pcinput[i]=='D') && (pcinput[i+1]=='i') && 
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || 
			(pcinput[i+2]=='o')) && (ipunct[i-2]==0)) {							// check for Di
			pcoutput[j]='D';
			itemppunct[j]=ipunct[i];
			j++; i++;
			pcoutput[j]='i';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}		
		if ((pcinput[i]=='i') && (i>0&&pcinput[i-1]=='p') &&
			 ((pcinput[i+1]=='a') || (pcinput[i+1]=='e') || 
			(pcinput[i+1]=='o') || (pcinput[i+1]=='u')) && ((ipunct[i]==0)  )  ) {							// check for ng
			pcoutput[j]='X';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='i') && (i>0&&pcinput[i-1]=='p') &&
			 ((pcinput[i+1]=='a') || (pcinput[i+1]=='e') || 
			(pcinput[i+1]=='o')) && (ipunct[i]==1)  ) {							// check for ng
			pcoutput[j]='i';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='i') && ((pcinput[i+1]=='a')  || 
			                      (pcinput[i+1]=='e') || (pcinput[i+1]=='o')) && 
								  (ipunct[i]==0) && (i>0&&pcinput[i-1]=='s')) {			// check for �� ->��
			pcoutput[j]='I';
			itemppunct[j]=ipunct[i];
			j++; i++;
			pcoutput[j]=pcinput[i];
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='i') && ((pcinput[i+1]=='a')  || 
			                      (pcinput[i+1]=='e') || (pcinput[i+1]=='o')) && 
								  (ipunct[i]==0) && (i>0&&pcinput[i-1]=='s')  
								  && (ipunct[i+1]==0)) {			// check for �� ->��
			pcoutput[j]='i';
			itemppunct[j]=ipunct[i];
			j++; i++;
			pcoutput[j]=pcinput[i];
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
	/*	if ((pcinput[i]=='i') && ((pcinput[i+1]=='a')  || 
			                      (pcinput[i+1]=='e') || (pcinput[i+1]=='o')) && 
								  (ipunct[i]==0) && (i>0&&pcinput[i-1]!='r') && (i>0&&pcinput[i-1]!='p')) {			// check for �� ->��
			pcoutput[j]='I';
			itemppunct[j]=ipunct[i];
			j++; i++;
			pcoutput[j]=pcinput[i];
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}	*/
		if (((pcinput[i]=='k') && (pcinput[i+1]=='i')) ||
			((pcinput[i]=='k') && (pcinput[i+1]=='e')) ||
			((pcinput[i]=='k') && (pcinput[i+1]=='�')) ||
			((pcinput[i]=='k') && (pcinput[i+1]=='�'))) {	// check for c� ce
			pcoutput[j]='c';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if (((pcinput[i]=='g') && ((pcinput[i+1]=='i') || (pcinput[i+1]=='�') || (pcinput[i+1]=='�') ||
			(pcinput[i+1]=='e')))) {						// check for g�
			pcoutput[j]='G';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if (((pcinput[i]=='l') && (pcinput[i+1]=='i')) && (ipunct[i+1]==0) && (ipunct[i+2]==1) &&
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || (pcinput[i+2]=='u') ||
			 (pcinput[i+2]=='o')) && (i>0&&pcinput[i-1]!='i')) {						// check for ��
			pcoutput[j]='L';
			itemppunct[j]=ipunct[i];
			j++; i++; i++;
			continue;
		}
		if (((pcinput[i]=='l') && (pcinput[i+1]=='i')) && (ipunct[i+1]==0) && (i>0&&ipunct[i-1]==1) &&
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || (pcinput[i+2]=='u') ||
			 (pcinput[i+2]=='o')) && (pcinput[i-2]!='i')) {						// check for ��
			pcoutput[j]='L';
			itemppunct[j]=ipunct[i];
			j++; i++; i++;
			continue;
		}
		if (((pcinput[i]=='n') && (pcinput[i+1]=='i') && (ipunct[i+1]==0)) && (ipunct[i+2]==1) &&
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || 
			(pcinput[i+2]=='o')) &&  (pcinput[i-2]!='i')) {							// check for nia
			pcoutput[j]='N';
			itemppunct[j]=ipunct[i];
			j++; i++; i++;
			//continue;
		}   
		if (((pcinput[i]=='n') && (pcinput[i+1]=='i') && (ipunct[i+1]==0))  &&
			(i>0&&pcinput[i-1]=='o') && (i>0&&ipunct[i-1]==1) &&
			((pcinput[i+2]=='a') || (pcinput[i+2]=='e') || 
			(pcinput[i+2]=='o')) && (i>1&&pcinput[i-2]!='i')) {							// check for nia
			pcoutput[j]='N';
			itemppunct[j]=ipunct[i];
			j++; i++; i++;
			//continue;
		}
		if ((pcinput[i]=='�') && (((i>0&&pcinput[i-1]=='a') && (i>0&&ipunct[i-1]==0)) || 
			((i>0&&pcinput[i-1]=='e') && (i>0&&ipunct[i-1]==0))) && ((pcinput[i+1]=='a') || 
			(pcinput[i+1]=='e') || (pcinput[i+1]=='o')|| (pcinput[i+1]=='i') ||  (pcinput[i+1]=='u') ||
			(pcinput[i+1]=='J') || (pcinput[i+1]=='r') || (pcinput[i+1]=='n') || 
			(pcinput[i+1]=='D') || (pcinput[i+1]=='m') || (pcinput[i+1]=='l') || 
			(pcinput[i+1]=='z'))) {							// check for -�-voiced
			pcoutput[j]='v';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='�') && (((i>0&&pcinput[i-1]=='a') && (i>0&&ipunct[i-1]==0)) || 
			((i>0&&pcinput[i-1]=='e') && (i>0&&ipunct[i-1]==0))) && ((pcinput[i+1]=='a') || 
			(pcinput[i+1]=='e') || (pcinput[i+1]=='o')|| (pcinput[i+1]=='i') || (pcinput[i+1]=='u') ||
			(pcinput[i+1]=='J') || (pcinput[i+1]=='r') || (pcinput[i+1]=='n') || 
			(pcinput[i+1]=='D') || (pcinput[i+1]=='m') || (pcinput[i+1]=='l') || 
			(pcinput[i+1]=='z'))) {							// check for -�-voiced
			pcoutput[j]='v';
			j--;
			itemppunct[j]=1;
			j++; i++; j++;
			continue;
		} 
		if ((pcinput[i]=='s') && (pcinput[i+1]=='m')) 
		{      // check for sm->zm
			pcoutput[j]='z';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='�')&&(((i>0&&pcinput[i-1]=='a')&&(i>0&&ipunct[i-1]==0))||(i>0&&pcinput[i-1]=='e'))) {	// check for -�-unvoiced
			pcoutput[j]='f';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='�')&&(((i>0&&pcinput[i-1]=='a')&&(i>0&&ipunct[i-1]==0))||(i>0&&pcinput[i-1]=='e'))) {	// check for -�-unvoiced
			pcoutput[j]='f';
			j--;
			itemppunct[j]=1;
			j++; i++; j++;
			continue;
		}
		if (pcinput[i]=='�') {								// check for �->i
			pcoutput[j]='i';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='�') || (pcinput[i]=='�')) {								// check for �->i
			pcoutput[j]='i';
			itemppunct[j]=0;
			j++; i++;
			continue;
		}
		if ((pcinput[i]=='�') || (pcinput[i]=='�')) {								// check for �->i
			pcoutput[j]='i';
			itemppunct[j]=1;
			j++; i++;
			continue;
		}
		if (pcinput[i]=='�') {								// check for �->i
			pcoutput[j]='i';
			itemppunct[j]=1;
			j++; i++;
			continue;
		}
		if (((pcinput[i]=='x') && ((pcinput[i+1]=='i') || 
			 (pcinput[i+1]=='e') || (pcinput[i+1]=='�') ||
			 (pcinput[i+1]=='�')))) {						// check for ��
			pcoutput[j]='X';
			itemppunct[j]=ipunct[i];
			j++; i++;
			continue;
		}
		else {
			pcoutput[j]=pcinput[i];
			itemppunct[j]=ipunct[i];
			j++; i++;
		}
	}

	ipunct=itemppunct;

	return;
}

// �� ��� ���������� ��������� ��� �� �������� ��� �������� ������������
// �� �������� ��� ������ �����������: Intonation Words: IWs, IWe
void CLetter2Sound::FindIW(string pcinput, list_int ipunct,
						   list_int &IWs, list_int &IWe) {

	IWs.resize(ipunct.size());
	IWe.resize(ipunct.size());

	sstring::size_type i;

	// ������ ������ ����� ��
	int nrOfAccents=0;
	for (i=0;i<pcinput.size();i++) {
		if (ipunct[i]) {
			nrOfAccents++;
		}
		if(i) if (((pcinput[i]==' ')||(pcinput[i]==',')||(pcinput[i]=='.')||
			 (pcinput[i]=='!')||(pcinput[i]==';'))&&(nrOfAccents==1)) {
			IWe[i-1]=1;
			IWe[i]=0;
			nrOfAccents=0;
			continue;
		}

		// ���� ��������� ��� ����� ������ ����������� ���� �����������.
		// ���� � ������� ��� ��������� �� ���������� ���� �����������
		// �� ��� ���� ����������� ���. ������� �� nrOfAccents �� �����
		// 1 ���� �� ������ �� �������� if ���� ������� ����.
		if (((pcinput[i]==' ')||(pcinput[i]==','))&&(nrOfAccents==2)) {
			IWe[i]=0;
			nrOfAccents=1;
			continue;
		}
		// ��������� ��� ������� �������� ��� ��� �����. ������ �����
		// ��� ������� ���� �������� ������ ���� ���������� ��� ����
		// ��� ��������� ������������� ��� ��.
		if(i) if (((pcinput[i]==' ')||(pcinput[i]==',')||(pcinput[i]=='.')||
			 (pcinput[i]=='!')||(pcinput[i]==';'))&&(nrOfAccents>2)) {
			IWe[i-1]=1;
			IWe[i]=0;
			nrOfAccents=0;
			continue;
		}
		IWe[i]=0;
	}

	// �� ���� �� �������� ��� ������� ������� �� ��������� �� ������
	// ����� ���� ��!
	bool space=false;
	for (i=0;i<pcinput.size();i++) {
		if (IWe[i]) {
			space=true;
			IWs[i]=0;
			continue;
		}
		if (space&&(pcinput[i]!=' ')&&(pcinput[i]!='.')&&(pcinput[i]!='!')&&
			       (pcinput[i]!=',')&&(pcinput[i]!=';')) {
			space=false;
			IWs[i]=1;
			continue;
		}
		IWs[i]=0;
	}
	IWs[0]=1;	// 1� ��

	return;
}

void CLetter2Sound::convert(const char *word, string &converted, bool stress)
{
  //������������ ��� ���������� input
  input = string(word);

  //������������ ������
  converted.erase();
  //!converted.reserve(input.size()+4);

	buffer=string("");
  pcoutput=string("");
  ipunct.clear();
  IWs.clear();
  IWe.clear();

	// ��������� ��� �� �������� �� ����� ��������
	ConvCap(input);
	// �������� ����� ��� ������ ��������� ��� �� input string.
	// O buffer ����� �� output string ��� �� ipunct �� ������� 
	// �� ���� ������.
	ConvPhon1(input, buffer, ipunct);
	// ����� ��������� ���������� (buffer) �� �������� (��������).
	Conv2Eng(buffer); 
	// ������ ��������� ��� buffer string �� �������� ��� pcoutput 
	// string. �� ipunct ����� �� �������� �� ���� ������.
	ConvPhon2(buffer, pcoutput, ipunct);
	// ������ ������ ����������� (Intonation Words) ��� �� string
	// ��������� pcoutput ��������������� �� �������� ��� �����.
	// �� ���������� ������ IWs ��� IWe ��������� ��� ����� ��� ��
	// ���������� ��� �� ��� ��������������.
	FindIW(pcoutput, ipunct, IWs, IWe);

	// �������: ��� ����� ������� ����� ����� ����� strlen(pcoutput.c_str())
	// ��� ��� pcoutput.size(): ���� ����� �� pcoutput.size() ����������
	// �������� ���������� ��� ��� ���������� �������� (����� ��� ������
	// ����� resize ��������.
	for (unsigned int i=0; i<strlen(pcoutput.c_str()); i++) {
		converted += pcoutput[i];
		if (stress && ipunct[i])
			converted += '\'';
	}
}

#define USE_STD_FSTREAM
////////////////////////////////////////////////////////////
// ctor �� ��� ������ ������� txt �� ��������� ������
CustomDic::CustomDic(const char *dicname)
{
  source_ok = true;

  dicfilename = sstring(dicname);    

  letter2sound = new CLetter2Sound();
  phonetic_greek.clear();

#ifdef USE_STD_FSTREAM
  fstream src;
  //������� ��� ������� �������
  src.open(dicname,ios::in);

  if(!src){
    source_ok = false;
    return;
  }
#else
  FILE *src;
  //������� ��� ������� �������
  src = fopen(dicname,"r");

  if(!src){
    source_ok = false;
    return;
  }
#endif

  sstring word;        // �������� ���� ��� �� ������
  sstring word_phon;   // ���������� ����

  // ���������� ����
  // �� ������������
  // ��� ��������� �� ������
  phonetic_greek_dic::iterator phonetic_greek_it; 
#ifdef USE_STD_FSTREAM
  std::string std_word;
  while(src>>std_word)
  {
    if(std_word.size()>0 && std_word[std_word.size()-1]=='\r')
      std_word.erase(std_word.size()-1);
    word = std_word.c_str();
#else
  char buffer[256];
  while(fscanf(src,"%s",buffer)!=EOF)
  {
    word = sstring(buffer);
#endif   
    std::string std_word_phon;
    letter2sound->convert(word.c_str(), std_word_phon, false);
    word_phon = std_word_phon.c_str();

    phonetic_greek_it = phonetic_greek.find(word_phon);
    if(phonetic_greek_it==phonetic_greek.end()){
      list<sstring> *temp = new list<sstring>();
      temp->clear();
      temp->push_back(word);
      phonetic_greek.insert(phonetic_greek_dic::value_type(word_phon,temp));
    }
    else
      phonetic_greek_it->second->push_back(word);
  }

#ifdef USE_STD_FSTREAM
  src.close();
#else
  fclose(src);
#endif
}

////////////////////////////////////////////////////////////
// dtor
CustomDic::~CustomDic()
{
  delete letter2sound;

  phonetic_greek_dic::iterator phonetic_greek_it;
  for(phonetic_greek_it = phonetic_greek.begin();
      phonetic_greek_it != phonetic_greek.end();
      phonetic_greek_it++){
    phonetic_greek_it->second->clear();
    delete phonetic_greek_it->second;
    phonetic_greek_it->second = NULL;
  }
}

////////////////////////////////////////////////////////////
void CustomDic::LoadSaveCustomDic(const char *dic)
{
  istrstream src(dic);

  if(!src){
    source_ok = false;
    return;
  }
  else
    source_ok = true;

  sstring word;        // �������� ���� ��� �� ������
  sstring word_phon;   // ��������� ����

  phonetic_greek.clear();

  // ���������� ����
  // �� ������������
  // ��� ��������� �� ������
  phonetic_greek_dic::iterator phonetic_greek_it;
  std::string std_word, std_word_phon;
  while(source_ok && src>>std_word)
  {
    word = std_word.c_str();
    letter2sound->convert(word.c_str(), std_word_phon, false);
    word_phon = std_word_phon.c_str();

    phonetic_greek_it = phonetic_greek.find(word_phon);
    if(phonetic_greek_it==phonetic_greek.end()){
      list<sstring> *temp = new list<sstring>();
      temp->clear();
      temp->push_back(word);
      phonetic_greek.insert(phonetic_greek_dic::value_type(word_phon,temp));
    }
    else
      phonetic_greek_it->second->push_back(word);
  }

#ifdef USE_STD_FSTREAM
  fstream dest;
  //������� ��� ������� ������
  dest.open(dicfilename.c_str(), ios::out);
  dest<<dic;
  dest.close();
#else
  FILE *dest;
  //������� ��� ������� ������
  dest = fopen(dicfilename.c_str(), "w");
  if(dest!=NULL){
    fprintf(dest,dic);
    fclose(dest);
  }
#endif
}

////////////////////////////////////////////////////////////
// ��������� ���� �����
// ���������� NULL �� ��� ������
const sstring * CustomDic::find(const sstring& word_phon, const sstring& word_grl)
{
  if(!source_ok) return NULL;

  phonetic_greek_dic::iterator it;

  it = phonetic_greek.find(word_phon);

  if(it != phonetic_greek.end())
  {
    list<sstring> *vec_ref = it->second;
    list<sstring>::iterator vec_ref_it, vec_ref_it_max;
    if(!vec_ref->size()) return NULL;
    int dummy;
    vec_ref_it = vec_ref->begin();
    vec_ref_it_max = vec_ref->begin();
    float tmp, max_back_prob = rules->match_greek_greeklish(*vec_ref_it_max, word_grl, dummy);
    for(;vec_ref_it!=vec_ref->end();vec_ref_it++){
      tmp = rules->match_greek_greeklish(*vec_ref_it, word_grl, dummy);
      if(max_back_prob < tmp){
        vec_ref_it_max = vec_ref_it;
        max_back_prob = tmp;
      }
    }

    return &*vec_ref_it_max;
  }

  return NULL;
}

////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// ctor �� ��� ������ ������� txt �� ������� ������
CustomEngDic::CustomEngDic(const char *dicname)
{
  sstring word;

  source_ok = true;

  eng_dic.clear();

  dicfilename = sstring(dicname);

#ifdef USE_STD_FSTREAM
  fstream src;
  //������� ��� ������� �������
  src.open(dicname,ios::in);

  if(!src){
    source_ok = false;
    return;
  }
#else
  FILE *src;
  //������� ��� ������� �������
  src = fopen(dicname,"r");

  if(!src){
    source_ok = false;
    return;
  }
#endif

  // ���������� ����
  // ��� ������� lower
  // ��� ��� ��������� ��� �����
#ifdef USE_STD_FSTREAM
  std::string std_word;
  while(src>>std_word)
  {
    if(std_word.size()>0 && std_word[std_word.size()-1]=='\r')
      std_word.erase(std_word.size()-1);
    word = std_word.c_str();
#else
  char buffer[256];
  while(fscanf(src,"%s",buffer)!=EOF)
  {
    word = sstring(buffer);
#endif
    lower_string(&word);
    eng_dic.push_back(word);
  }
  eng_dic.sort();

#ifdef USE_STD_FSTREAM
  src.close();
#else
  fclose(src);
#endif
}

////////////////////////////////////////////////////////////
void CustomEngDic::LoadSaveCustomDic(const char *dic)
{
  istrstream src(dic);

  if(!src){
    source_ok = false;
    return;
  }
  else
    source_ok = true;

  sstring word;

  eng_dic.clear();

  // ���������� ����
  // ��� ������� lower
  // ��� ��� ��������� ��� �����  
  string std_word;
  while(src>>std_word)
  {          
    word = std_word.c_str();
    lower_string(&word);
    eng_dic.push_back(word);
  }
  eng_dic.sort();

#ifdef USE_STD_FSTREAM
  fstream dest;
  //������� ��� ������� ������
  dest.open(dicfilename.c_str(), ios::out);
  dest<<dic;
  dest.close();
#else
  FILE *dest;
  //������� ��� ������� ������
  dest = fopen(dicfilename.c_str(), "w");
  if(dest!=NULL){
    fprintf(dest,dic);
    fclose(dest);
  }
#endif
}

////////////////////////////////////////////////////////////
// ��������� ���� �����
// ���������� true �� ������
bool CustomEngDic::find(const char *word)
{
  if(!source_ok) return false;

  sstring key(word);

  return find(key);
}
bool CustomEngDic::find(const sstring& w)
{
  if(!source_ok) return false;

  sstring word = w;
  lower_string(&word);

  return std::find(eng_dic.begin(),eng_dic.end(),word)!=eng_dic.end();
}

/*
void CustomEngDic::lower_string(sstring *s)
{
  for(sstring::size_type i = 0;i<s->size();i++)
	{
	  if(isalnum((*s)[i]))
		  (*s)[i] = tolower((*s)[i]);
  }
}
*/
////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////
//CustomDicExceptions
////////////////////////////////////////////////////////////
CustomDicExceptions::CustomDicExceptions(const char *dicname)
{
  sstring word1, word2;

  source_ok = true;

  exc_dic.clear();

  dicfilename = sstring(dicname);

#ifdef USE_STD_FSTREAM
  fstream src;
  //������� ��� ������� �������
  src.open(dicname,ios::in);

  if(!src){
    source_ok = false;
    return;
  }                   
#else
  FILE *src;
  //������� ��� ������� �������
  src = fopen(dicname,"r");

  if(!src){
    source_ok = false;
    return;
  }
#endif

  // ���������� ����
  // ��� ������� lower
  // ��� ��� ��������� ��� �����
#ifdef USE_STD_FSTREAM
  std::string std_word1, std_word2;
  char buffer2[256];
  while(src>>std_word1 && src.getline(buffer2,255))
  {
    word1 = std_word1.c_str();
    if(buffer2[0]=='\t')
      word2 = sstring(buffer2+1);
    else
      word2 = sstring(buffer2);
#else
  char buffer1[256], buffer2[256];
  while(fscanf(src,"%s",buffer)!=EOF && fgets(buffer2,255,src)!=EOF)
  {
    word1 = sstring(buffer1);
    if(buffer2[0]=='\t')
      word2 = sstring(buffer2+1);
    else
      word2 = sstring(buffer2);
#endif
    lower_string(&word1);
    //lower_string(&word2);
    if(word2.size()){
      char cend = word2[word2.size()-1];
      if(cend=='\r')
        word2.erase(word2.size()-1,1);
    }
    exc_dic.insert(map<sstring,sstring>::value_type(word1,word2));
  }

#ifdef USE_STD_FSTREAM
  src.close();
#else
  fclose(src);
#endif
}

// ������� ��� text ��� ����������
void CustomDicExceptions::LoadSaveCustomDic(const char *dic){
  istrstream src(dic);

  if(!src){
    source_ok = false;
    return;
  }
  else
    source_ok = true;

  sstring word1, word2;

  exc_dic.clear();

  std::string std_word1, std_word2;
  while(source_ok && src>>std_word1>>std_word2)
  {
    word1 = std_word1.c_str();
    word2 = std_word1.c_str();

    lower_string(&word1);
    //lower_string(&word2);
    exc_dic.insert(map<sstring,sstring>::value_type(word1,word2));
  }

#ifdef USE_STD_FSTREAM
  fstream dest;
  //������� ��� ������� ������
  dest.open(dicfilename.c_str(), ios::out);
  dest<<dic;
  dest.close();
#else
  FILE *dest;
  //������� ��� ������� ������
  dest = fopen(dicfilename.c_str(), "w");
  if(dest!=NULL){
    fprintf(dest,dic);
    fclose(dest);
  }
#endif
}

// ��������� ���� �����
// ���������� NULL �� ��� ������
const sstring* CustomDicExceptions::find(const char *word){
  sstring w(word);
  return find(w);
}
const sstring* CustomDicExceptions::find(const sstring& word){
  if(!source_ok) return NULL;

  map<sstring, sstring>::iterator it;

  it = exc_dic.find(word);

  if(it != exc_dic.end())
  {
    return &it->second;
  }

  return NULL;
}


} //ILSP_GREEKLISH
