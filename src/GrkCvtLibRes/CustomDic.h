#ifndef _CUSTOMDIC_H_
#define _CUSTOMDIC_H_

#include <map>      //dictionary

#include "sstring/sstring.h"
#include <string>		// strings, strlen

#include <vector>		// vector<int>     
#include <list>		  // english custom dictionary
#include <algorithm>	// generic algorithms: ����� replace(..)
#include <fstream>		//
using namespace std;

namespace ILSP_GREEKLISH{

typedef vector<int> list_int;

class CLetter2Sound  {

public:
	CLetter2Sound();

	// �� ��������� ��� ������� ������� �� dtor ��������������� ��� � ����� ���
	// ���� ��������������.
	~CLetter2Sound();

  //!������ 27-1-2004
  // ���������� ��� �������� ���� �� ����������
  void convert(const char *word, string &converted, bool stress = true);

private:
	// ������� �� pcinput ��� �� ���������� ���� �������� ������ �� �� ���������� �����.
	void ConvCap(string &pcinput);

	// ������ ���� ������ ��� �� ��������.
	void ConvPhon1(string pcinput, string &pcoutput,
		           list_int &ipunct);

	// ���������� �� ��������� ����������: �������� �������� ���������.
	void Conv2Eng(string &pcinput);

	// ��������� �� ������ ���������� ���������.
	void ConvPhon2(string pcinput, string &pcoutput,
		           list_int &ipunct);

	// �� ��� ���������� ��������� ��� �� �������� ��� �������� ������������
	// �� �������� ��� ������ �����������: Intonation Words: IWs, IWe
	void FindIW(string pcinput, list_int ipunct,
                list_int &IWs, list_int &IWe);

	// ��� input ��������� � �������, �� buffer ����� temporary ���������
	// ��� ��� pcoutput �������� � ������
	string input, buffer, pcoutput;

	// ����������� ���� ������� �������� ������ ��� Standard Template Container 
	// vector<int> = �������� ��� integers. ��� ����� ����� �� ��� ���������� �� 
	// ��� ���� ����� ���� ��� ������ �� �� ipunct[4]=1;
	//
	// �� ipunct �������� �� �������� �� ���� ������ ��� ���������� ���� 
	// ���������� ���������. �� ���������� IWs ��� IWs ��������� ��� ���� ���
	// �� �������� ���� ����� ����������� (Intonation Word start/end).
	list_int ipunct, IWs, IWe;
};

typedef map< sstring, list<sstring>* > phonetic_greek_dic;

class CustomDic
{
  // ������ �������� - ��������
  phonetic_greek_dic phonetic_greek;

  // ��� ��� ��������� ��� ����� ��� �������
  CLetter2Sound *letter2sound;

  // ������ ������� OK
  bool source_ok;

  // ����� �������
  sstring dicfilename;

public:
  // ctor �� ��� ������ ������� txt �� ��������� ������
  CustomDic(const char *dicname);
  ~CustomDic();

  // ������� ��� text ��� ����������
  void LoadSaveCustomDic(const char *dic);

  // ��������� ���� �����
  // ���������� NULL �� ��� ������
  const sstring * find(const sstring& word_phon, const sstring& word_grl);
};

class CustomEngDic
{
  // ������ ��� lower
  list<sstring> eng_dic;

  // ������ ������� OK
  bool source_ok;
  //void lower_string(sstring *s);  

  // ����� �������
  sstring dicfilename;

public:
  // ctor �� ��� ������ ������� txt �� �������� ������
  CustomEngDic(const char *dicname);

  // ������� ��� text ��� ����������
  void LoadSaveCustomDic(const char *dic);

  // ��������� ���� �����
  // ���������� NULL �� ��� ������
  bool find(const char *word);
  bool find(const sstring& word);
};

class CustomDicExceptions{
  //������ �� ����������
  map<sstring, sstring> exc_dic;  

  // ������ ������� OK
  bool source_ok;

  // ����� �������
  sstring dicfilename;   

public:
  // ctor �� ��� ������ ������� txt �� ����� ������
  CustomDicExceptions(const char *dicname);

  // ������� ��� text ��� ����������
  void LoadSaveCustomDic(const char *dic);

  // ��������� ���� �����
  // ���������� NULL �� ��� ������
  const sstring* find(const char *word);
  const sstring* find(const sstring& word);
};


}//ILSP_GREEKLISH

#endif //_CUSTOMDIC_H_


