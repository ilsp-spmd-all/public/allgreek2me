#ifndef _FILEMAPPER_H_
#define _FILEMAPPER_H_

#ifdef _WIN32
  #include <windows.h>
#else
  #include <sys/mman.h>
  #include <sys/stat.h>
  #include <fcntl.h>
  #include <stdlib.h>
  #include <stdio.h>
#endif

namespace ILSP_GREEKLISH{

class FileMapper{ 
#ifdef _WIN32
  HANDLE hMap;
  HANDLE hFile;
#else //_WIN32
  unsigned long map_len;
#endif //_WIN32
	unsigned char *data;
public:
  FileMapper(const char *fullpathfilename);
  ~FileMapper();

  const unsigned char *getData(){return data;}
};

} //ILSP_GREEKLISH

#endif //_FILEMAPPER_H_
 

