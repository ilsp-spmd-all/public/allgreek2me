#include <string.h>

#include "TriphonsProbs.h"
 
namespace ILSP_GREEKLISH{
	
TriphonsProbs::TriphonsProbs(char *src, bool _del_src):data_src(src),del_src(_del_src)
{
  triphons_probs = NULL;
  gram_extras = NULL;
  gram_extras_pos = NULL;
  if(src==NULL) return;
  int prob_array = read_triphons_header(src);
  if(prob_array>=0)
    triphons_probs = (float*)(src + prob_array);
}

TriphonsProbs::~TriphonsProbs()
{
  if(gram_extras) delete[] gram_extras;
  if(gram_extras_pos) delete[] gram_extras_pos;

  if(del_src && data_src!=NULL) 
    delete[] data_src;
}

char TriphonsProbs::char2index(char c)
{
	if(c>=GRAM_SEQ_FIRST && c<=GRAM_SEQ_LAST)
		return c - GRAM_SEQ_FIRST;

	char ret = 0;
	do
	{
		if(gram_extras[ret]==c) return gram_extras_pos[ret];
		ret++;
	}
	while(ret<GRAM_EXTRAS_COUNT && gram_extras[ret]!='\0');

	return -1;
}


int TriphonsProbs::read_triphons_header(char *in_src)
{
	//format:	char(GRAM_SEQ_FIRST) char(GRAM_SEQ_LAST) char(GRAM_COUNT) char (GRAM_EXTRAS_COUNT)
	//			char[GRAM_EXTRAS_COUNT](gram_extras) char[GRAM_EXTRAS_COUNT](gram_extras_pos)
	//			4 byte alignment
	//			float(ZERO_PROB)
  char *in = in_src;

	memcpy((char*)&GRAM_SEQ_FIRST,in,sizeof(char));
  in++;
	memcpy((char*)&GRAM_SEQ_LAST,in,sizeof(char)); 
  in++;
	memcpy((char*)&GRAM_COUNT,in,sizeof(char));
  in++;
	memcpy((char*)&GRAM_EXTRAS_COUNT,in,sizeof(char));
  in++;

  try{
    gram_extras = new char[GRAM_EXTRAS_COUNT];
    gram_extras_pos = new char[GRAM_EXTRAS_COUNT];
  } 
  catch(std::bad_alloc){
    return -1;
  }
  
	memcpy(gram_extras,in,GRAM_EXTRAS_COUNT);
  in+=GRAM_EXTRAS_COUNT;
	memcpy(gram_extras_pos,in,GRAM_EXTRAS_COUNT);
  in+=GRAM_EXTRAS_COUNT;

  //4 byte align
	if(GRAM_EXTRAS_COUNT%2) in+=2;

	memcpy((char*)&ZERO_PROB,in,sizeof(float));
  in+=sizeof(float);

  return (in - in_src);
}

double TriphonsProbs::triphon_prob(const char triphon[3])
{
  /*int i, j, k;
  i=(int)char2index(triphon[0]);
  j=(int)char2index(triphon[1]);
  k=(int)char2index(triphon[2]);

  if(i<0 || j<0 || k<0) return ZERO_PROB;

  return triphons_probs[i*GRAM_COUNT*GRAM_COUNT + j*GRAM_COUNT + k];*/

  return triphon_idx_prob(char2index(triphon[0]),
                          char2index(triphon[1]),
                          char2index(triphon[2]));
}

double TriphonsProbs::triphon_idx_prob(char i, char j, char k)
{
  if(i<0 || j<0 || k<0 || !triphons_probs) return ZERO_PROB;

  return triphons_probs[i*GRAM_COUNT*GRAM_COUNT + j*GRAM_COUNT + k];
}


} //ILSP_GREEKLISH

