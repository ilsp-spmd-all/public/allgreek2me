#ifndef _THREADSFESTRING_H_
#define _THREADSFESTRING_H_

namespace thread_safe_string{

class string
public:
    static const size_type npos = -1;
    string(const string& rhs);
    string(const char *s);
    string& operator=(const string& rhs);
    string& operator=(const char *s);
    string& operator=(char c);
    const char& operator[](int pos) const;
    char& operator[](int pos);
    const char *c_str() const;
    int size() const;
    string& operator+=(const string& rhs);
    string& operator+=(const char *s);
    string& operator+=(char c);
    string& erase(int p0 = 0, int n = npos);
    //iterator erase(iterator it);
    //iterator erase(iterator first, iterator last);
    int find(const string& str, int pos = 0) const;
    int find(const char *s, int pos, int n) const;
    int find(const char *s, int pos = 0) const;
    int find(char c, int pos = 0) const;
    int find_first_of(const string& str,
        int pos = 0) const;
    int find_first_of(const char *s, int pos,
        int n) const;
    int find_first_of(const char *s, int pos = 0) const;
    int find_first_of(char c, int pos = 0) const;
    int find_last_of(const string& str,
        int pos = npos) const;
    int find_last_of(const char *s, int pos,
        int n = npos) con/t;
    int find_last_of(const char *s, int pos = npos) const;
    int find_last_of(char c, int pos = npos) const;
    int find_first_not_of(const string& str,
        int pos = 0) const;
    int find_first_not_of(const char *s, int pos,
        int n) const;
    int find_first_not_of(const char *s, int pos = 0) const;
    int find_first_not_of(char c, int pos = 0) const;
    int find_last_not_of(const string& str,
        int pos = npos) const;
    int find_last_not_of(const char *s, int pos,
         int n) const;
    int find_last_not_of(const char *s,
        int pos = npos) const;
    int find_last_not_of(char c, int pos = npos) const;
    string substr(int pos = 0, int n = npos) const;
}

#endif //_THREADSFESTRING_H_

