#ifndef _RULES_H_
#define _RULES_H_

#include "sstring/sstring.h"
#include <string>
#include <vector>
#include <set>
#include <map>
#include <iostream>
#include <strstream>
#include <fstream>

using namespace std;

#include "TriphonsProbs.h"


#define myisspace(x) ((x)==' '||(x)=='\t'||(x)=='\r'||(x)=='\n')

namespace ILSP_GREEKLISH{

extern TriphonsProbs *triphons_probs;

class dic_word_prob
{
public:
	sstring word;
	double prob;

	dic_word_prob():word(""),prob(0){};
	dic_word_prob(sstring w, double p) :word(w),prob(p){};
	~dic_word_prob() {}

	bool operator<(const dic_word_prob& other) const {return prob<other.prob;}
};

class word_prob
{
public:
	sstring word, conv_word;
	double prob;
  float p1, p2, p3;

	word_prob():word(""),conv_word(""),prob(0),p1(0),p2(0),p3(0) {};
	word_prob(sstring w, double p, sstring w2 = "", float pp1 = 0, float pp2 = 0, float pp3 = 0)
    :word(w),conv_word(w2),prob(p),p1(pp1),p2(pp2),p3(pp3) {};
	~word_prob() {}

	bool operator<(const word_prob& other) const {return prob<other.prob;}
};

extern void lower_string(sstring *s);
extern void lower_string_greek(sstring *s); 
extern void lower_destress_greek(sstring *s);

typedef vector<sstring> stringvec;
typedef vector<float> floatvec;
typedef map<sstring,stringvec> map_string_stringvec;
typedef map<sstring,double> map_string_double;
typedef map<sstring,sstring> map_string_string;
typedef map<sstring,word_prob> map_string_word_prob;
typedef map<double,sstring> map_double_string;


class Rules
{
	map_string_stringvec greeklish2phon_rules;
	//map_string_double diphones_probabilities;
	map_string_double rules_probabilities;

  set<sstring> extensions_domains;

	map_string_double greek_from_greeklish_rules;
  
	double match_greek_greeklish_rec(sstring greek, sstring greeklish, int &num_of_rules);

	//sstring symfwna;
	sstring letters;
  sstring white_chars;
  sstring payla_chars;
  sstring letters_payla;
  sstring greek_vowels;
  sstring greek_letters;

	sstring nonalpha_letters;
  sstring stress_chars;

  sstring test;
public:
	sstring & get_nonalpha_letters(){return nonalpha_letters;}
  sstring & get_stress_chars(){return stress_chars;}

  inline bool is_nonalpha_letter(char c) {return nonalpha_letters.find(c)!=sstring::npos;}
  inline bool is_letter(char c) {return letters.find(c)!=sstring::npos;}
  inline bool is_alpha_letter(char c) {return is_letter(c) && !is_nonalpha_letter(c);}
  inline bool is_white(char c) {return white_chars.find(c)!=sstring::npos;}
  inline bool is_payla(char c) {return payla_chars.find(c)!=sstring::npos;}
  inline bool is_letter_or_payla(char c) {return payla_chars.find(c)!=sstring::npos || letters.find(c)!=sstring::npos;}

  inline bool is_digit(char c) {return c>='0'&&c<='9';}
  inline bool is_stress(char c) {return stress_chars.find(c)!=sstring::npos;}

  inline bool is_greek_vowel(char c) {return greek_vowels.find(c)!=sstring::npos;}
  inline bool is_greek_letter(char c) {
    //7/10/04 optimization
    //return greek_letters.find(c)!=sstring::npos;
    return ((c>='�' && c<='�') || c=='�');
  }

  bool is_extension_domain(sstring key);

	Rules(){}
	Rules(istream& greeklish2phon_rules,
        istream& greek_from_greeklish_rules,
        istream& extensions_domains_file);
  ~Rules();

	void read_greeklish2phon_rules(sstring ifs_name);
	void read_greeklish2phon_rules(istream& ifs);

	//void read_diphone_probabilities(sstring ifs_name);
	//void read_diphone_probabilities(istream& ifs);

	void read_greek_from_greeklish_rules(sstring ifs_name);
	void read_greek_from_greeklish_rules(istream& ifs);

	void read_extensions_domains(sstring ifs_name);
	void read_extensions_domains(istream& ifs);

	sstring phoneme2greek(sstring phon_word, sstring greeklish);
	stringvec * greeklish2phon(sstring key);
  
  sstring phon_grk_2_gr(sstring phon, sstring grk);

	//double diphone_probability(sstring key);
	double rule_probability(sstring key);
	//double wordprob(sstring word);       
	double wordprob_triphons(sstring word);     
	bool wordprob_triphons_exist(sstring word);
  
	double match_greek_greeklish(sstring greek, sstring greeklish, int &num_of_rules);
	//bool isSymphwno(char c);
  char upper_greek(char grch);
  char upper_greek_bare(char grch);

  sstring unstress_greek(sstring in);

  char stress_greek_vowel(char c);

  bool is_greek_word_stressed(sstring &word);
  bool is_sentence_end(sstring &word);

  //sstring elot_greek2greeklish(sstring greek);
};

} //ILSP_GREEKLISH

#endif //_RULES_H_

