#ifndef _GRKCVTLIB_H_
#define _GRKCVTLIB_H_

namespace ILSP_GREEKLISH{

bool __unlockit(const char *key);
void __destroy();
char * __convert(const char *source);
char * __convert_analyse(const char *source);
#ifdef ALLGREEK_LM
char * __convert_lm(const char *source);
#endif //ALLGREEK_LM
char * __elot_gr2grl(const char *source);
char * __custom_gr2grl(const char *source);
void __release_pchar(const char *cpsz);
void __convert_file(const char *source, const char *dest);
void __convert_gr2grl_file(const char *source, const char *dest);

void __load_custom_gr2grl();
void __save_custom_gr2grl();
void __import_custom_gr2grl(const char *rules_str);
bool __is_valid_gr2grl_rule(const char *rule);
const char* __export_rules(bool custom);

void __load_save_custom_gr_dic(const char *dic);   
void __load_save_custom_en_dic(const char *dic);
void __load_save_custom_ex_dic(const char *dic);

const char *__get_module_path();
#ifndef _WIN32
void __set_module_path(const char *);
#endif

//#define HELP_FUNCS
#ifdef HELP_FUNCS
void __clear_file(const char *source, const char *dest, const char *r);
void __engfngr(const char *source, const char *dest, bool prob_exists);
void __word_phonprob(const char *source, const char *dest);
#endif //HELP_FUNCS

//------------------------------------------------------------------------------
#ifdef NOT_LIB_DLL
/*
#define unlockit __unlockit
#define convert __convert
#define elot_gr2grl __elot_gr2grl
#define custom_gr2grl __custom_gr2grl

#define load_custom_gr2grl __load_custom_gr2grl
#define save_custom_gr2grl __save_custom_gr2grl
#define import_custom_gr2grl __import_custom_gr2grl
#define is_valid_gr2grl_rule __is_valid_gr2grl_rule
#define export_rules __export_rules

#define load_save_custom_gr_dic __load_save_custom_gr_dic
#define load_save_custom_en_dic __load_save_custom_en_dic

#define get_module_path __get_module_path

#ifdef HELP_FUNCS
#define clear_file __clear_file
#define engfngr __engfngr
#define word_phonprob __word_phonprob
#endif //HELP_FUNCS
*/
#endif //NOT_LIB_DLL    
//------------------------------------------------------------------------------

} //ILSP_GREEKLISH


#endif //_GRKCVTLIB_H_

