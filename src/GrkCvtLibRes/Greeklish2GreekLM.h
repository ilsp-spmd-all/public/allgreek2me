// Greeklish2GreekLM.h: interface for the Greeklish2GreekLM class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _GREEKLISH2GREEKLM_H_
#define _GREEKLISH2GREEKLM_H_

#ifdef ALLGREEK_LM

#include "sstring/sstring.h"
#include <string>
#include <vector>
using namespace std;

#include "Globals.h"
#include "Rules.h"
#include "PhonNode.h"
#include "indexreader.h"
#include "mapfilereader.h"

namespace ILSP_GREEKLISH{

//default values
double default_dic_prob = -10.0;
double default_phon_prob = -8.0;
double default_rul_prob = -1.0;
double default_bck_prob = 0.0;
double default_eng_prob = -9.0;
double default_join_prob = -10.0;

//weights
double w_dic_prob = 1.;
double w_phon_prob = 0.;
double w_rul_prob = 1.;
double w_bck_prob = 10.;
double w_eng_prob = 1.;
double w_join_prob = 10.;

class Greeklish2GreekLM
{
private:

  struct token{ //!!all in one
    sstring text_r;
    sstring text;  
    sstring phon;
    bool isdelim;
    double dic_prob, phon_prob, rul_prob, bck_prob, eng_prob;
    //double join_prob;                                      
    double best_join_prob;
    vector<double> join_probs;
    double prob;
    long index;
    long layer_index;
    token():
      text(""), text_r(""), phon(""),
      isdelim(false),
      dic_prob(default_dic_prob),
      phon_prob(default_phon_prob),
      rul_prob(default_rul_prob),
      bck_prob(default_bck_prob),
      eng_prob(default_eng_prob),
      //join_prob(default_join_prob),
      best_join_prob(default_join_prob),
      prob(.0),
      index(-1),
      layer_index(-1)
    {
    }
  };
  typedef vector<token> vector_token;
  typedef vector_token::iterator vector_token_it;
  typedef vector_token::const_iterator vector_token_cit;

private:
  bool next_word2convert(sstring &text, sstring &text_r, int pos_beg, int &pos_word_start, int &pos_word_end);

  void capitalize_word(sstring &word, sstring &word_r);

  double calc_join_cost(sstring &word1, sstring &word2);

	void tokenize(sstring &text, vector_token &tokens);

  void analyze_token(const token &tok, vector_token &conversions);

  void analyze_text(sstring &text, vector_token &tokens);

public:
	Greeklish2GreekLM();
	virtual ~Greeklish2GreekLM();

  void convert(sstring &text, sstring &converted_text);

private:
	MapFileReader *greeklish_dicindex, *english_dicindex;  
	MapFileReader *lm_dicindex;
};


} //ILSP_GREEKLISH

#endif //ALLGREEK_LM

#endif // _GREEKLISH2GREEKLM_H_


