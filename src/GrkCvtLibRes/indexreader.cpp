#include "indexreader.h"

namespace ILSP_GREEKLISH{

IndexReader::IndexReader(const char *fname)
{
	source.open(fname,ios::in | ios::binary);

  filename = sstring("") + fname;

	if(!source)
	{
		//!sstring s = "Dictionary not found! (file " + sstring(fname) + ")";
		//!throw s;
		source_ok = false;
	}
	else
	{
		source_ok = true;
	}
	data_length = 0;
	must_decrypt = false;

  mask_length = 27;
  mask_srand = 11;
  try{
    mask = new unsigned char[mask_length];
  }
  catch(std::bad_alloc){
		source_ok = false;
    return;
  }

  build_mask();
}

IndexReader::~IndexReader()
{
  if(mask) delete[] mask;

  if(source_ok)
  	source.close();
}      

void IndexReader::reset_source()
{
	if(!source_ok) return;
  
  try{
    source.clear();
	  source.seekg(0);
  }
  catch(...){
    source_ok = false;
  }

	if(!source_ok){ 
  	source.close();
	  source.open(filename.c_str(),ios::in | ios::binary);
  	if(!source) source_ok = false; else	source_ok = true;
  }
}

inline void IndexReader::build_mask()
{
#ifdef __BORLANDC__
  srand(mask_srand);
	for(int i=0;i<mask_length;i++)
    mask[i] = (unsigned char)(rand()%256);
#else //__BORLANDC__
  int i = 0;
  mask[i++] = 0x16;
  mask[i++] = 0xCD;
  mask[i++] = 0xDD;
  mask[i++] = 0x42;
  mask[i++] = 0x7F;
  mask[i++] = 0x06;
  mask[i++] = 0x54;
  mask[i++] = 0x7A;
  mask[i++] = 0x05;
  mask[i++] = 0x0A;
  mask[i++] = 0xD5;
  mask[i++] = 0xC1;
  mask[i++] = 0x9B;
  mask[i++] = 0xDA;
  mask[i++] = 0x1D;
  mask[i++] = 0x69;
  mask[i++] = 0xBE;
  mask[i++] = 0x6C;
  mask[i++] = 0xC6;
  mask[i++] = 0xE2;
  mask[i++] = 0xA8;
  mask[i++] = 0xC4;
  mask[i++] = 0x14;
  mask[i++] = 0xEE;
  mask[i++] = 0xB9;
  mask[i++] = 0x53;
  mask[i++] = 0xFE;
#endif //_WIN32
}

/*
inline void IndexReader::decrypt(unsigned char *buf, short length, unsigned char mask)
{
	if(must_decrypt)
	{
		for(int i=0;i<length;i++)
		{
			buf[i] = mask^buf[i];
		}
	}
}
*/

inline void IndexReader::source_read(char *dest, int length)
{
	if(!source_ok) return;
  int pos = (int)source.tellg();
	source.read(dest,length);

	//decrypt((unsigned char*)dest,length);
  for(int i=0;i<length;i++)
    dest[i] = mask[(pos+i)%mask_length]^dest[i];
}

double IndexReader::searchKey(const char *key)
{
  reset_source();
	if(!source_ok) return 0.0;

	int key_pos = 0, key_size = strlen(key), count, i, offset;

	while(key_pos < key_size)
	{
		source_read((char*)&count,sizeof(int));
		source_read(keys,count + ((count%4)?(4-count%4):0));
		//int dbg = source.tellg();
		source_read((char*)offsets,count*sizeof(int));

		for(i=0;i<count;i++)
			if(keys[i]==key[key_pos]) break;

		if(i==count) return 0;

		offset = offsets[i];
		source.seekg(offset);
		key_pos++;
	}
	
	source_read((char*)&count,sizeof(int));
	offset = count*sizeof(int) + count + ((count%4)?(4-count%4):0);
	source.seekg((int)source.tellg() + offset);

	source_read((char*)&data_length,sizeof(int));

  firstProbRead = true;

	return data_length;
}

void IndexReader::copyData(void *buf)
{
  //!long fpos = source.tellg();
	source_read((char*)buf,data_length);
}

///////////////////////////////////////////////////////////

void WordProbReader::getWordProbs(list<dic_word_prob*> &res)
{
	if(!source_ok) return;

	char *buf;
  try{
    buf = new char[data_length];
  }
  catch(std::bad_alloc){
    return;
  }

	int dtpart, length = data_length, pos = 0;

	copyData(buf);
  dic_word_prob *tmp_word_prob;
	do
	{ 
    try{
      tmp_word_prob = new dic_word_prob(sstring(buf+pos+sizeof(double)),*(double*)(buf+pos));
    }
    catch(std::bad_alloc){
      tmp_word_prob = NULL;
    }

		if(tmp_word_prob) res.push_back(tmp_word_prob);
		dtpart = strlen(buf+pos+sizeof(double)) + 1;
		dtpart += sizeof(double);
		if(length<=dtpart) break;
		pos += dtpart;
		length -= dtpart;
	}while(length>4);

  firstProbRead = false;
  firstProb = (*res.begin())->prob;

	delete[] buf;
}

///////////////////////////////////////////////////////////

double WordProbReader::getFirstProb()
{
	if(!source_ok) return 0.0;
	
	if(firstProbRead)
    source_read((char*)&firstProb,sizeof(double));

  firstProbRead = false;

	return firstProb;
}

///////////////////////////////////////////////////////////

sstring StringReader::getString()
{
	if(!source_ok) return sstring("");

	char *buf;
  try{
    buf = new char[data_length];
  }  
  catch(std::bad_alloc){
    return sstring("");
  }

	copyData(buf);
	
	sstring ret(buf);

	delete[] buf;

	return ret;
}

} //ILSP_GREEKLISH
