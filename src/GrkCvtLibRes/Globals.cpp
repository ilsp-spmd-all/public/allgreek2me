#include "Globals.h"
#include "GrkRes.rh"

#ifdef _WIN32
HINSTANCE hModuleInstance;
#endif //_WIN32

namespace ILSP_GREEKLISH{

#ifndef _WIN32
static char resource_path[5000] = {'\0'};
void SetResoucePath(const char *path){
  //resource_path = path;
  strcpy(resource_path, path);
}
#endif //_WIN32

//rules global object
Rules *rules = NULL;
TriphonsProbs *triphons_probs;
CustomDic *customDic;
CustomEngDic *customEngDic;
CustomDicExceptions *customDicExceptions;
Greek2Greeklish *greek2Greeklish;
FileMapper *fileMapperGr, *fileMapperEn;

static const sstring greek_low     = "????????????????????????????????????";
static const sstring greek_up      = "????????????????????????ٸ??????????";
static const sstring greek_all_but_low  = "??????????????????????????????????ٸ????????";
static const sstring greek_low_destress = "????????????????????????????????????????????";

void lower_string(sstring *s_in)
{
  sstring& s = *s_in;
  sstring::size_type sz = s.size();
	for(sstring::size_type i = 0;i<sz;i++)
	{
		/*if(isalnum((int)(unsigned char)s[i])){
			s[i] = tolower(s[i]);
    }*/
    if(s[i]>='A' && s[i]<='Z'){
			s[i] = 'a' + s[i] - 'A';
    }
    //else if(s[i]=='\?'){
	  else if((unsigned char)s[i]==146){
      s[i] = '\'';
    }
    //else if(isspace((int)s[i])){
    else if(myisspace(s[i])){
      continue;
    }
    else if(greek_low.find(s[i])!=sstring::npos || greek_up.find(s[i])!=sstring::npos){
      continue;
    }
    else if(s[i]<0 || !isprint((int)s[i])){
      s[i] = ' ';
    }
	}
}

/*
sstring lower_string(sstring s)
{
	sstring ret = s;
  lower_string(&ret);
	return ret;
}*/

void lower_string_greek(sstring *s_in)
{
  sstring& s = *s_in;
  sstring::size_type sz = s.size();
  sstring::size_type pos;
	for(sstring::size_type i = 0;i<sz;i++)
	{
    pos = greek_up.find_first_of(s[i]);
		if(pos!=sstring::npos)
			s[i] = greek_low[pos];
	}
}

void lower_destress_greek(sstring *s_in)
{
  sstring& s = *s_in;
  sstring::size_type sz = s.size();
  sstring::size_type pos;
	for(sstring::size_type i = 0;i<sz;i++)
	{
    pos = greek_all_but_low.find_first_of(s[i]);
		if(pos!=sstring::npos)
			s[i] = greek_low_destress[pos];
	}
}

#ifndef _WIN32
sstring FilenameFromResId(unsigned long resID){
  switch(resID){
  case IDR_GRL2PHON_PROBS: return GetModulePath() + "IDR_GRL2PHON_PROBS.bin";
  case IDR_DIPH_PROBS: return GetModulePath() + "IDR_DIPH_PROBS.bin";
  case IDR_GR_FROM_GRL: return GetModulePath() + "IDR_GR_FROM_GRL.bin";
  case IDR_TRIPHONS_PROBS: return GetModulePath() + "TRIPHONS.bin";
  case IDR_COMMONGR2GRL: return GetModulePath() + "COMMONGR2GRL.bin";
  case IDR_EXTENSIONS_DOMAINS: return GetModulePath() + "EXTENSIONS_DOMAINS.bin";
  }
  return NULL;
}
#endif

//---------------------------------------------------------------------------
char * GetResource(unsigned long resID, const char* lpType, bool mask) {
	unsigned char masks[7] = {0x3f,0xa5,0x63,0xf1,0xe8,0x29,0x32};
	char *retbuf;
  long sz;

#ifdef _WIN32
	char *buffer;

	HMODULE __module__ = ::GetModuleHandle("GrkCvt.dll");

	char lpName[32];
#ifdef __BORLANDC__
	sprintf(lpName, "#%i", resID);
#else
  sprintf(lpName, "%i+%i", IDR_GREEKLISH_BASE, resID-IDR_GREEKLISH_BASE);
#endif

	HRSRC hRsrc = ::FindResource(__module__, lpName, lpType);
	HGLOBAL hGlbl = ::LoadResource(__module__, hRsrc);
	LPVOID lpRsrc = ::LockResource(hGlbl);
	sz = ::SizeofResource(__module__, hRsrc);

  try{
    retbuf = new char[sz+1];
  }
  catch(std::bad_alloc){
    return NULL;
  }

	memcpy(retbuf, lpRsrc, sz);
  retbuf[sz] = 0;

#else
  FILE *fp;
  fp = fopen(FilenameFromResId(resID).c_str(),"rb");
  if(fp!=NULL){
    fseek(fp,0,SEEK_END);
    sz = ftell(fp);
    rewind(fp);

    try{
      retbuf = new char[sz+1];
    }
    catch(std::bad_alloc){
      return NULL;
    }
    fread(retbuf,1,sz,fp);
    retbuf[sz] = 0;
  }
  else{
    printf("Cannot open file %s\n",FilenameFromResId(resID).c_str());
    return NULL;
  }
#endif

  if(mask)
    for(long i=0;i<sz;i++)
		retbuf[i] ^= masks[i%7];

	return retbuf;
}

//---------------------------------------------------------------------------
void init_triphons_probs(unsigned int resID, const char* lpType) {
#ifdef _WIN32
	//HMODULE __module__ = (HMODULE)hModuleInstance;
	HMODULE __module__ = ::GetModuleHandle("GrkCvt.dll");

	//LPCSTR lpName = MAKEINTRESOURCE(resID);
	char lpName[32];
#ifdef __BORLANDC__
	sprintf(lpName, "#%i", resID);
#else
  sprintf(lpName, "%i+%i", IDR_GREEKLISH_BASE, resID-IDR_GREEKLISH_BASE);
#endif

	HRSRC hRsrc = ::FindResource(__module__, lpName, lpType);
	HGLOBAL hGlbl = ::LoadResource(__module__, hRsrc);
	LPVOID lpRsrc = ::LockResource(hGlbl);

  triphons_probs = new TriphonsProbs((char*)lpRsrc);

#else
  triphons_probs = new TriphonsProbs(GetResource(resID, lpType, false), true);
#endif
}

sstring GetModulePath()
{
#ifdef _WIN32
	char buf[256];

	//HMODULE __module__ = (HMODULE)hModuleInstance;
	HMODULE __module__ = ::GetModuleHandle("GrkCvt.dll");

	DWORD hres = GetModuleFileName(
		__module__,
		buf,
		255
	);

  if(!hres) return sstring("");

	sstring ret(buf);

	int pos = ret.find_last_of('\\');
	ret.erase(pos+1);
  //ret.remove(pos+1);

	return ret;
#else
  sstring ret = sstring(resource_path);
  if(ret.size()>0 && ret[ret.size()-1]!='/') ret+='/';
  return ret;
#endif
}

float THRESHOLD_GR_BACK_FOUND,
      THRESHOLD_GR_DIC_FOUND,
      THRESHOLD_GR_BACK_NOT_FOUND,
      THRESHOLD_TRIPHON,
      THRESHOLD_DIFF,
      THRESHOLD_UNSTRESS;

#define USE_STD_FSTREAM
void read_ini(const char *inifile)
{
  THRESHOLD_GR_BACK_NOT_FOUND = -0.25;
  THRESHOLD_GR_BACK_FOUND = -2;
  THRESHOLD_GR_DIC_FOUND = -4;
  THRESHOLD_TRIPHON = -9;
  THRESHOLD_DIFF = -9;
  THRESHOLD_UNSTRESS = -7;

#ifdef USE_STD_FSTREAM
  fstream fini;
  sstring tok;

  fini.open(inifile, ios::in);

  if(!fini) return;

  std::string std_tok;
  while(fini>>std_tok)
  {
    tok = std_tok.c_str();
    lower_string(&tok);

    if(tok[0]=='/' && tok[1]=='/')
    {
      fini.unget();
      fini.ignore(1024,'\n');
      continue;
    }

    if(tok=="difference") {fini>>THRESHOLD_DIFF; continue;}
    if(tok=="backward_found") {fini>>THRESHOLD_GR_BACK_FOUND; continue;}
    if(tok=="gr_dic_found") {fini>>THRESHOLD_GR_DIC_FOUND; continue;}
    if(tok=="backward_not_found") {fini>>THRESHOLD_GR_BACK_NOT_FOUND; continue;}
    if(tok=="phonetic") {fini>>THRESHOLD_TRIPHON; continue;}
    if(tok=="unstress") {fini>>THRESHOLD_UNSTRESS; continue;}
  }

  fini.close();
#else
  FILE *fini;
  try{
    char buf[256];
    sstring tok;

    fini = fopen(inifile, "r");

    if(!fini) return;

    while(fscanf(fini,"%s",buf)!=EOF)
    {
      tok = sstring(buf);
      lower_string(&tok);

      if(tok[0]=='/' && tok[1]=='/')
      {
        while(getc(fini)!='\n');
        continue;
      }

      if(tok=="difference") {fscanf(fini,"%f",&THRESHOLD_DIFF); continue;}
      if(tok=="backward_found") {fscanf(fini,"%f",&THRESHOLD_GR_BACK_FOUND); continue;}
      if(tok=="gr_dic_found") {fscanf(fini,"%f",&THRESHOLD_GR_DIC_FOUND); continue;}
      if(tok=="backward_not_found") {fscanf(fini,"%f",&THRESHOLD_GR_BACK_NOT_FOUND); continue;}
      if(tok=="phonetic") {fscanf(fini,"%f",&THRESHOLD_TRIPHON); continue;}
      if(tok=="unstress") {fscanf(fini,"%f",&THRESHOLD_UNSTRESS); continue;}
    }

    fclose(fini);
  }
  catch(...){
    fclose(fini);
  }
#endif
}

bool InitRules()
{
	char *buf1 = NULL, *buf2 = NULL, *buf3 = NULL;

  try{
    customDic = new CustomDic((GetModulePath()+"CustomLexicon.txt").c_str());
    customEngDic = new CustomEngDic((GetModulePath()+"CustomEngLexicon.txt").c_str());
    customDicExceptions = new CustomDicExceptions((GetModulePath()+"CustomExcLexicon.txt").c_str());
    greek2Greeklish = new Greek2Greeklish((GetModulePath()+"CustomGreek2Greeklish.txt").c_str());

    fileMapperGr = new FileMapper((GetModulePath()+_GR_INDEX_FILENAME_).c_str());
    fileMapperEn = new FileMapper((GetModulePath()+_EN_INDEX_FILENAME_).c_str());

    read_ini((GetModulePath() + "GrkCvt.ini").c_str());
  }
  catch(std::bad_alloc){
    return false;
  }

	buf1 = GetResource(IDR_GRL2PHON_PROBS, "TEXT");
	buf2 = GetResource(IDR_GR_FROM_GRL, "TEXT");
	buf3 = GetResource(IDR_EXTENSIONS_DOMAINS, "TEXT");

  /*istream *ret;
  try{
    ret = new istrstream(buf,sz);
  }
  catch(std::bad_alloc){
    delete[] buf;
    return NULL;
  }*/

  init_triphons_probs(IDR_TRIPHONS_PROBS, "TEXT");

  rules = NULL;
	if(buf1 && buf2 && buf3){
    istream *is1 = new istrstream(buf1,strlen(buf1)),
            *is2 = new istrstream(buf2,strlen(buf2)),
            *is3 = new istrstream(buf3,strlen(buf3));
    rules = new Rules(
		  *is1,
		  *is2,
      *is3);
    delete is1;
    delete is2;
    delete is3;
  }

	if(buf1!=NULL) delete[] buf1;
	if(buf2!=NULL) delete[] buf2;
	if(buf3!=NULL) delete[] buf3;

  return rules!=NULL;
}


} //ILSP_GREEKLISH
