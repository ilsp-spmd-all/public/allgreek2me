import greeklish
from flask import Flask, jsonify, render_template, request, url_for

from flask_cors import CORS, cross_origin

converter = greeklish.GreeklishConverter()


app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"


@app.route("/greeklish", methods=["POST"])
def correct():
    sentence = request.json["sentence"]
    greek = converter.convert(sentence)

    return jsonify({"sentence": greek})

@app.route("/greek2greeklish", methods=["POST"])
def correct1():
    sentence = request.json["sentence"]
    greek = converter.convert(sentence,to="greeklish")

    return jsonify({"sentence": greek})

if __name__ == "__main__":
    from waitress import serve

    serve(app, host="0.0.0.0", port=4444)
    # example usage
    # import requests
    # res = requests.post("http://IP:4444/greeklish", json={"sentence": "geia sou"})
    # greek_sentence = res.json()["sentence"]
