import ag2m

class GreeklishConverter(object):
    def __init__(self):
        ag2m.init()

    def __del__(self):
        ag2m.finish()

    def grl2g(self, greeklish):
        return ag2m.greeklish2greek(greeklish).decode("iso-8859-7")

    def g2grl(self, greek):
        return ag2m.greek2greeklish(greek).decode("iso-8859-7")

    def convert(self,text,to="greek"):
        text = text.encode("iso-8859-7")
        if to=="greek":
            return self.grl2g(text)
        elif to=="greeklish":
            return self.g2grl(text)
        else:
            raise ValueError("Invalid 'to' parameter, should either be 'greek' or 'greeklish'")

        
