#include "GrkCvt.h"

#include <pybind11/pybind11.h>


pybind11::bytes
greeklish2greek(char *input)
{
    void *cpsz = greeklish_convert(input);
    const char *converted = greeklish_get_text(cpsz);
    char out[2048] = { "\0" };
    strcpy(out, (char *)converted);
    greeklish_release_handle(cpsz);
    return pybind11::bytes(out);
}

pybind11::bytes
greek2greeklish(char *input)
{
    void *cpsz = greeklish_custom_gr2grl(input);
    const char *converted = greeklish_get_text(cpsz);
    char out[2048] = { "\0" };
    strcpy(out, (char *)converted);
    greeklish_release_handle(cpsz);
    return pybind11::bytes(out);
}


void
init()
{
    greeklish_init(NULL);
}

void
finish()
{
    greeklish_fini();
}


PYBIND11_MODULE(ag2m, m)
{
    m.doc() = "Greeklish to greek conversion bindings";

    m.def("greeklish2greek", &greeklish2greek, "Convert a string in greeklish to greek");
    m.def("greek2greeklish", &greek2greeklish, "Convert a string in greek to greeklish");
    m.def("init", &init, "Initialize greeklish to greek converter");
    m.def("finish", &finish, "finalize greeklish to greek converter");
}
