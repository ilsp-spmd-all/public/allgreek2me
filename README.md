# Compile the project

Run

```bash
mkdir build
cd build
cmake ..
```


Tested on Ubuntu 20.04.


# Use the executable to convert greeklish to greek

The compilation produces an executable `build/greeklish2greek`.

Input is provided by a file. Careful, output is in Windows-1253 format.

```bash
cd build/
echo "geia sou" > testf
echo "ti kaneis;" >> testf

./greeklish2greek -i testf | iconv -f Windows-1253 -t UTF-8
```


# Use the executable to convert greek to greeklish

The same procedure can be followed to convert greek text to english

```bash

echo "είμαι καλά" | iconv -f UTF-8 -t Windows-1253 > testf

./greeklish2greek -g -i testf
```

# Use the python API to convert greeklish to greek

Install the package

```bash
python setup.py install
```


```python
import greeklish
g2g = greeklish.GreeklishConverter()
print(g2g.convert("geia sou"))
>>> γεια σου
```
